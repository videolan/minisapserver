/*****************************************************************************
 * socket.h: VideoLAN Channel Server network facilities
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: host.h,v 1.2 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

/**
 * \brief This class encapsulates a subnet or a host description and its
 * associated policy.
 *
 * Only works with IPv4
 *
 * \see C_ConfigTxt
 */
class C_Host
{
public:
    typedef enum { ALLOW, DENY } type;
    
    /**
     * \brief creates a new host object 
     * 
     * \param config : the pattern is  addr/mask : 192.168.120.0/255.255.255.0
     */
    C_Host(char * config);
    /**
     * \brief creates a new host object 
     * 
     * \param config : the pattern is  addr/mask : 192.168.120.0/255.255.255.0
     */
    C_Host(const std::string config);

    /**
     * \brief tests if the specified ip corresponds to this object
     */
    bool test(in_addr_t ip);

    /**
     * \brief returns the policy for this host
     */
    type getType();

    /**
     * \brief returns a string description of this object
     *
     * for example : "Allowing from 127.0.0.1/255.255.255.255"
     */
    std::string getStr();

    /**
     * \brief converts an ip to its in_addr_t value
     */
    static in_addr_t pton(std::string str);
    
protected:
    /**
     * \brief the string reprentation of this object
     */
    std::string str;

    /**
     * \brief the base address of this host
     */
    in_addr_t addr;

    /**
     * \brief the netmork mask of this host
     */
    in_addr_t mask;

    /**
     * \brief the policy of this host
     */
    type m_type;
};
