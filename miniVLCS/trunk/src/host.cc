
/*****************************************************************************
 * socket.c: VideoLAN Channel Server network facilities
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: host.cc,v 1.2 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <string>
#include <string.h>
#include <arpa/inet.h>
#include <fstream>     
#include <iostream>                                                             

#include "host.h"

in_addr_t C_Host::pton(std::string str)
{
    struct in_addr addr;
    inet_pton(AF_INET, str.c_str(), &addr);
    return addr.s_addr;
}

C_Host::C_Host(char * config)
{
    std::string * _config;
    C_Host(*(_config = new std::string(config)));
    delete _config;
}


C_Host::C_Host(std::string config)
{
    int pos = config.find('/');
    std::string sip;
    std::string smask;
   
    if (pos >= 0)
    {
        sip = config.substr(1, pos-1);
        smask = config.substr(pos+1);
    }
    else
    {
        sip = config.substr(1);
        smask = "255.255.255.255";
        if (!sip.compare("all"))
        {
            sip = "0.0.0.0";
            smask = "0.0.0.0";
        }
    }
    
    if (config[0] == '+')
    {
        m_type = ALLOW;    
        str = "Allowing ";
    }
    else
    {
        m_type = DENY;
        str = "Denying ";
    }

    addr = pton(sip);
    mask = pton(smask);

    str += "from " + sip + "/" + smask;
}

std::string C_Host::getStr()
{
    return str;
}

bool C_Host::test(in_addr_t ip)
{
    return ((ip & mask) == (addr & mask));
}

C_Host::type C_Host::getType()
{
    return m_type;
}


