/*****************************************************************************
 * logger.h: VideoLAN Channel Server logging facility
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: logger.h,v 1.3 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

/**
 * \brief Logging facility abstract class
 *
 * This class describes the interface  of the logging facility.
 */
class C_Logger
{
public:
    /**
     * \brief Enumeration of the logging commands.
     *
     * send commands using &operator<<(log_command value)
     */
    typedef enum 
    { 
        DEBUG,  /**< change logging level to DEBUG */
        INFO,   /**< change logging level to INFO */
        WARNING,/**< change logging level to WARNING */
        ERROR,  /**< change logging level to ERROR */
        ALERT,  /**< change logging level to ALERT */
        END     /**< Command to end a message */
    } log_command;

    /**
     * \brief Class destructor, must be implemented by children
     */
    virtual ~C_Logger( void ) = 0;

    /**
     * \brief Adds msg to the message
     */
    virtual C_Logger &operator<<(const char * msg) = 0;

    /**
     * \brief Adds a number to the message
     */
    virtual C_Logger &operator<<(int value) = 0;

    /**
     * \brief Sends a command to the logging object
     */
    virtual C_Logger &operator<<(log_command value) = 0;

    /**
     * \brief Adds a string to the message
     */
    virtual C_Logger &operator<<(std::string value) 
    {
        *this << value.c_str(); 
        return *this;
    }
};

/**
 * \brief Logging facility implementing a chain of logging facilities
 *
 * When something is sent to this object, it will be sent back to all
 * of the C_Logger objects of the list.
 */
class C_ChainedLogger : public C_Logger
{
public:
    C_ChainedLogger();
    virtual ~C_ChainedLogger( void );
    virtual C_Logger &operator<<(const char * msg);
    virtual C_Logger &operator<<(int value);
    virtual C_Logger &operator<<(log_command value);

    /**
     * \brief Add another C_Logger to the list
     */
    void Add(C_Logger * logger);

    /**
     * \brief Remove the specified C_Logger from the list
     */
    void Remove(C_Logger * logger);
protected:
    /**
     * \brief The list of C_Logger objects.
     */
    std::list<C_Logger *> logger_list;
};

/**
 * \brief Logging facility using the host's system logger
 */
class C_SysLogger : public C_Logger
{
public:
    /**
     * \brief create a new logger to the syslog
     *
     * \param syslog_name the name appearing in the syslog
     * \facility the syslog facility used
     */
    C_SysLogger(const std::string syslog_name, int facility);
    virtual ~C_SysLogger( void );
    virtual C_Logger &operator<<(const char * msg);
    virtual C_Logger &operator<<(int value);
    virtual C_Logger &operator<<(log_command value);

protected:
    /**
     * As the syslog() function wants the complete message , we must
     * use a buffer until we get the END command.
     */
    std::string buffer;

    /**
     * \brief Gets a trace of the current logging level
     */
    int level;
};

/**
 * \brief Logging facility outputing to a file or any output stream.
 */
class C_FileLogger : public C_Logger
{
public:
    /**
     * \brief Coloration scheme.
     */
    typedef enum 
    { 
        NO_COLOR, /**< no coloring is used */
        LINUX     /**< adds color commands in the stream in the tty way */
    } logging_color;
    
    /**
     * \brief Creates a logging facility using the specified output stream
     * 
     * \param os a pointer to the output stream (this ostream object will not be
     * destroyed when thi object is destroyed)
     * \param color If none specified, no coloring is used.
     */
    C_FileLogger(std::ostream *os, logging_color color = NO_COLOR);

    /**
     * \brief Creates a logging facility appending to the specified file
     * 
     * \param filename The name of the file where to log to
     * \param color If none specified, no coloring is used.
     */
    C_FileLogger(const std::string filename, logging_color color = NO_COLOR);
    virtual ~C_FileLogger( void );

    virtual C_Logger &operator<<(const char * msg);
    virtual C_Logger &operator<<(int value);
    virtual C_Logger &operator<<(log_command value);

protected:
    /**
     * true if the object should be destroyed when this object is destroyed.
     */
    bool destroy;

    /**
     * Pointer to the outputstream where data will be sent.
     */
    std::ostream * log_file;

    /**
     * Current coloring scheme
     */
    logging_color m_color;
};

/**
 * \brief Logging facility using standard ERROR
 */
class C_StdLogger : public C_FileLogger
{
public:
    /**
     * \brief Default constructor
     *
     * Constructs a new logger to Standard error.
     *
     * \param color If none specified, no coloring is used.
     */
    C_StdLogger( logging_color color = NO_COLOR );
    virtual ~C_StdLogger( void );
};

