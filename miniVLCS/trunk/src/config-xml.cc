/*****************************************************************************
 * config-xml.cc: mini-VideoLAN Channel Server, configuration system
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: config-xml.cc,v 1.2 2002/12/20 15:53:59 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <malloc.h>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>


#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <syslog.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>
#include <vector> 
#include <list> 
#include <iostream>                                                             
#include <fstream>
#include <string>
#include <map>
#include <utility>

#include "host.h"
#include "vlcs.h"
#include "logger.h"
#include "config-common.h"
#include "config-xml.h"


C_ConfigXml::C_ConfigXml(C_Logger &logger) 
    :C_Config(logger), channels(0), i_channel_max(0)
{
    socket_port = 0;
    socket_type = IPV4;
    configFile = CHANNEL_CONF_XML;
}

C_ConfigXml::~C_ConfigXml()
{
}

void C_ConfigXml::ReadFile()
{
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        logger << C_Logger::ERROR << "Error during initialization! :\n"
            << message << "\n" << C_Logger::END;
        delete [] message;
        return;
    }

    XercesDOMParser* parser = new XercesDOMParser();
    parser->setValidationScheme(XercesDOMParser::Val_Always);    // optional.
    parser->setDoNamespaces(true);    // optional

    ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
    parser->setErrorHandler(errHandler);

    mtime = GetModificationTime();
    channels.clear();
    h_hosts.clear();

    try {
        parser->parse(configFile.c_str());
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        logger << C_Logger::ERROR << "Exception message is: \n"
            << message << "\n" << C_Logger::END;
        delete [] message;
        return;
    }
    catch (const DOMException& toCatch) {
        char* message = XMLString::transcode(toCatch.msg);
        logger << C_Logger::ERROR << "Exception message is: \n"
            << message << "\n" << C_Logger::END;
        delete [] message;
        return;
    }
    catch (...) {
        logger << C_Logger::ERROR << "Unexpected Exception : " << configFile << C_Logger::END;
        return;
    }

    DOMNode *configuration = parser->getDocument();
    DOMNode *current = configuration->getFirstChild();
    std::string str = XMLString::transcode(current->getNodeName());

    if (str != "vlcs-conf")
    {
        logger << C_Logger::ERROR << "Bad config file, missing vlcs-conf element" << C_Logger::END;
    }

    current = current->getFirstChild();

    i_channel_max = 0;
    while (current != NULL)
    {
        if (current->getNodeType() == DOMNode::ELEMENT_NODE)
        {
            str = XMLString::transcode(current->getNodeName());
            if (str == "channel")
            {
                i_channel_max++;
                channels.push_back(C_Channel(current));
            }
            else if (str == "group")
            {
                h_hosts.insert(newGroup(current));
            }
        }
        current = current->getNextSibling();
    }

    delete parser;
    delete errHandler;
}
    
std::pair<std::string, std::list<std::pair<std::string, std::string> > > 
    C_ConfigXml::newGroup(DOMNode * node)
{
    std::string name;
    std::string addr;
    std::string mask;
    std::string subnet;
    std::list<std::pair<std::string, std::string> > liste;
    
    DOMNode * current;
    std::string str;

    current = node->getFirstChild();
    while (current != NULL)
    {
        if (current->getNodeType() == DOMNode::ELEMENT_NODE)
        {
            str = XMLString::transcode(current->getNodeName());
            if (str == "name")
            {
                name = XMLString::transcode(
                        current->getFirstChild()->getNodeValue());
            }
            else if (str == "ip")
            {
                addr = XMLString::transcode(
                        current->getFirstChild()->getNodeValue());
                mask = "255.255.255.255";
                liste.push_back(std::pair<std::string, std::string>(addr,mask));
            }
            else if (str == "subnet")
            {
                subnet = XMLString::transcode(
                        current->getFirstChild()->getNodeValue());
                int pos = subnet.find('/');

                addr = subnet.substr(0, pos);
                mask = subnet.substr(pos+1);

                liste.push_back(std::pair<std::string, std::string>(addr,mask));
            }
        }
        current = current->getNextSibling();
    }
    
    return std::pair<std::string, std::list<std::pair<std::string, std::string> > > (name, liste);
}

int C_ConfigXml::GetChannelNumber()
{
    return i_channel_max;
}

bool C_ConfigXml::isAllowed(std::string client, int channel)
{
    in_addr_t addr;
    in_addr_t mask;
    in_addr_t caddr = C_Host::pton(client);
    
    if (channel >= i_channel_max)
    {
        return true;
    }
    
    std::list<std::pair<std::string, C_Channel::type> > 
        groupes = channels[channel].auth;

    
    std::list<std::pair<std::string, C_Channel::type> >::iterator 
        groupe = groupes.begin();
    
    while (groupe != groupes.end())
    {
        std::list<std::pair<std::string, std::string> >
            subnets = h_hosts[(*groupe).first];

        std::list<std::pair<std::string, std::string> >::iterator
            subnet = subnets.begin();
        
            
        while (subnet != subnets.end())
        {
            addr = C_Host::pton((*subnet).first);
            mask = C_Host::pton((*subnet).second);
            if ((addr & mask) == (caddr & mask))
            {
                if ((*groupe).second == C_Channel::ALLOW)
                {
                    return true;
                }
                else
                {
                    logger << C_Logger::WARNING << "Break in attempt from " 
                        << client << C_Logger::END;
                    return false;
                }
            }
           ++subnet;
        }
        ++groupe;
    }
        
    return true;
}

std::string C_ConfigXml::GetChannel(int i_channel)
{
    return channels[i_channel].vlc_string;
}

/////////////////////
// C_Channel implementation

C_Channel::C_Channel()
{
}

C_Channel::C_Channel(DOMNode * node)
{
    DOMNode * current;
    std::string str;

    current = node->getFirstChild();
    while (current != NULL)
    {
        if (current->getNodeType() == DOMNode::ELEMENT_NODE)
        {
            str = XMLString::transcode(current->getNodeName());
            if (str == "name")
            {
                name = XMLString::transcode(
                        current->getFirstChild()->getNodeValue());
            }
            else if (str == "vlc")
            {
                vlc_string = XMLString::transcode(
                        current->getFirstChild()->getNodeValue());
            }
            else if (str == "allow")
            {
                auth.push_back(std::pair<std::string, type>(
                        std::string(XMLString::transcode(current->getFirstChild()->getNodeValue())), ALLOW));
            }
            else if (str == "deny")
            {
                auth.push_back(std::pair<std::string, type>(
                        std::string(XMLString::transcode(current->getFirstChild()->getNodeValue())), DENY));
            }
        }
        current = current->getNextSibling();
    }
    std::cerr << "Adding : " << name << " " << vlc_string << std::endl;
}

C_Channel::~C_Channel()
{
}

