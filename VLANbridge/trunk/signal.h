/* VideoLAN VLANbridge: Signal processing
 *
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr> 23/10/98
 *
 * You dont need to include anything before this file
*/


#ifndef _SIGNAL_H
#define _SIGNAL_H

/* Install the signal handlers */
int SetupSigHandlers();

#endif
