/* VideoLAN VLANbridge: Remote administration commands
 *
 * Definition file
 *

*/


int Help(int iSockFd, int i, struct s_Argument* asaArg);
int Logout(int iSockFd, int i, struct s_Argument* asaArg);
int ServerStatus(int iSockFd, int iArgCount, struct s_Argument* asaArg);
