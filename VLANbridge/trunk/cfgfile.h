/* VideoLAN VLANbridge: Config files parsing
 *
 * Definition file
 *
 * You need to include <NOTHING> before including this file
*/

#ifndef _CFGFILE_H
#define _CFGFILE_H

/* Return codes for Cfg_Read */
#define CFG_OK		0	/* Line was read correctly */	
#define CFG_EOF		1	/* Reached the end of the file */
#define CFG_BEGIN	2	/* Reached the beginning of a section */
#define CFG_END		3	/* Reached the end of a section */
#define CFG_BAD		4	/* Wrong file format */
#define CFG_ERR		5	/* Read error */

FILE* OpenCfg (const char* strFileName);
int ReadCfg (FILE* pfdCfgFile, char** pstrVarName, char** pstrVarVal);
int FindCfg(FILE* pfdCfgFile, const char* strSection);
int CloseCfg (FILE* pfdCfgFile);

#endif
