/* VideoLAN VLANbridge : Kernel ARP table management
 *
 * Definition file
 *
 * You need to include <sys/socket.h> before including this file
*/


#ifndef _CONNECT_H
#define _CONNECT_H


/* Maximum simultaneous incomming connections allowed */
#define MAX_IN_CONN 5

int OpenWaitingSocket (const char* strWaitPort);
int ProcessConnections (int iWaitSockFd);
int HandleDeadSocket (int iSockFd);
int CloseConnections();


#endif
