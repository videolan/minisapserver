/* VideoLAN VLANbridge
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * TO DO: Mettre un timeout a l'identification pour ne pas bloquer le login
	  Mettre des semaphores pour que le programme ne puisse continuer avant que le manager et le listener soient completement initialises : par exemple, si une connection arrive avant que le listener n'en soit au pthread_cond_wait, le signal est perdu et le soft est bloque.
*/


/* For Solaris */
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <netinet/in.h>

#include "define.h"
#include "debug.h"
#include "signal.h"
#include "log.h"
#include "cfgfile.h"
#include "listener.h"
#include "performer.h"
#include "sender.h"
#include "manager.h"
#include "connect.h"
#include "vlanbridge.h"

 
/* Handle datas for the thread managing VLANserver requests */
struct s_Manager manager;
/* Handle datas for the thread receiving the requests from VLANserver */
struct s_Listener listener;
/* Handle datas for the thread sending the answers to the VLANserver */
struct s_Sender sender;


/* Name of the module of the program */
#define MOD_NAME MOD_VLANBRIDGE


int ConfigBridge(FILE* hCfgFile, char** pstrPort)
{
  char* strVarName = NULL;
  char* strVarVal = NULL;
  int iRc = 0;

  ASSERT(hCfgFile);
  ASSERT(pstrPort);

  /* Find the right section */
  iRc = FindCfg(hCfgFile, "Globals");

  /* Read the entries of the section */
  while (iRc == CFG_OK)
  {
    iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);

    if (iRc == CFG_OK && !strcmp(strVarName, "Port"))
    {
      *pstrPort = strVarVal;
      Log(LOG_NOTE, MOD_NAME, "Default port is %s", *pstrPort);
    }
  }
    
  /* Test the code that made the loop terminate */
  if (iRc == CFG_END)
    iRc = 0;

  return iRc;
}


/***************************************************************************/
/* Stop the bridge		       					   */
/***************************************************************************/
int StopBridge(int iErrorStatus)
{
  int iRc = iErrorStatus;
  
  Log(LOG_NOTE, MOD_NAME, "Exiting...");

  /* Stop the listener thread if it is running */
  iRc |= CancelListener(&listener);

  /* Stop the manager if it is running and flush all pending requests */
  iRc |= CancelManager(&manager);

  /* Stop the sender if it is running */
  iRc |= CancelSender(&sender);

  /* Now that the threads are stopped, we can destoy the ressources they
     have and that they can share */
  iRc |= FreeListener(&listener);
  iRc |= FreeSender(&sender);
  iRc |= FreeManager(&manager);

  /* Close all over open ressources */
  iRc |= CloseConnections();
  iRc |= CloseLog();
  
  return iRc;
}


/***************************************************************************/
/* Print an help message and exit             				   */
/***************************************************************************/
void Usage(char* strProg)
{
  ASSERT(strProg);
  
#define HELP ""				\
"Usage : %s\n"				\
"[-c <cfg>]: Config file\n"             \
"[-l <log>]: Log file\n\n"

  printf(HELP, strProg);
  exit(0);
}


/***************************************************************************/
/* VLANbridge main function 						   */
/***************************************************************************/
int main(int argc, char **argv)
{
  int opt_arg;
  int iWaitSockFd;
  char* strWaitPort;
  char* strLogFile;
  char* strCfgFile;
  FILE* pfdCfgFile = NULL;
  int iRc = 0;

  /* Init structures */
  bzero(&manager, sizeof(struct s_Manager));
  bzero(&listener, sizeof(struct s_Listener));
  bzero(&sender, sizeof(struct s_Sender));
  
  /* Check environnement and stop immediately if there is any problem */
  if (geteuid() != 0)
  {
    printf("VLANbridge must be run as root: stopping\n");
    exit(1);
  }

  if (!getprotobyname("tcp"))
  {
    printf("TCP protocol unknown: stopping\n");
    exit(1);
  }

  if (!getprotobyname("icmp"))
  {
    printf("ICMP protocol unknown: stopping\n");
    exit(1);
  }

  /* Startup message */
  if (!iRc)
    printf("Starting VLANbridge version %s\n\n", VERSION);

  /* Variables initialisations */
  if (!iRc)
  {
    /* Setting default values */
    strCfgFile = DEFAULT_CFG;		/* Default config file */
    strLogFile = DEFAULT_LOG;		/* Default log file */
    strWaitPort = DEFAULT_PORT;		/* Default port used to wait for VLANserver */

    /* Parse command line */
    /* First parse optional args */
    opterr = 0;		/* Force getopt not to generate an error message for '?' */
    while ((opt_arg = getopt(argc, argv, "c:l:h")) != EOF)
    {
      switch (opt_arg)
      {
	case 'c':
	  strCfgFile = optarg;
	  break;
	case 'l':
	  strLogFile = optarg;
	  break;
	case 'h':
	{
          /* Display help message and exit immediately */
	  Usage(*argv);
          break;
	}
	default:
	{
	  printf("Invalid '%c' option or missing parameter", optopt);
          /* Display help message and exit immediately */
          Usage(*argv);
	}
      }
    }
    
    /* Now parse the mandatory ones */
    if (argc - optind != 0)
    {
      printf("Invalid %d extra argument(s)", argc - optind);
      Usage(*argv);
      iRc = -1;
    }
  }

  /* Open log file in order be able to trace startup process */
  if (!iRc)
  {
    LogScreen(LOG_NOTE, MOD_NAME, "Opening log file (%s)...", strLogFile);
    iRc = OpenLog(strLogFile);
  }

  /* Init the bridge */
  if (!iRc)
  {
    /* Catch all important signals */

    LogScreen(LOG_NOTE, MOD_NAME, "Initialising signal handlers...");
    iRc = SetupSigHandlers();
  }

  if (!iRc)
  {
    /* Read config file */
    LogScreen(LOG_NOTE, MOD_NAME, "Retriving configuration from %s...", strCfgFile);

    /* Open cfg file for performer */
    pfdCfgFile = OpenCfg(strCfgFile);

    if (pfdCfgFile == NULL)
      iRc = FILE_ERROR;
    else
    {
      /* Global Initialisations */
      iRc = ConfigBridge(pfdCfgFile, &strWaitPort);

      /* Initialisation of the mapping structures used by all performer threads */
      if (!iRc)
        iRc = ConfigPerformer(pfdCfgFile);

      /* Setup routing tables in order the bridge to work */
      if (!iRc)
      { 
        LogScreen(LOG_NOTE, MOD_NAME, "Setting up routing and arp tables...");
        iRc |= InitRouting(pfdCfgFile);
        iRc |= InitArp(pfdCfgFile);
      }

      /* Close cfg file */
      iRc |= CloseCfg(pfdCfgFile);
    }
  }

  if (!iRc)
  {
    /* Create and init the thread that will manage the requests from VLANserver */
    LogScreen(LOG_NOTE,MOD_NAME, "Starting manager thread...");
    iRc = InitManager(&manager);
  }

  if (!iRc)
  {
    /* Create and init the thread that will listen to the requests from VLANserver */
    LogScreen (LOG_NOTE, MOD_NAME, "Starting listener thread...");
    iRc = InitListener(&listener);
  }

  if (!iRc)
  {
    /* Create and init the thread that will answer to VLANserver */
    LogScreen(LOG_NOTE, MOD_NAME, "Starting sender thread...");
    iRc = InitSender(&sender);
  }

  if (!iRc)
  {
    /* Open and initialise the socket who will listen for connections */    
    LogScreen(LOG_NOTE, MOD_NAME, "Setting up connections...");
    iWaitSockFd = OpenWaitingSocket(strWaitPort);

    if (iWaitSockFd <= 0)
      iRc = -1;
  }

  /* Fork the process and stop the parent so that the VLANbridge will run in background */
#ifndef DEBUG  
  if (!iRc)
  {
    switch (fork())
    {
      case -1:		/* Error: stopping now */
      {
	LogScreen(LOG_NOTE, MOD_NAME, "Unable to fork process: %s", strerror(errno));
	iRc = 1;
	break;
      }
     case 0:		/* Current process is the child one: let it work */
      {
	LogScreen(LOG_NOTE, MOD_NAME, "VLANbridge now running in background");
	break;
      }

      default:		/* Current process is the parent one: stop it */
	exit(0);
    }
  }
#endif

  /* The init process is now finished: start to work if there was no error,
     else warn that there was a pb and exit */
  if (iRc)
  {
    /* We just printed before the operation that failed, so don't have to remind it.
       The exit session is common to the normal one */
    LogScreen(LOG_WARN, MOD_NAME, "An error occured: stopping bridge");
  }
  else
  {
    /* Manage incoming connections */
    iRc = ProcessConnections(iWaitSockFd);
  }

  /* Exit process */
  iRc = StopBridge(iRc);

  return iRc;
}
