/* VideoLAN VLANbridge : gratuitous.c
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * Heavily inspired from dhcpcd - DHCP client daemon -
 *   Copyright (C) 1996-1997 Yoichi Hariguchi <yoichi@fore.com>
 *
 * TO DO: 
 */


/* For Solaris */
#include <sys/types.h>
#include <strings.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "log.h"
#include "gratuitous.h"

#define MAC_BCAST	"\xff\xff\xff\xff\xff\xff"
#define IP_BCAST	"\xff\xff\xff\xff"

#define MOD_NAME MOD_PERFORMER


/* ARP message which will be send in the raw socket */
struct s_ARPMsg
{
  struct ether_header ethhdr;   /* Ethernet header */
  u_short htype;	       	/* hardware type (must be ARPHRD_ETHER) */
  u_short ptype;	       	/* protocol type (must be ETH_P_IP) */
  u_char  hlen;		       	/* hardware address length (must be 6) */
  u_char  plen;		       	/* protocol address length (must be 4) */
  u_short operation;	       	/* ARP opcode */
  u_char  sHwaddr[6];	       	/* sender's hardware address */
  u_char  sInaddr[4];	       	/* sender's IP address */
  u_char  tHwaddr[6];	       	/* target's hardware address */
  u_char  tInaddr[4];	       	/* target's IP address */
  u_char  pad[18];	       	/* pad for min Ethernet payload (60 bytes) */
};


/***************************************************************************/
/* Builds the gratuitous ARP message					   */
/***************************************************************************/
int MakeMsg (const char* pTInAddr, const char* pTHwAddr,
             const char* pSInAddr, const char* pSHwAddr,
             /*const char* pSHwSendAddr,*/ struct s_ARPMsg* pamPacket)
{
  bzero(pamPacket, sizeof(*pamPacket));

  bcopy(MAC_BCAST, pamPacket->ethhdr.ether_dhost, 6);	/* MAC destination address */
  bcopy(pSHwAddr/*pSHwSendAddr*/, pamPacket->ethhdr.ether_shost, 6);	/* MAC source address */
  pamPacket->ethhdr.ether_type = htons(ETHERTYPE_ARP);	/* protocol type (Ethernet) */

  pamPacket->htype = htons(ARPHRD_ETHER);		/* hardware type */
  pamPacket->ptype = htons(ETHERTYPE_IP);		/* protocol type (IP) */
  pamPacket->hlen = 6;					/* hardware address length */
  pamPacket->plen = 4;					/* protocol address length */
  pamPacket->operation = htons(ARPOP_REQUEST);		/* ARP op code */

  bcopy(pSHwAddr, pamPacket->sHwaddr, 6);		/* source hardware address */
  bcopy(pSInAddr, pamPacket->sInaddr, 4);		/* source IP address */
  bcopy(pTHwAddr, pamPacket->tHwaddr, 6);		/* target hardware address */
  bcopy(pTInAddr, pamPacket->tInaddr, 4);		/* target IP address */

  return 0;
};


/***************************************************************************/
/* Open and configure a socket to send the gratuitous packet   		   */
/***************************************************************************/
int OpenSocket(int* piSockFd)
{
  int iRc = 0;
  int iOptVal = 1;

  *piSockFd = socket(AF_INET, SOCK_PACKET, htons(ETHERTYPE_ARP));

  if (*piSockFd < 0)
  {
    Log (LOG_ERROR, MOD_NAME, "Unable to open socket to send gratuitous ARP: %s",
         strerror(errno));
    iRc = errno;
  }
  else
  {
    if (setsockopt(*piSockFd, SOL_SOCKET, SO_BROADCAST, &iOptVal, sizeof(&iOptVal)) < 0)
    {
      Log (LOG_ERROR, MOD_NAME, "Unable to configure socket to send gratuitous ARPs: %s",
           strerror(errno));
      iRc = errno;
    }
  }

  return iRc;
}


/***************************************************************************/
/* Send the gratuitous ARP packet 					   */
/***************************************************************************/
int SendMsg (const char* strDev, const struct s_ARPMsg* pamPacket, int iSockFd)
{
  int iRc = 0;
  struct sockaddr saIfName;

  /* Set the interface to use */
  bzero(&saIfName, sizeof(saIfName));
  strcpy(saIfName.sa_data, strDev);

  if (sendto(iSockFd, (char *)pamPacket, sizeof(*pamPacket), 0, &saIfName,
      sizeof(saIfName)) < 0)
  {
    Log(LOG_ERROR, MOD_NAME, "Unable to send gratuitous ARP: %s",
        strerror(errno));
    iRc = errno;
  }
  
  return iRc;
}


/***************************************************************************/
/* Get the IP address of a specified interface				   */
/***************************************************************************/
int GetDevPa (const char* strIfName, struct sockaddr* psaHwAddr, int iSockFd)
{
  int iRc = 0;
  struct ifreq ifrRequest;
  
  /* Get IP address of the device */
  strcpy(ifrRequest.ifr_name, strIfName);

  if (ioctl(iSockFd, SIOCGIFDSTADDR, &ifrRequest) < 0 )
  {
    Log (LOG_ERROR, MOD_NAME, "Unable to retrieve %s IP address: %s",
         strIfName, strerror (errno));
    iRc = errno;
  }
  
  /* Copy it to the sockaddr struct */
  if (!iRc)
    memcpy(psaHwAddr, &(ifrRequest.ifr_hwaddr), sizeof(struct sockaddr));

  return iRc;
};


/***************************************************************************/
/* Get the hardware address of a specified interface			   */
/***************************************************************************/
int GetDevHw (const char* strIfName, struct sockaddr* psaHwAddr, int iSockFd)
{
  int iRc = 0;
  struct ifreq ifrRequest;
  
  /* Get hardware address of the device */
  strcpy(ifrRequest.ifr_name, strIfName);

  if (ioctl(iSockFd, SIOCGIFHWADDR, &ifrRequest) < 0 )
  {
    Log (LOG_ERROR, MOD_NAME, "Unable to retrieve %s MAC address: %s",
         strIfName, strerror (errno));
    iRc = errno;
  }
  
  /* Copy it to the sockaddr struct */
  if (!iRc)
    memcpy(psaHwAddr, &(ifrRequest.ifr_hwaddr), sizeof(struct sockaddr));

  return iRc;
};



/***************************************************************************/
/* Sends a gratuitous ARP                                                  */
/***************************************************************************/
int Gratuitous_Send(const struct sockaddr_in* psaFakeIP,
                    const struct sockaddr* psaFakeHw, const char* strDev)
{
  int iRc = 0;
  struct s_ARPMsg* pamPacket;
//  struct sockaddr saDevHwAddr;
//  struct sockaddr saDevPaAddr;
  int iSockFd;

  /* Open the socket used to send the gratuitous ARP */
  iRc = OpenSocket(&iSockFd);

  /* Get hardware and protocol addresses of the device on which the packet
     will be sent */
//  if (!iRc)
//  {
//    iRc = GetDevHw(strDev, &saDevHwAddr, iSockFd);
//    iRc |= GetDevPa(strDev, &saDevPaAddr, iSockFd);
//  }
  
  /* Build gratuitous ARP packet */
  if (!iRc)
  {
    pamPacket = (struct s_ARPMsg *)malloc(sizeof(struct s_ARPMsg));
    if (pamPacket == NULL)
    {
      Log (LOG_ERROR, MOD_NAME, "Unable to create gratuitous ARP packet");
      iRc = -1;
    }
    else
      iRc = MakeMsg((char *)&psaFakeIP->sin_addr, psaFakeHw->sa_data,
                    (char *)&psaFakeIP->sin_addr, psaFakeHw->sa_data, pamPacket);
  }
  
  /* Send the packet */
  if (!iRc)
    iRc = SendMsg (strDev, pamPacket, iSockFd);
  
  if (iRc)
    Log (LOG_WARN, MOD_NAME, "Gratuitous ARP packet for host %s not sent on %s",
         inet_ntoa (psaFakeIP->sin_addr), strDev);

  /* Close the socket */
  if (close (iSockFd) < 0)
    Log (LOG_ERROR, MOD_NAME, "Unable to close socket");
  
  return iRc;
}
