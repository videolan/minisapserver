/* VideoLAN VLANbridge: Remote administration
 *
 * Definition file
 *
 * You need to include <pthread.h> before including this file
*/




struct s_Admin
{
  /* thread id */
  pthread_t tId;

  /* Handle the sockfd used to communicate with the administrator */
  int iSockFd;
};


/* Parameter storage */
union u_Value
{
  char* strValue;
  int iValue;
  float fValue;
  struct sockaddr_in saValue;
};


/* Command argument */
struct s_Argument
{
  char* strName;
  int iArgType;
  union u_Value uVal;
};

/* Argument types */
#define ARG_STANDALONE  128     /* Standalone arg, of form <Val> or [Val] */
#define ARG_NAMED       0       /* Optional named arg, of form [-strName {Val}] */

/* Type of the value stored in this arg */
#define ARG_TYPE_NONE   0       /* Option has no associated value */
#define ARG_TYPE_STRING 1       /* Option must be followed by a string */
#define ARG_TYPE_INT    2       /* Option must be followed by an integer */
#define ARG_TYPE_FLOAT  4       /* Option must be followed by a float */
#define ARG_TYPE_IP     8       /* Option must be followed by an IP address */


/* Command definition */
struct s_Command
{
  /* Command definition */
  char* strName;
  char* strArgDescr;

  /* Execution */
  int (*handler)(int iSockFd, int iArgNumber, struct s_Argument* asaArg);

  /* Help information */
  char* strSummary;
  char* strOptions;
  char* strUsage;
};


/* Commands return code */
#define CMD_CLOSE       -2
#define CMD_TIMEOUT     -3
#define CMD_NONE        -4
#define CMD_BAD         -5



void* AdminThread(void* arg);
