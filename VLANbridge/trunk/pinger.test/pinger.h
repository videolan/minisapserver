/* VideoLAN VLANbridge : Pinger module
 *
 * Definition file
 *
 * You need to include <sys/socket.h> before including this file
*/


#ifndef _PINGER
#define _PINGER

struct s_Pinger
{
  int iSockFd;
  struct sockaddr_in saHostAddr;
  int iIdent;
  u_char* strPacket;
};


int Pinger_Init (const struct sockaddr_in* psaHostAddr, struct s_Pinger* pspConfig);
int Pinger_DetectMove (const struct s_Pinger* pspPinger, int* piHasMoved);
int Pinger_CheckMove (const struct s_Pinger* pspPinger, int* piMoveOk);
int Pinger_Destroy (struct s_Pinger* pspPinger);

#endif
