/* VideoLAN VLAN bridge
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * To-do-list : Create a FIFO for communications with others threads
                Make a database that holds the state of each computer
*/


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "debug.h"
#include "log.h"
#include "convert.h"
#include "listener.h"
#include "performer.h"
#include "sender.h"
#include "base.h"
#include "manager.h"
#include "define.h"
#include "fifo.h"
#include "common.h"
#include "define.h"


/* Used to communicate with the sender thread */
extern struct s_Sender sender;
/* DfltVLAN got from cfgfile */
extern int iVLANDflt;
/* VLAN/interfaces map */
extern char** strMapToDev;

#define MOD_NAME MOD_MANAGER


/***************************************************************************/
/* Warn the manager							   */
/***************************************************************************/
int WarnManager(struct s_Manager* psmManager, struct s_Request* psrJob,
                int iJobType)
{
  int iRc = 0;

  ASSERT(psmManager);
  ASSERT(psrJob);  

  /* Set the next action to performed */
  psrJob->iNextStep = iJobType;

  /* Push the request in the manager FIFO */
  iRc = FifoPush(psmManager->psfWaitingJobs, psrJob);

  return iRc;
}


/***************************************************************************/
/* */
/***************************************************************************/
int StartTask (struct s_Manager* psmManager, struct s_Request* psrRequest)
{
  int iRc = 0;
  struct s_Record* psrHostStatus = NULL;

  ASSERT(psmManager);
  ASSERT(psrRequest);

  /* No potential misconfiguration detected so far */
  psrRequest->iMisconfig = 0;

  /* Check the request */
  if (psrRequest->iVLANsrc == psrRequest->iVLANdst)
  {
    /* The request may have been sent again because a problem appeared the 1st time*/
    psrRequest->iMisconfig = 1;
  }
  if (!strcmp(strMapToDev[psrRequest->iVLANsrc], "") ||
      !strcmp(strMapToDev[psrRequest->iVLANdst], ""))
  {
    /* No interface on that VLAN */
    iRc = BRIDGE_UNABLE;
    Log(LOG_NOTE, MOD_NAME, "Aborting request for host %s: no interface on VLAN %d or %d",
        inet_ntoa(psrRequest->saClientIP.sin_addr), psrRequest->iVLANsrc, psrRequest->iVLANdst);
  }

  /* Check if an entry already exists for this host */
  psrHostStatus = FindInBase(psmManager->psbBase, &psrRequest->saClientIP);

  if (psrHostStatus != NULL)
  {
    /* Don't want to deal with 2 requests for the same host at the same time */
    if (psrHostStatus->iThread != 0)
    {
      iRc = BRIDGE_BUSY;
      Log(LOG_NOTE, MOD_NAME, "Aborting request for host %s: already dealing with it",
          inet_ntoa(psrRequest->saClientIP.sin_addr));
    }
    else
    {
      /* Store the address of the field to avoid a new lookup in the base */
      psrRequest->psRecord = psrHostStatus;

      /* Compare the request with the data in the stack and modify it if needed */
      if (psrRequest->iVLANsrc != psrHostStatus->iCurrentVLAN)
      {
        Log(LOG_WARN, MOD_NAME, "%s is on VLAN %d in our database, in VLAN %d for VLANserver",
            inet_ntoa(psrRequest->saClientIP.sin_addr), psrHostStatus->iCurrentVLAN,
            psrRequest->iVLANsrc);
        psrRequest->iMisconfig = 1;
        psrRequest->iVLANsrc = psrHostStatus->iCurrentVLAN;
      }
    }
  }
  else
  {
    /* Check for potential further problems */
    if (psrRequest->iVLANsrc != iVLANDflt)
    {
      /* For us, this host is on DfltVLAN whatever the VLANserver says */
      psrRequest->iVLANsrc = iVLANDflt;
      /* But this could lead to pbs */
      psrRequest->iMisconfig = 1; 
    }
 
    /* Add an entry in the base and store its address in the request */
    Log(LOG_NOTE, MOD_NAME, "Host does not exist in database: adding it");
    psrRequest->psRecord = malloc(sizeof(struct s_Base));
    psrRequest->psRecord->saHost = psrRequest->saClientIP;
    psrRequest->psRecord->iCurrentVLAN = iVLANDflt;
    iRc = AddToBase(psmManager->psbBase, psrRequest->psRecord);
  }

  if (!iRc)
  {
    if (psrRequest->iVLANsrc == psrRequest->iVLANdst)
    {
      /* The request may have been sent again because a problem appeared the 1st time*/
      psrRequest->iMisconfig = 1;
    }

    /* Run the performer */
    psrRequest->psRecord->iThread = 1;		/***Could set the thread id here instead***/
    iRc = RunPerformer(psrRequest);		/* Create the performer thread */
  }
  else
    Log(LOG_ERROR, MOD_NAME, "Could not add entry for %s to the database",
        inet_ntoa(psrRequest->saClientIP.sin_addr));
  
  return iRc;
}


/***************************************************************************/
/* */
/***************************************************************************/
int FinishTask (struct s_Manager* psmManager, struct s_Request* psrTask)
{
  int iRc = FreePerformer(psrTask);

  ASSERT(psmManager);
  ASSERT(psrTask);

#ifdef DEBUG
  Log(LOG_NOTE, MOD_NAME, "Performer status = %d, %s, %s, %d, %d",
      psrTask->iStatus, "<MAC not human readable>",
      inet_ntoa(psrTask->saClientIP.sin_addr), psrTask->iVLANdst, psrTask->iVLANsrc);
#endif

  /* Save client status to database */
  if (psrTask->iStatus != VLAN_ERROR && psrTask->iStatus != VLAN_ROUTE_NOMOVE)
    psrTask->psRecord->iCurrentVLAN = psrTask->iVLANdst;
  /* else 
     the configuration is the same as before the request, nothing to save */
  
  /* We now accept another request for that client */
  psrTask->psRecord->iThread = 0;

  /* Warn VLANserver */
  iRc = WarnSender(&sender, psrTask);

  return iRc;
}


/***************************************************************************/
/* Wait for all tasks the manager has to monitor to finish		   */
/***************************************************************************/
int FinishAllTasks(struct s_Manager* psmConfig)
{
  int iRc = 0, iIndex = 0;
  int iRunningPerformers = 0;
  struct s_Request* psrFinishedJob = NULL;

  ASSERT(psmConfig);
  
  /* Count performers that are currently running */
  if (GetBaseSize(psmConfig->psbBase) > 0)
  {
    for(iIndex = 0; iIndex < GetBaseSize(psmConfig->psbBase); iIndex++)
    {
      if (GetBaseRecord(psmConfig->psbBase, iIndex)->iThread != 0)
        iRunningPerformers++;
    }
  }
  
  Log(LOG_NOTE, MOD_NAME, "Waiting for the %d running performers to finish",
      iRunningPerformers);

  while (iRunningPerformers > 0)
  {
    /* Wait for a running performer end signal*/
    iRc = FifoPull(psmConfig->psfWaitingJobs, &psrFinishedJob);

    if (!iRc)
    {
      /* Check that the request is valid (ie comes from a performer) */

      if (psrFinishedJob->iNextStep != CLEAN_PERFORMER)
	Log(LOG_WARN, MOD_NAME, "Manager is shutting down: ignoring all requests");
      else
      	iRc = FinishTask(psmConfig, psrFinishedJob);
    }

    /* Stop now if an error occured */
    if (iRc)
    {
      Log(LOG_ERROR, MOD_NAME, "XXXX");
      break;
    }
  }

  return iRc;
}


/***************************************************************************/
/* */
/***************************************************************************/
#define psmConfig ((struct s_Manager *)arg)

void* ManagerThread (void *arg)
{
  struct s_Request* psrTask = NULL;
  int iRc = 0;
  int iMustStop = 0;

  ASSERT(arg);
  
#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "Manager thread is running");
#endif

  while (!iMustStop)
  {
//    printf("New Loop\n");

    /* Get a message and wait for one if the FIFO is empty */
    iRc = FifoPull(psmConfig->psfWaitingJobs, &psrTask);
    
//    printf("Msg got\n");

    if (!iRc)
    {
//      printf ("received signal: %d\n", psrTask->iNextStep);
    
      /* Test the predicate for maybe better response time when telneting the server */
      switch (psrTask->iNextStep)
      {
	case EXIT_MANAGER:		/* The manager must terminate */
	{
	  free(psrTask);
	  iRc = FinishAllTasks(psmConfig);
	  if (iRc)
	    Log(LOG_WARN, MOD_NAME, "Manager could not stop all performer");
	  else
	    Log(LOG_NOTE, MOD_NAME, "All performers have been stopped");

	  iMustStop = 1;
	  break;
	}
      
	case ADMIN_REQUEST:
	{
	  Log(LOG_ERROR, MOD_NAME, "Admin call not yet supported");
	  break;
	}
      
	case START_PERFORMER:		/* New message from VLANserver */
	{
	  iRc = StartTask(psmConfig, psrTask);
	  if (iRc)
	  {
	    Log(LOG_ERROR, MOD_NAME, "Unable to process VLANserver request for host %s",
                inet_ntoa(psrTask->saClientIP.sin_addr));
	    /* Warn bridge */
	    psrTask->iStatus = VLAN_ERROR; 
	    iRc = WarnSender(&sender, psrTask);
	  }
	  break;
	}
	
	case CLEAN_PERFORMER:
	{
	  iRc = FinishTask(psmConfig, psrTask);
	  if (iRc)
	    Log(LOG_ERROR, MOD_NAME, "Unable to finish task for host %s",
                inet_ntoa(psrTask->saClientIP.sin_addr) );	
	  break;
	}
      
	default:
	  Log(LOG_ERROR, MOD_NAME, "Received an unknown signal, ignore it");
      }
    }
  }

  /* Exit from the thread returning the NULL value */
#ifdef DEBUG
  Log(LOG_NOTE, MOD_NAME, "Manager thread is exiting");
#endif
  pthread_exit(NULL);
}


/***************************************************************************/
/* */
/***************************************************************************/
int InitManager (struct s_Manager* psmManager)
{ 
  int iRc = 0;

  ASSERT(psmManager);

  /* Stack initialisation */
  psmManager->psbBase = CreateBase();
  if (psmManager->psbBase == NULL)
  {
    Log(LOG_ERROR, MOD_NAME, "Unable to create manager stack");
    iRc = -1;
  }

//  printf("Manager base (%p) size: %d\n", psmManager->psbBase, GetBaseSize(psmManager->psbBase));

  /* FIFO initialisation */
  psmManager->psfWaitingJobs = FifoCreate();
  if (psmManager->psfWaitingJobs == NULL)
  {
    Log(LOG_ERROR, MOD_NAME, "Unable to create manager fifo");
    iRc = -1;
  }
  
  /* Thread initialisations */
  if (!iRc)
  {
    if ((iRc = pthread_mutex_init (&(psmManager->WorkLock), NULL)))
      Log (LOG_ERROR, MOD_NAME, "Could not init mutex: %s", strerror(iRc));
    else if ((iRc = pthread_cond_init (&(psmManager->WorkSignal), NULL)))
      Log (LOG_ERROR, MOD_NAME, "Could not init condition: %s", strerror(iRc));
  }

  /* Now create the thread used to manage VLAN server requests */
  if (!iRc)
  {
    iRc = pthread_create(&(psmManager->tId), NULL, ManagerThread, psmManager);

    if (iRc)
      Log (LOG_ERROR, MOD_NAME, "Manager thread not created: %s",
           strerror(iRc));
    else
      Log (LOG_NOTE, MOD_NAME, "Manager thread sucessfully created: id %d",
           psmManager->tId);
  }

  return iRc;
}


/***************************************************************************/
/* Send a shutdown message to the manager */
/* A revoir completement */
/***************************************************************************/
inline int CancelManager(struct s_Manager* psmManager)
{
  int iRc = 0;
  struct s_Request* psrRequest = NULL;

  ASSERT(psmManager);

  if (psmManager->tId == 0)
    Log(LOG_WARN, MOD_NAME, "Could not stop manager, none is running");
  else
  {
    Log(LOG_NOTE, MOD_NAME, "Stopping manager thread");
    
    psrRequest = malloc(sizeof(struct s_Request));
    iRc = pthread_mutex_lock (&psmManager->WorkLock);

//    printf ("Got lock\n"); 

    if (iRc)
      Log(LOG_ERROR, MOD_NAME, "Could not lock mutex: %s", strerror(iRc));
    else
    {
      /* Warn the manager */
      iRc = WarnManager(psmManager, psrRequest, EXIT_MANAGER);
    
      if (iRc)
	Log(LOG_ERROR, MOD_NAME, "Unable to send manager termination signal: %s",
	 strerror(iRc));
      else
      {
        /* Synchronise ourselves wit the destruction of the manager */
        if (pthread_join(psmManager->tId, NULL))
        {
          iRc = -1;
          Log(LOG_ERROR, MOD_NAME, "Unable to join manager thread");
        }
        else
          Log(LOG_NOTE, MOD_NAME, "Manager thread is now stopped");
      }
      
      if (pthread_mutex_unlock (&psmManager->WorkLock))
      {
	iRc |= -1;
	Log(LOG_ERROR, MOD_NAME, "Could not unlock mutex: %s");
      }
    }
  }

//  printf("Exited from cancel manager\n");
  

  return iRc;
}


/***************************************************************************/
/* Free all ressources used by the performer thread			   */
/* Beware: the thread must have been stopped before calling this function  */
/***************************************************************************/
int FreeManager(struct s_Manager* psmManager)
{
  int iRc = 0;

  ASSERT(psmManager);

  if (psmManager->tId != 0)
  {
    iRc = DestroyBase(psmManager->psbBase);
    if (iRc)
      Log(LOG_ERROR, MOD_NAME, "Unable to free manager stack");

//    printf("Base destroyed\n");
    
    iRc = FifoDestroy(psmManager->psfWaitingJobs);
    if (iRc)
      Log(LOG_ERROR, MOD_NAME, "Unable to free manager FIFO");

    iRc |= pthread_mutex_destroy (&psmManager->WorkLock);
    iRc |= pthread_cond_destroy (&psmManager->WorkSignal);

    if (iRc)
      Log (LOG_ERROR, MOD_NAME, "Unable to free the ressources used by manager thread");

//    printf("Mutex and cond destroyed\n");

  }
  else
    Log(LOG_WARN, MOD_NAME, "Could not destroy manager, none has been created");

  /* No more manager is running */
  psmManager->tId = 0;

//printf("Exited from destroy manager\n");

  return iRc;
}
