#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>

#include "arp.h"

int sockfd=0;     /* active socket descriptor */
 

int main(int argc, char **argv)
{
   char host[] = "bs.via.ecp.fr";

   struct sockaddr_in target;
   unsigned long inaddr;
   struct hostent *hp;
   int i = 0;
   

   bzero (&target, sizeof (struct sockaddr_in));
   target.sin_family = AF_INET;

   if ( (inaddr = inet_addr(host)) != INADDR_NONE)
    { /* it's dotted-decimal */
      bcopy(&inaddr, &target.sin_addr, sizeof(inaddr));
    }
    else
    { if ( (hp = gethostbyname(host)) == NULL)
      return -1;
      bcopy(hp->h_addr, &target.sin_addr, hp->h_length);
    };

  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) <0)
  {
    printf ("Can't open socket\n");
    exit(-1);
  };

  printf("Sockfd : %d\n", sockfd); 


  i = Arp_Add (&target, "eth0", sockfd);
  printf ("1: %d\n", i);
  i = Arp_Add (&target, "eth0", sockfd);  
  printf ("2: %d\n",i) ;
  sleep (10);
  i = Arp_Del (&target, "eth0", sockfd);
  printf ("3: %d\n", i);

  return (0);
}
