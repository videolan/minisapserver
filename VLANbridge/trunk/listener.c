/* VideoLAN VLAN bridge : Listener thread
 *
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr>
 *
 * To-do-list : Add a lock on the iConnectionOK and iSockfd variables
		The listener receives only one caracter at a time : very slow
 */


/* For Solaris */
#include <strings.h>
#include <errno.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

#include "debug.h"
#include "log.h"
#include "convert.h"
#include "performer.h"
#include "manager.h"
#include "connect.h"
#include "sender.h"
#include "listener.h"
#include "common.h"

#define MOD_NAME MOD_LISTENER

/* Used to communicate with the manager thread */
extern struct s_Manager manager;
/* Used to communicate with the sender thread */
extern struct s_Sender sender;

/* Connection state */
extern int iConnectionState;
extern pthread_mutex_t ConnectionLock;



/*****************************************************************************/
/* Create the thread used to handle VLANserver requests			     */
/*****************************************************************************/
int InitListener(struct s_Listener* listener)
{
  /* Thread initialisations */
  int iRc = pthread_cond_init (&listener->WakeUpSignal, NULL);

  ASSERT(listener);

  /* Now create the thread used to listen to the VLANserver requests */
  if (!iRc)
  {
    iRc = pthread_create(&(listener->tid), NULL, ListenerThread, listener);
        
   if (!iRc)
     Log (LOG_NOTE, MOD_NAME, "Listener thread successfully created: id %d",
          listener->tid);
  }

  return iRc;
}


/*****************************************************************************/
/*			     */
/*****************************************************************************/
int ParseMsg (char* strMsg, struct s_Request* psrBuf)
{
  int iRc = 0;
  char strIP[16];
  char strMAC[17];

  ASSERT(strMsg);
  ASSERT(psrBuf);

  /* Init the buffers */
  bzero((char *)psrBuf, sizeof (struct s_Request));
  bzero(strIP, 16);
  bzero(strMAC, 17);

  /* Parse received request */ 
  if (sscanf(strMsg, "%d %d %s %s %d %d", &psrBuf->iType, &psrBuf->iRequestId,
      strMAC, strIP, &psrBuf->iVLANdst, &psrBuf->iVLANsrc) != 6)
  {
    /* Message did not respect the good format */
    iRc = 1;
  }

  /* Convert IP */
  if (!iRc)
  {
    psrBuf->saClientIP.sin_family = AF_INET;
    iRc = !inet_aton(strIP, &(psrBuf->saClientIP.sin_addr));
  }
  
  /* Convert MAC */
  if (!iRc)
    iRc = mac_atob(strMAC, (unsigned char *)&psrBuf->saClientMAC.sa_data);

  return iRc;
}


/*****************************************************************************/
/*			     */
/*****************************************************************************/
int GetSocket(struct s_Listener* pslListener)
{
  int iRc = pthread_mutex_lock(&ConnectionLock);
  
  ASSERT(pslListener);

  if (!iRc)
  { 
    if (iConnectionState == 0)
    {
      /* Wait for the signal of the main thread */
      iRc = pthread_cond_wait(&pslListener->WakeUpSignal, &ConnectionLock);
    }

    iRc |= pthread_mutex_unlock(&ConnectionLock);
  }
 
  return iRc;
}


/*****************************************************************************/
/* */
/*****************************************************************************/
int Receive(struct s_Listener* share)
{
  int iRc = 0;
  char* strMsg = calloc(VLAN_MSG_LEN, sizeof (char));
  struct s_Request* psrRequest = NULL;
  int iConnectionOK = 1;
  int iIndex;

  pthread_cond_t condl;
  pthread_mutex_t mutexl;
  pthread_mutex_init (&mutexl, NULL);
  pthread_cond_init (&condl, NULL);

  ASSERT(share);

  Log (LOG_NOTE, MOD_NAME, "Waiting for VLANserver requests");

  while (iConnectionOK > 0)
  {
    for (iIndex = 0; iIndex < VLAN_MSG_LEN; iIndex++)
    {
      /* We need those testcancel() for classical system calls are not
         cancellation points at the present time */
      pthread_testcancel();
      iConnectionOK = recv(share->iSockFd, strMsg+iIndex, 1, 0);
      pthread_testcancel();

      /* Check what we received */
      if (iConnectionOK <= 0)
      { 
	/* Connection lost: register error and exit the for loop */
	Log(LOG_ERROR, MOD_NAME, "Recv from VLANserver error: %s", strerror(errno));
	iRc = errno;
	break;
      }

      if (*(strMsg+iIndex) == '\n')
      {
	/* End of the message */
	Log (LOG_NOTE, MOD_NAME, "Request received from VLANserver: %s", strMsg);

	/* The memory will persist until the performer has ahieve his job */
	psrRequest = malloc(sizeof(struct s_Request));

	/* Translate msg in VLANbridge binary language */
	if (ParseMsg(strMsg, psrRequest))
	{
	  Log(LOG_NOTE, MOD_NAME, "Bad request received from VLANserver");
	  
	  /* Warn the VLANserver */
	  psrRequest->iStatus = VLAN_ERROR;
	  iRc = WarnSender(&sender, psrRequest);	  
	}
	else
	{
	  /* Warn the manager */
	  iRc = WarnManager(&manager, psrRequest, START_PERFORMER);

	  if (iRc)
	    Log(LOG_WARN, MOD_NAME, "An error occured when warning the manager of a request: request may be lost");
	}
	
	/* Loop to wait for new message */
	bzero (strMsg, VLAN_MSG_LEN);
	break;
      }
    }
  }
  
  return iRc;
}



/*****************************************************************************/
/* Thread listening for the VLANserver requests 			     */
/*****************************************************************************/
#define listener ((struct s_Listener *)arg)

void *ListenerThread(void *arg)
{
  /* True until the thread must be stopped */
  int iMustLoop = 1;
  int iRc = 0;

  ASSERT(arg);

  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  
#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "Listener thread is running");
#endif

  while (iMustLoop)
  {
    /* Get the sockfd on which one must read */
    iRc = GetSocket(listener);

    /* Deal with VLANserver requests */
    if (!iRc)
    {
      iRc = Receive(listener);

      /* Handle connection losts if needed */
      if (iRc)
      {
	Log (LOG_WARN, MOD_NAME, "Connection with VLANserver lost");

	/* Enter exclusive execution zone */
	iRc = pthread_mutex_lock(&ConnectionLock);

	if (!iRc)
	{
	  if (iConnectionState == 0 || iConnectionState != listener->iSockFd)
	  {
	    /* The sender has already handled the connection lost, and the VLANserver either is
	       waiting for a new connection (iIsConnected == 0) or has already set up another
	       connection: so there is nothing to do */
	  }
	  else
	    iRc = HandleDeadSocket(listener->iSockFd);
	}
 
	/* Leave exclusive execution zone */
	iRc |= pthread_mutex_unlock(&ConnectionLock);

	if (iRc)
	  Log(LOG_ERROR, MOD_NAME, "Connection lost could not be correctly handled by listener because of errors");
      }
    }
    else
    {
      Log(LOG_WARN, MOD_NAME, "Unable to get a valid socket file descriptor, retrying");
      /* Just loop */
    }
  }

  /* Exit the thread returning the NULL value */
  pthread_exit(NULL);
}
    

/*****************************************************************************/
/* Stop the running listener thread    					     */
/*****************************************************************************/
int CancelListener (struct s_Listener* pslListener)
{
  int iRc = 0;

  ASSERT(pslListener);

  if (pslListener->tid != 0)
  {
    Log(LOG_NOTE, MOD_NAME, "Stopping listener thread");
    iRc = pthread_cancel(pslListener->tid);
//    printf("listner canceled with iRc %d and error: %s\n", iRc, strerror(iRc));
    /* Synchronise ourselves with the destruction of the thread */
    iRc |= pthread_join(pslListener->tid, NULL);
//    printf("listner joined with iRc %d and error: %s\n", iRc, strerror(iRc));
    /* When the thread is cancelled while waiting for a a socket, it relocks
       the ConnectionLock, so that the sender thread cannot anymore work for
       it shares the ConnectionLock with the listener */
    pthread_mutex_trylock(&ConnectionLock);
    pthread_mutex_unlock(&ConnectionLock);
  }
  else
    Log(LOG_WARN, MOD_NAME, "Could not stop listener, none is running");

  return iRc;
}


/*****************************************************************************/
/* Free all ressources used by the running listener thread     		     */
/*****************************************************************************/
int FreeListener(struct s_Listener* pslListener)
{
  int iRc = 0;

  ASSERT(pslListener);

  if (pslListener->tid != 0)
  {
    iRc |= pthread_cond_destroy(&pslListener->WakeUpSignal);
//    printf("Listener cond destroyed with iRc %d and error: %s\n", iRc, strerror(iRc));

    /* No more listener is running */
    pslListener->tid = 0;

    if (!iRc)
      Log(LOG_NOTE, MOD_NAME, "Listener thread destroyed");
    else
      Log(LOG_WARN, MOD_NAME, "Listener thread not cleanly destroyed");
  }
  else
    Log(LOG_WARN, MOD_NAME, "Could not destroy listener, none has been created");
  
  return iRc;
}

