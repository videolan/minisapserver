/* VideoLAN VLAN bridge : manage the list used by the manager to access performer threads
 *
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr>
 *
 * To-do-list : Everything
 */


#include <stdlib.h>
#include <stdio.h>

#include "debug.h"
#include "list.h"

/* Remove element from the list */
void remove_from_list (struct list *l, struct cell* pos)
{
  ASSERT(l);
  ASSERT(pos);
  
  /* Rebuild all links */
  pos->previous->next = pos->next;
  pos->next->previous = pos->previous;

  /* Delete element */
  free (pos);

  l->size --;
};


int find_in_list (struct list *l, pthread_t id, struct cell **answer)
/* Find element in the list */
{ int i;
  struct cell *c = l->first.next;

  ASSERT(l);
  ASSERT(answer);

  /*printf ("Seeking the element (Beginning at %p)\n", l->first.next);*/
  if (l->size <= 0)
     return -1;

  for (i = 0; i < l->size; i++)
  { if ( c->performer->tid == id)
    { *answer = c;
      /*printf ("Element found at %p\n", *answer);*/
      return 1;
    };
    c = c->next;
    /*printf ("Now looking at %p\n", c->next);*/
  };

  return 0;
};


struct list * create_list ()
{ struct list *l;
  l = (struct list *) malloc (sizeof (struct list));
  
  if (l > 0)
  { l->size = 0;

    l->last.next = NULL;       /* 'Next' member of 'last' element in the list always points to NULL */
    l->first.previous = NULL; /* 'Previous' member of 'first' element in the list always points to NULL */

    l->first.next = &l->last;
    l->last.previous = &l->first;

    return l;
  };

  return NULL;
};


void destroy_list (struct list *l)
{ 
  ASSERT(l);
  
  while (l->size > 0)
  { remove_from_list (l, l->first.next);
    l->size --;
  };

  free (l); 
};


int add_to_list (struct list *l, struct s_performer *p)
/* Add an element to the end of the list (before the 'last' element) and return its address */
{
  struct cell *c = (struct cell *)malloc (sizeof (struct cell));

  ASSERT(l);
  ASSERT(p);

  if (!c)
  {
    /* Coud'nt allocate memory space */
    log(LOG_WARN, "Can't create member");
    return -1;
  }
  else
  {
    /*printf ("member = %p\n", c);*/
    c->next = &l->last;
    c->previous = l->last.previous;

    (l->last.previous)->next = c; /* Initialise the former last element (the one before the 'last' member */

    l->last.previous = c;         /* 'next' member still points to NULL */

    l->size ++;

    c->performer = p;             /* Tell the address of the s_performer structure */
    return 1;
  };
};


int delete_from_list (struct list *l, pthread_t id)
/* Remove element given by its pid from the list */
{ 
  struct cell *pos;
 
  ASSERT(l);
  
  switch (find_in_list (l, id, &pos))
  { 
    case 1 : 
      /*printf ("Removing element at %p\n", pos);
      remove_from_list (l, pos);
      printf ("Element removed from list\nList is now : %p, %d, %p, %p\n", l, l->size, l->first.next, l->last.previous);*/
      break;

    case 0 :
      log(LOG_WARN, "Element was not found\n");
      break;

    default :
      /* Big pb !! */
  };
  return -1;
};
