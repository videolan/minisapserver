/* VideoLAN VLANbridge: main file
 *
 * Definition file
 *
 * You don't need to include anything before including this file
 */


#ifndef _VLANBRIDGE_H
#define _VLANBRIDGE_H


/* Default server port */
#define DEFAULT_PORT "2000"

/* Default log file */
#define DEFAULT_LOG "VLANbridge.log"

/* Default config file */
#define DEFAULT_CFG "VLANbridge.cfg"

/* Software version */
#define VERSION "1.1"

/* */
int StopBridge(int iRc);

#endif
