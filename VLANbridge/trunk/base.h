/* VideoLAN VLANbridge: internal database
 *
 * Definition file
 *
 * You need to include <pthread.h> and "performer.h" to use this module
*/

#ifndef _BASE_H
#define _BASE_H


/* Database structure definition */
struct s_Cell
{ struct s_Cell* pcNext;
  struct s_Cell* pcPrevious;
  struct s_Record* psrJob;
};

/* Database definition */
struct s_Base
{ int iSize;
  struct s_Cell cFirst; /* Not used to store datas but to improve performance */
  struct s_Cell cLast;  /* id */
};

/* Data stored in database */
struct s_Record
{
  struct sockaddr_in saHost;	/* Name of the host */
  int iCurrentVLAN;		/* His VLAN */
  int iThread;			/* Thread handling a request for this host (if any) */
};


/* Functions used to manipulate the list */
struct s_Base* CreateBase();
int DestroyBase();

int AddToBase(struct s_Base* psbBase, struct s_Record* psrJob);
int DeleteFromBase(struct s_Base* psbBase, struct sockaddr_in psaHost);
struct s_Record* FindInBase(struct s_Base* psbBase, struct sockaddr_in* psaHost);
int GetBaseSize(struct s_Base* psbBase);
struct s_Record* GetBaseRecord(struct s_Base* psbBase, unsigned int iPos);

#endif
