/* VideoLAN VLAN bridge : pinger
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr> 24/10/98
 *
 * To DO: Faire une vraie verification du paquet recu en reponse
*/


#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "debug.h"
#include "log.h"
#include "pinger.h"

#define PINGER_PAYLOAD 10
#define PINGER_QUERYSIZE (sizeof(struct icmphdr) + PINGER_PAYLOAD)
#define PINGER_ANSWERSIZE (sizeof(struct iphdr) + sizeof(struct icmphdr) + PINGER_PAYLOAD)

#define PINGER_RETRIES 20
#define PINGER_TIMEOUT 500000

#define MOD_NAME MOD_PERFORMER


/****************************************************************************/
/* Compute checksums for ICMP headers 					    */
/****************************************************************************/
unsigned short IcmpChksum (u_short* puAddr, int iLen)
{
  register int nleft = iLen;
  register int sum = 0;
  u_short answer = 0;

  ASSERT(puAddr);

  while (nleft > 1)
  {
    sum += *puAddr++;
    nleft -= 2;
  }
  if (nleft == 1)
  {
    *(u_char *)(&answer) = *(u_char *)puAddr;
    sum += answer;
  }
  sum = (sum >> 16) + (sum + 0xffff);
  sum += (sum >> 16);
  answer = ~sum;
  
  return answer;
}


/****************************************************************************/
/* Test the validity of an answer					    */
/****************************************************************************/
int CheckAnswer(char* strPacket, const struct s_Pinger* pspConfig)
{
  struct ip* pIpHeader;
  struct icmp* pIcmpHeader;
  int iHlen;
  int iIsOk = 0;
  
  ASSERT(strPacket);
  ASSERT(pspConfig);
  
  /* Check the IP header */
  pIpHeader = (struct ip *)strPacket;
  iHlen = pIpHeader->ip_hl << 2;

  /* Now Check the ICMP header */
  pIcmpHeader = (struct icmp *)(strPacket + iHlen);
  if (pIcmpHeader->icmp_type == ICMP_ECHOREPLY)
  {
    if (pIcmpHeader->icmp_id == pspConfig->iIdent)
      iIsOk = 1;
  }

  return iIsOk;
}


/***************************************************************************/
/* Send a ping the remote host using interface corresponding to local_addr */
/***************************************************************************/
int Pinger (const struct s_Pinger* pspConfig)
{
  int iRc = 0;

  ASSERT(pspConfig);

  /* Send the packet */
  if (sendto(pspConfig->iSockFd, pspConfig->strPacket, PINGER_QUERYSIZE, 0,
      (struct sockaddr *)&(pspConfig->saHostAddr),
      sizeof(struct sockaddr)) < 0)
  {
    Log(MOD_NAME, LOG_WARN, "Unable to send ping packet to host %s: %s\n",
        inet_ntoa(pspConfig->saHostAddr.sin_addr), strerror(errno));
    iRc = errno;
  }
  
  return iRc;
};


/***************************************************************************/
/* Create the socket and fill in the packet used by the pinger 		   */
/***************************************************************************/
int Pinger_Init (const struct sockaddr_in* psaHostAddr, struct s_Pinger* pspConfig)
{
  int iRc = 0;
  struct icmphdr* pIcmpHeader;
  int len = sizeof(struct sockaddr);
  
  ASSERT(psaHostAddr);
  ASSERT(pspConfig);

  /* Set the host address */
  memcpy(&pspConfig->saHostAddr, psaHostAddr, sizeof(struct sockaddr_in));

  /* Set the Id of the process */
  pspConfig->iIdent = getpid()&0xFFFF;
  
  /* Create the ping packet */
  pspConfig->strPacket = calloc(PINGER_QUERYSIZE, sizeof(char));
  if (pspConfig->strPacket == NULL)
  {
    Log(MOD_NAME, LOG_WARN, "Unable to create packet to ping host %s\n",
        inet_ntoa(psaHostAddr->sin_addr));
    iRc = -1;
  }
  else
  {
    bzero(pspConfig->strPacket, sizeof(pspConfig->strPacket));
    pIcmpHeader = (struct icmphdr *)pspConfig->strPacket;
    pIcmpHeader->type = ICMP_ECHO;
    pIcmpHeader->code = 0;
    pIcmpHeader->un.echo.id = pspConfig->iIdent;
    /* Sequence is not increased to avoid checksum recalculation */
    pIcmpHeader->un.echo.sequence = 0;
    /* Checsum calculated on the entire icmp packet */
    pIcmpHeader->checksum = IcmpChksum((u_short *)pIcmpHeader, PINGER_QUERYSIZE);

    /* Open and connect the socket */
    pspConfig->iSockFd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (pspConfig->iSockFd < 0)
    {
      Log(MOD_NAME, LOG_WARN, "Couldn't open socket to ping host %s: %s\n",
          inet_ntoa(psaHostAddr->sin_addr), strerror(errno));
      iRc = errno;
    }
    else
    {
      if (connect(pspConfig->iSockFd, (struct sockaddr *)&pspConfig->saHostAddr,
          len))
      {
        printf("Connect error\n");
      }
    }
  }
  
  return iRc;
};


/***************************************************************************/
/* Release the ressources used by the pinger 				   */
/***************************************************************************/
int Pinger_Destroy (struct s_Pinger* pspConfig)
{ 
  int iRc = 0;

  ASSERT(pspConfig);

  /* close the socket */
  if (close(pspConfig->iSockFd) < 0)
  {
    Log(MOD_NAME, LOG_WARN, "Unable to destroy socket used to ping host %s: %s\n",
        inet_ntoa(pspConfig->saHostAddr.sin_addr), strerror(errno));
    iRc = errno;
  }
  
  /* Destroy the packet */
  free(pspConfig->strPacket);
    
  return iRc;
}


/***************************************************************************/
/* Try to detect the exact time at which the routing table must be updated */
/* To do so, just ping the host until it doesn't answer                    */
/***************************************************************************/
int Pinger_DetectMove(const struct s_Pinger* pspConfig, int* piHasMoved)
{
  int iRc = 0;
  fd_set fds;
  struct timeval tv;
  int iAnswered = 0;
  char strAnswer[PINGER_ANSWERSIZE];
  int i = 0;
int iAddrLen = sizeof(pspConfig->saHostAddr);

  ASSERT(pspConfig);
  ASSERT(piHasMoved);

  FD_ZERO (&fds);
  FD_SET (pspConfig->iSockFd, &fds);
  tv.tv_sec = 0;
  tv.tv_usec = PINGER_TIMEOUT;		/* We try to join host every half second */

  *piHasMoved = 0;

  /* First announce ourselves to the host and check we are able to send ping */
  iRc = Pinger(pspConfig);

  if (!iRc)
  {
    for (i = 0; i < PINGER_RETRIES; i++)	/* We renounce after 10 seconds */
    {
      iAnswered = select (pspConfig->iSockFd+1, &fds, NULL, NULL, &tv);

      switch (iAnswered)
      {
	case 1:				/* The host has answered */
        {
          iAnswered = recvfrom(pspConfig->iSockFd, strAnswer, sizeof(strAnswer),
                               0, (struct sockaddr *)&pspConfig->saHostAddr, &iAddrLen);

          if (iAnswered > 0 && CheckAnswer(strAnswer, pspConfig))
            Pinger(pspConfig);		/* Reannounce ourselves to the host */
          else
          {
            Log(LOG_NOTE, MOD_NAME, "Bad packet receive or receive error from %s",
                inet_ntoa(pspConfig->saHostAddr.sin_addr));
            *piHasMoved = 1;
          }
	  break;
	}
        case 0:				/* The host didn't answer */
	  *piHasMoved = 1;
	  break;
	case -1:			/* There was a problem */
	  iRc = errno;
	  break;
      }

      if (*piHasMoved == 1)
	/* Exit from the loop */
	break;
      else
      {
	/* Reinit tv struct or select modifies it at each call */
	tv.tv_sec = 0;
	tv.tv_usec = PINGER_TIMEOUT;
      }
    }
  }
  
  *piHasMoved = 1;

  return iRc;
};


/***************************************************************************/
/* Check if the route was correctly updated				   */
/* To do so, just ping the host until it answers			   */
/***************************************************************************/
int Pinger_CheckMove (const struct s_Pinger* pspConfig, int* piMoveOk)
{
  int iRc = 0;
  fd_set fds;
  struct timeval tv;
  char strAnswer[PINGER_ANSWERSIZE];
  int iAnswered = 0;
  int i = 0;
  int iAddrLen = sizeof(pspConfig->saHostAddr);

  ASSERT(pspConfig);
  ASSERT(piMoveOk);

  FD_ZERO (&fds);
  FD_SET (pspConfig->iSockFd, &fds);
  tv.tv_sec = 0;
  tv.tv_usec = PINGER_TIMEOUT;		/* We try to join host every half second */

  *piMoveOk = 0;

  /* First announce ourselves to the host and check we are able to send ping */
  iRc = Pinger(pspConfig);

  if (!iRc)
  {
    for (i = 0; i < PINGER_RETRIES; i++)	/* We renounce after 10 second */
    {
      iAnswered = select (pspConfig->iSockFd+1, &fds, NULL, NULL, &tv);

      switch (iAnswered)
      {
	case 0:				/* The host didn't answer */
	  Pinger (pspConfig);		/* Reannounce ourselves to the host */
	  break;
	case 1:				/* The host has answered */
          /* Check that this is our ECHO_REPLY */
          iAnswered = recvfrom(pspConfig->iSockFd, strAnswer, sizeof(strAnswer),
                         0, (struct sockaddr *)&pspConfig->saHostAddr, &iAddrLen);

          if (iAnswered > 0 && CheckAnswer(strAnswer, pspConfig))
            *piMoveOk = 1;
          else
            Log(LOG_NOTE, MOD_NAME, "Bad packet receive or receive error from %s",
                inet_ntoa(pspConfig->saHostAddr.sin_addr));
  
          break;
	case -1:			/* There was a problem */
	  iRc = errno;
	  break;
      }

      if (*piMoveOk == 1)
	/* Exit from the loop */
	break;
      else
      {
	/* Reinit tv struct or select modifies it at each call */
	tv.tv_sec = 0;
	tv.tv_usec = PINGER_TIMEOUT;
      }
    }
  }
  
  return iRc;
}

