/* VideoLAN VLANbridge : Gratuitous module
 *
 * Definition file
 *
*/


#ifndef _GRATUITOUSARP_H
#define _GRATUITOUSARP_H

int Gratuitous_Send (const struct sockaddr_in* psaFakeIP,
                     const struct sockaddr_in* psaFakeHw, const char *strDev);

#endif /* _GRATUITOUSARP_H */
