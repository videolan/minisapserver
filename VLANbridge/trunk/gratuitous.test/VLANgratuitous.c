/* VideoLAN router
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
*/

#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "gratuitous.h"


int main ()
{  char host[] = "tiny.via.ecp.fr";

   struct sockaddr_in target;
   struct sockaddr_in mac;
   unsigned long inaddr;
   struct hostent *hp; 
   char MAC[]="\x00\x60\\x97\x34\xbf\x66";
   char dev[]="eth0";
   int sockfd;

   bzero (&target, sizeof (struct sockaddr_in));
   target.sin_family = AF_INET;

   if ( (inaddr = inet_addr(host)) != INADDR_NONE)
    { /* it's dotted-decimal */
      bcopy(&inaddr, &target.sin_addr, sizeof(inaddr));
    }
    else
    { if ( (hp = gethostbyname(host)) == NULL)
      return -1;
      bcopy(hp->h_addr, &target.sin_addr, hp->h_length);
    };

   if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
   {
     printf ("Unable t oopen socket\n");
     exit (1);
   }

   printf ("Ready to send gratuitous\n");

   printf ("Host address: %s\n", inet_ntoa(target.sin_addr));
   return Gratuitous_Send (&target, &mac, dev);
};
