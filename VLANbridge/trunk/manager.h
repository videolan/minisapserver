/* VideoLAN VLANbridge: Manager module
 *
 * Definition file
 *
 * You need to include <pthread.h> before including this file
*/



#ifndef _MANAGER_H
#define _MANAGER_H

#define EXIT_MANAGER		0
#define START_PERFORMER		1
#define CLEAN_PERFORMER		2
#define ADMIN_REQUEST		3


struct s_Manager
{
  /* Thread id */
  pthread_t tId;

  /* Predicate, mutex used to test it, and condition variable */
  pthread_mutex_t WorkLock;
  pthread_cond_t WorkSignal;

  /* Internal database */
  struct s_Base* psbBase;

  /* Fifo used to communicate with listener and performer threads */
  struct s_Fifo* psfWaitingJobs;
};


int InitManager (struct s_Manager* psmManager);
int CancelManager(struct s_Manager* psmManager);
int FreeManager(struct s_Manager* psmManager);
int WarnManager(struct s_Manager* psmManager, struct s_Request* psrJob,
                int iJobType);


#endif
