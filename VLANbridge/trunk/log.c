/* VideoLAN VLANbridge : logging facilities
 *
 * Arnaud Bienvenu, VIA, ECP, France <fred@via.ecp.fr>, 20/09/97
 * Regis Duchesne, VIA, ECP, France <regis@via.ecp.fr>, 20/09/97
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr>, 22/10/98
 *
 * Log a message, stating the severity level and the module which produced the output
 * Provide the printf format facilities
 * The module is now thread safe
 *
 * TO DO: Limit the size of the log file
*/


/* For Solaris */
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#include "debug.h"
#include "log.h"


/* Size of the information fields coming before the log message */
#define LOG_TIMESIZE	29
#define LOG_LEVELSIZE	15
#define LOG_MODULESIZE	15

/* We just use a single log file */
static FILE* fdLogFile = NULL;
/* Mutex to avoid write overlapping */
pthread_mutex_t ptmLogFile;


/**************************************************************************/
/* Open the log file 							  */
/**************************************************************************/
int OpenLog (char* strLogName)
{
  int iRc = 0;
  ASSERT(strLogName)

  if ( !(fdLogFile = fopen (strLogName, "w+")) )
  {
    iRc = 1;
    /* For debug purposes (Message will be printed to screen) */
    Log (LOG_ERROR, MOD_VLANBRIDGE, "Unable to open %s log file: %s",
         strLogName, strerror(errno));
  }
  else
  {
    /* Init the mutex */
    iRc = pthread_mutex_init (&ptmLogFile, NULL);
  }
  
  return iRc;
}


/**************************************************************************/
/* Close the log file 							  */
/**************************************************************************/
int CloseLog ()
{
  int iRc = 0;
  
  if (fdLogFile)
  {
    /* Close the file */
    iRc = fclose (fdLogFile);

    if (iRc)  /* For debug purposes (Message will be printed to screen) */
      Log (LOG_WARN, MOD_VLANBRIDGE, "Unable to close log file: %s",
       strerror(errno));

    /* Release the mutex */
    iRc = pthread_mutex_destroy (&ptmLogFile);
  }
  else
  {
    iRc = 1;
    /* For debug purposes (Message will be printed to screen) */
    Log (LOG_ERROR, MOD_VLANBRIDGE,
         "Unable to close log file: file not opened");
  }

  return iRc;
}


/**************************************************************************/
/* Append a string in the log file 					  */
/**************************************************************************/
void Log (int iLevel, int iModule, const char* strMsg, ...)
{
  char strLogBuff[LOG_TIMESIZE+LOG_LEVELSIZE+LOG_MODULESIZE+LOG_BUFFSIZE+1];
  va_list ap;
  time_t tTime;
  struct tm* ptmTime;
  ASSERT(strMsg)

  /* Record the time */
  time (&tTime);
  ptmTime = localtime (&tTime);
  sprintf (strLogBuff, "%i/%i/%i %i:%i:%i ", ptmTime->tm_mon, ptmTime->tm_mday,
           ptmTime->tm_year, ptmTime->tm_hour, ptmTime->tm_min, ptmTime->tm_sec);

  /* Indicate the severity level of the message */
  switch (iLevel)
  {
    case LOG_NOTE:
      strcat (strLogBuff, "[INFO/");
      break;
    case LOG_WARN:
      strcat (strLogBuff, "[WARN/");
      break;
    case LOG_ERROR:
      strcat (strLogBuff, "[ERROR/");
      break;
    default:
      strcat (strLogBuff, "[EXCEPTION/");
      break;
  }

  /* Add the name of the module which logged the message */
  switch (iModule)
  {
    case MOD_VLANBRIDGE:
      strcat (strLogBuff, "VLANbridge]  ");
      break;
    case MOD_MANAGER:
      strcat (strLogBuff, "Manager]  ");
      break;
    case MOD_LISTENER:
      strcat (strLogBuff, "Listener]  ");
      break;
    case MOD_SENDER:
      strcat (strLogBuff, "Sender]  ");
      break;
    case MOD_PERFORMER:
      strcat (strLogBuff, "Performer]  ");
      break;
    case MOD_ADMIN:
      strcat (strLogBuff, "Admin]  ");
      break;
    default:
      strcat (strLogBuff, "Unknown]  ");
      break;
  }

  /* Now add the log message to the buffer */ 
  va_start(ap, strMsg);
  vsnprintf(strLogBuff+strlen(strLogBuff), LOG_BUFFSIZE, strMsg, ap);
  va_end(ap);

  /* Add the final CR */
  strcat(strLogBuff, "\n");

  /* And finally output the buffer to the logfile */
  if (fdLogFile)
  {
    pthread_mutex_lock (&ptmLogFile);
    fprintf (fdLogFile, strLogBuff);
    fflush (fdLogFile);
    pthread_mutex_unlock (&ptmLogFile);
  }

  /* When debugging, print to the screen too */
#ifdef DEBUG
  fprintf (stderr, "%s", strLogBuff);
#endif
}


/**************************************************************************/
/* Append a string in the log file and send it to stderr too		  */
/**************************************************************************/
void LogScreen (int iLevel, int iModule, const char* strMsg, ...)
{
  char strLogBuff[LOG_TIMESIZE+LOG_LEVELSIZE+LOG_MODULESIZE+LOG_BUFFSIZE];
  va_list ap;
  ASSERT(strMsg)

  /* Build the complete message */
  va_start(ap, strMsg);
  vsnprintf(strLogBuff, LOG_BUFFSIZE, strMsg, ap);
  va_end(ap);

  /* Print the message to the screen */
#ifndef DEBUG
   fprintf(stderr, "%s\n", strLogBuff);
/* else this will be done by the Log function */
#endif

  /* Add the required info */
   Log(iLevel, iModule, strLogBuff);
}
