/* VideoLAN VLANbridge: Listener module
 *
 * Definition file
 *
 * You need to include <pthread.h> before including this file
 */


#ifndef _LISTENER_H
#define _LISTENER_H



struct s_Listener
{
  /* thread id */
  pthread_t tid;

  /* Handle the alone sockfd used to communicate with the VLANserver */
  int iSockFd;

  /* Signal used to wake up the listener when the bridge is connected to VLANserver */
  pthread_cond_t WakeUpSignal;
};


int InitListener (struct s_Listener* pslListener);
void* ListenerThread (void* pvArg);
int CancelListener (struct s_Listener* pslListener);
int FreeListener (struct s_Listener* pslListener);

#endif
