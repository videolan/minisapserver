/* Videolan VLANbridge: Kernel routing table management
*
* Definitions file
*
* You need to include <net/route.h> before including this file
*/

#ifndef _ROUTER_H
#define _ROUTER_H

int Route_Add (int iSockFd, struct sockaddr_in* psaAddr, struct sockaddr_in* psaNetMask, char* strDev);
int Route_Del (int iSockFd, struct sockaddr_in* psaAddr, char* strDev);
/*int Route_Move (char* strInitialIf, char* strFinalIf, struct sockaddr_in* psaAddr);*/

#endif
