/* VideoLAN VLAN operations: definitions of all codes for messages exchange between
 *			     VLANserver, VLANclient and VLANbridge
 *
 * Regis Duchesne, VIA, ECP, France <regis@via.ecp.fr>, 23/09/97
 * Arnaud Bienvenu, VIA, ACP, France <fred@@via.ecp.fr>, 23/09/97
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr>, 13/10/98
 *
 */


/***************************************************************************/
/* Format of a message: ASCII control code and then arguments written next */
/* to the message code below (<XXX> <YYY>) if needed			   */
/***************************************************************************/


/* Message format definition */
#define VLAN_MSG_LEN		256	/* Maximum length of a message */
#define VLAN_CODE_LEN		2	/* Control code values range from 0 to 99 */
#define VLAN_MAC_SIZE		18	/* Length of a MAC field (including final NULL char) */
#define VLAN_IP_SIZE		16	/* Lentgh of an IP field (including final NULL char) */

/* Error msg (common for VLANserver, client and bridge) */
#define VLAN_BAD_COMMAND        4       /* Invalid request: bad command */
#define VLAN_BAD_FORMAT		5       /* Invalid request: bad format */
#define VLAN_BUSY		6	/* Server is busy, request ignored */
#define VLAN_ERROR		7       /* A global unrecoverable error occured */

/* login msg (common for all) */
#define VLAN_LOGIN_REQUEST	98	/* <soft version> <login> <passwd> */
#define VLAN_LOGIN_OK		97	/* Login accepted */
#define VLAN_LOGIN_KO		96	/* Login rejected */

/* logout (common for all too) */
#define VLAN_BYE		99	/* Logout msg */

/* VLANbridge <> VLANserver msg */
#define VLAN_ROUTE_REQUEST	11	/* <MAC> [IP] <VLAN Dest> [VLAN Src */
#define VLAN_ROUTE_OK		13	/* Route request succesfully completed */
#define VLAN_ROUTE_KO		14	/* Request failed to be processed */
#define VLAN_ROUTE_NOMOVE	18	/* Request not processed: client didn't move */

/* VLANserver <> client msg */
#define VLAN_MOVE_REQUEST	21	/* <MAC> [IP] <VLAN Dest> [VLAN Src] */
#define VLAN_MOVE_OK		22	/* Move request succesfully completed */
#define VLAN_MOVE_KO		23	/* Request failed to be processed */

/* VLANserver <> VLANclient */
#define VLAN_LOCATION_REQUEST	31	/* No argument */
#define VLAN_LOCATION_ANSWER	32	/* <switch> <port> <vlan> <sharers> */
#define VLAN_LOCATION_UNKNOW	33	/* VLANserver don't know */

