/* VideoLAN VLANbridge: Signal processing
 *
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr> 23/10/98
 *
 * Known Bug: When stopping the pgrm with a Ctrl+C, it often freezes when
 * trying to cancel the sender thread
*/


#include <signal.h>

#include "debug.h"
#include "log.h"
#include "vlanbridge.h"
#include "signal.h"

#define MOD_NAME MOD_VLANBRIDGE


/* */
void SigHup(int iCrap)
{
  Log(LOG_WARN, MOD_NAME, "Received SIGHUP: Hangup detected");
}


/* */
void SigInt(int iCrap)
{
  Log(LOG_WARN, MOD_NAME,
      "Received SIGINT: Interrupt from keyboard, launching shutdown sequence...");
  exit(StopBridge(0));
}


/* */
void SigQuit(int iCrap)
{
  Log(LOG_WARN, MOD_NAME,
      "Received SIGQUIT: Quit from keyboard, launching shutdown sequence...");
  exit(StopBridge(0));
}


/* */
void SigTerm(int iCrap)
{
  Log(LOG_WARN, MOD_NAME,
      "Received SIGTERM: Software termination signal, exiting...");
  exit(StopBridge(0));
}


/* */
void SigSegv(int iCrap)
{
  Log(LOG_WARN, MOD_NAME,
      "Received SIGSEGV: Segmentation Violation, exiting...");
  exit(1);
}


/* Handle signal sent when one try to write on a broken stream
   (socket dead for example) */
void SigPipe(int iCrap)
{
  Log(LOG_WARN, MOD_NAME, "Received SIGPIPE: Broken Pipe");
}


/* Handle signal sent when AOB data is received on a TCP connection */
void SigUrg(int iCrap)
{
  Log(LOG_NOTE, MOD_NAME, "Received SIGURG: AOB data received");
}


void SigDefault(int iCrap)
{
  Log(LOG_NOTE, MOD_NAME, "Received signal %d", iCrap);
}


/* */
int SetupSigHandlers ()
{
  unsigned int iRc = 0;
  
  /* Setup signal handlers */
  if (signal(SIGHUP, SigHup) == SIG_ERR)
  {
    Log(LOG_ERROR, MOD_NAME, "Could not install handler for signal SIG_HUP");
    iRc |= 1;
  }

  if (signal(SIGINT, SigInt) == SIG_ERR)
  {
    Log(LOG_ERROR, MOD_NAME, "Could not install handler for signal SIG_INT");
    iRc |= 1;
  }

  if (signal(SIGQUIT, SigQuit) == SIG_ERR)
  {
    Log(LOG_ERROR, MOD_NAME, "Could not install handler for signal SIG_QUIT");
    iRc |= 1;
  }

  if (signal(SIGPIPE, SigPipe) == SIG_ERR)
  {
    Log(LOG_ERROR, MOD_NAME, "Could not install handler for signal SIG_PIPE");
    iRc |= 1;
  }

  if (signal(SIGURG, SigUrg) == SIG_ERR)
  {
    Log(LOG_ERROR, MOD_NAME, "Could not install handler for signal SIG_URG");
    iRc |= 1;
  }

  if (signal(SIGSEGV, SigSegv) == SIG_ERR)
  {
    Log(LOG_ERROR, MOD_NAME, "Could not install handler for signal SIG_SEGV");
    iRc |= 1;
  }

  if (signal(SIGTERM, SigTerm) == SIG_ERR)
  {
    Log(LOG_ERROR, MOD_NAME, "Could not install handler for signal SIG_TERM");
    iRc |= 1;
  }

  return iRc;
}
