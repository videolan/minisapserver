 /* VideoLAN VLANbridge: fifo management
 *
 * Definition file
 *
 * You need to include "performer.h" before this file
*/


#ifndef _FIFO_H
#define _FIFO_H

/* Size of the FIFO */
#define FIFO_SIZE 5

/* Structure used to describe the fifo */
struct s_Fifo
{
  int iSize;
  struct s_Request* asrData[FIFO_SIZE];
  
  /* Pointer to the address of the array where to pull */
  struct s_Request** psrWhereToPull;
  /* Pointer to the addess where to push */
  struct s_Request** psrWhereToPush;

  /* Private signal */
  pthread_cond_t FifoSignal;

  /* Private mutex */
  pthread_mutex_t FifoLock;
};

struct s_Fifo* FifoCreate();
int FifoDestroy(struct s_Fifo* psfFifo);
int FifoPush(struct s_Fifo* psfFifo, struct s_Request* psrToStore);
int FifoPull(struct s_Fifo* psfFifo, struct s_Request** psrRequest);


#endif
