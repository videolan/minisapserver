/*****************************************************************************
 * types.c
 * Init of the VLANserver
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: types.c,v 1.26 2001/10/24 20:35:16 marcari Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdlib.h>                                                /* malloc */
#include <pthread.h>                                   /* pthread_mutex_init */
#include <semaphore.h>                                           /* sem_wait */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */
#include <signal.h>

#include "types.h"
#include "logger.h"

#include "snmp/snmp_switch.h"                                   /* if_snmp.h */
#include "snmp/if_snmp.h"                                      /* SNMP_start */
#include "db/db.h"                                               /* DB_start */
#include "config/config.h"                                       /* CFG_init */
#include "server/server.h"                                       /* VS_start */
#include "interface/interface.h"                                 /* IF_start */
#include "vlanserver.h"                                                /* vs */

static void VS_finish(int);

unsigned int VS_init(int argc,char *argv[])
{
  ERR_CODE grumpf;

  signal(SIGINT, VS_finish);      /* arrange interrupts to terminate */
  
#ifdef VLB
  grumpf=pthread_mutex_init(&(vs->cnx_lock),NULL);
#endif
  
  /* Initializing the log process */
  /* with screen only because we don't know yet the logfile  */
  LOG_init();
  LOG_create(stderr, LOGTYPE_SCREEN);


  /* Initializing the DataBase */
  grumpf=DB_start(vs->db);
  if (grumpf)
    return grumpf;
  
  /* Initializing the interface */
  grumpf=IF_start();
  if(grumpf)
    return grumpf;
  
  /* Initializing the SNMP */
  vs->snmp->sem=malloc(sizeof(sem_t));
  sem_init(vs->snmp->sem,0,0);
  grumpf=SNMP_start(vs->snmp);
  if (grumpf)
    return grumpf;

  /* Reading configuration file */
  grumpf=CFG_init(vs->cfg,argc,argv);
  if (grumpf)
    return grumpf;

  /* creating logging output */
  /* XXX : the fd returned is still ignored, please fix it */
  if (vs->cfg->logscreen==0)
  {
    LOG_delete(stderr);
  }
  if (vs->cfg->logfile!=NULL)
  {
    LOG_file(vs->cfg->logfile);
  }

  /* Starting the server */
  grumpf=VS_start();
  if (grumpf)
    return grumpf;
 
  LOG_close();
  
  return 0;
}

/* it is the callback called when ctrl-C is pressed *
 * it is a big kludge because I don't know how to   *
 * do it : someone should correct this              */

static void VS_finish(int sig)
{
  VS_stop(vs->info_poller);
}

//char * VS_MAC2A(VS_MachineId y)
//{
//char buffer [18];  
//sprintf (buffer,"%Lx:%Lx:%Lx:%Lx:%Lx:%Lx",((y)>>40)%256,((y)>>32)%256,((y)>>24)%256,((y)>>16)%256,((y)>>8)%256,(y)%256);
//return buffer;
//}
