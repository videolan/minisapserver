#! /bin/sh

#/****************************************************************************
# * vlans-h2.sh
# * script to change VLAN on a specific switch
# ****************************************************************************
# * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
# * $Id: vlans-h2.sh,v 1.2 2001/04/29 03:41:49 nitrox Exp $
# *
# * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
# *          Michel Kaempf <maxx@via.ecp.fr>
# *
# * This program is free software; you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation; either version 2 of the License, or
# * (at your option) any later version.
# * 
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
# ****************************************************************************/

IPADDR="138.195.128.38"

MACADDR="$1"
VLAN="$2"

if PORT=`snmpwalk "$IPADDR" community .1.3.6.1.4.1.43.10.22.2.1.3 | grep -i "$(echo $MACADDR | sed s/':'/'.'/g)" | cut -d '.' -f 9`
then

    if [ "$VLAN" = "2" ]
    then
        COIN="12782"
    elif [ "$VLAN" = "3" ]
    then
        COIN="21713"
    elif [ "$VLAN" = "4" ]
    then
        COIN="9513"
    elif [ "$VLAN" = "5" ]
    then
        COIN="63289"
    elif [ "$VLAN" = "6" ]
    then
        COIN="61040"
    elif [ "$VLAN" = "7" ]
    then
        COIN="27188"
    fi

    if [ "$PORT" = "2" ]
    then
        MEUH="60752"
    elif [ "$PORT" = "3" ]
    then
        MEUH="51563"
    elif [ "$PORT" = "5" ]
    then
        MEUH="52256"
    elif [ "$PORT" = "6" ]
    then
        MEUH="20124"
    elif [ "$PORT" = "7" ]
    then
        MEUH="51761"
    elif [ "$PORT" = "13" ]
    then
        MEUH="52467"
    elif [ "$PORT" = "14" ]
    then
        MEUH="31580"
    elif [ "$PORT" = "15" ]
    then
        MEUH="37355"
    elif [ "$PORT" = "16" ]
    then
        MEUH="139"
    elif [ "$PORT" = "17" ]
    then
        MEUH="64920"
    elif [ "$PORT" = "18" ]
    then
        MEUH="57235"
    fi

    snmpset "$IPADDR" community "31.1.2.1.3.$COIN.$MEUH" i 4
fi
