/*****************************************************************************
 * vlans.c
 * mini server to change VLAN
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: vlans.c,v 1.2 2001/04/29 03:41:49 nitrox Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 31337
#define BACKLOG 8

int main( int argc, char * argv[] )
{
    int i_socket;
    struct sockaddr_in sa_server;
    int i_nsocket;
    char buffer[ 1024 ];
    char snmp[ 1024 ];

    i_socket = socket( AF_INET, SOCK_STREAM, 0 );

    bzero( &sa_server, sizeof(struct sockaddr_in) );
    sa_server.sin_family = AF_INET;
    sa_server.sin_port = htons( PORT );
    sa_server.sin_addr.s_addr = htonl( INADDR_ANY );

    bind( i_socket, (struct sockaddr *)(&sa_server), sizeof(sa_server) );

    listen( i_socket, BACKLOG );

    for (;;)
    {
        i_nsocket = accept( i_socket, (struct sockaddr *)(&sa_server), NULL );

	bzero( buffer, sizeof(buffer) );
	recv( i_nsocket, buffer, sizeof(buffer), 0 );

	bzero( snmp, sizeof(snmp) );
	sprintf( snmp, "./vlans.sh %s", buffer );
	system( snmp );

	close( i_nsocket );
    }

    close( i_socket );

    return( 0 );
}
