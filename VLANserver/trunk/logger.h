/*****************************************************************************
 * logger.h
 * Header file for logger.c
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: logger.h,v 1.13 2001/05/28 00:49:41 marcari Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __LOGGER_H__
#define __LOGGER_H__

#define LOGBUFF_MAXSIZE 460
#define LOGBUFF_LINESIZE 46

/* different types the LOG can be */
#define LOGTYPE_NONE 0
#define LOGTYPE_FILE 1
#define LOGTYPE_SCREEN 2 
#define LOGTYPE_TELNET 3

/* Structure that represents a log stream :
 * there is one structure for each log output
 * the different output are chained
 * the root element is ls_none
 * the last element has next_stream=NULL
 * no log is caracterized by : ls_none.next_stream==NULL 
 */
struct LOG_stream 
{
  FILE * fd_output;
  int type;
  pthread_mutex_t ptm_log;
  struct LOG_stream * next_stream;
};

char * VS_log_errstr(ERR_CODE err);

void VS_log(LOG_LEVELS level, LOG_MODULES module, char * format, ...);

int LOG_init();
int LOG_create(FILE * , int type);
int LOG_delete(FILE *);
int LOG_close();
FILE * LOG_file(char * filename);
#endif
