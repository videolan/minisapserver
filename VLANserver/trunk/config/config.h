/*****************************************************************************
 * config.h
 * Header file for config.c (once generated)
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: config.h,v 1.9 2001/10/24 20:35:16 marcari Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __CFG_CONFIG_H__
#define __CFG_CONFIG_H__

/* the type of a port for the configuration file parser */
struct CFG_PORT
{
  VS_PROTECTION_LEVEL protection;
  VS_PORT_FLAGS flags;
};

/* the data struct for a switch type and the macro to get the switch type's name
 * */
struct CFG_SWITCH_TYPE          /* note the type's name is stored just at the
                                   end of this structure */
{
  VS_SwitchType type;
  VS_UNIT unit;            /* the default unit in case of matrix use */
  VS_PORT nports;               /* the number of ports, ports numbers will go
                                   from 1 to nports */
  struct CFG_PORT *ports;       /* an array of default values (ports[1] is the
                                   default port 1, and ports[nports] is the
                                   default port nport), an ugly trick still
                                   exists to avoid wasting memory */
  struct CFG_SWITCH_TYPE *next; /* the next switch type in the list */
};
#define CFG_SWITCH_TYPE_NAME(x) ((char *)((&(x))+1))

/* information on a vlan */
struct CFG_CHANNEL
{
  char *raw_data;                       /* raw data to transmit to the client
                                           when receiving a request */
  VS_VLAN vlan;                         /* the vlan number */
  VS_PROTECTION_LEVEL protection;       /* the privileges required to access
                                           this VLAN */
};

/* the data struct for a switch */
struct CFG_SWITCH
{
  VS_SwitchType type;
  VS_UNIT unit;
  VS_SwitchId ip;
  VS_PORT nports;
  struct CFG_PORT *ports;
  struct CFG_SWITCH *next;
  char *community;
  VS_PORT_FLAGS flags;
};

/* the data structure for the configuration parser */
struct VS_config
{
  VS_CHANNEL nchannels;                 /* the number of channels existing on
                                           this vlanserver */
  struct CFG_CHANNEL *chan_map;         /* chan_map[i].vlan is the vlan for
                                           channel i */
  struct CFG_SWITCH_TYPE *switch_types; /* the list of switch types */
  struct CFG_SWITCH *zwitches;          /* the list of switches */
  char * vlanbridge;                    /* the DNS address of the vlanbridge */
  char *vlanbridge_login;               /* the login on the vlanbridge */
  char *vlanbridge_password;            /* the password on the vlanbridge */
  char *logfile;                        /* the file where we will log */
  unsigned char logscreen;              /* the log to screen if !=0 */
};

/* parses the command line arguments and the configuration file, just feed it
 * with &vs->cfg, &vs->db, argc and argv */
ERR_CODE CFG_init(struct VS_config *cfg,int argc,char *argv[]);

/* looks for a switch type, feed it with &vs->cfg and the switch type's name */
struct CFG_SWITCH_TYPE *CFG_switch_type(struct VS_config *cfg,\
                                        char *switch_type_name);

#endif
