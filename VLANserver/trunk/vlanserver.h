/*****************************************************************************
 * vlanserver.h
 * Provide the struct VLANserver
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: vlanserver.h,v 1.9 2001/10/24 20:35:16 marcari Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __VLANSERVER_H__
#define __VLANSERVER_H__

/* VLANserver : global VLANserver variables and their initialization function */
struct VLANserver
{
  pthread_mutex_t cnx_lock;        /* the mutex used by VS_cnx_lost when the
                                        connexion with the VLANbridge breaks */
  struct DB_db     *db;                                      /* the database */
  struct VS_config *cfg;                                /* the configuration */
  struct SNMP_snmp *snmp;                                 /* the snmp engine */
  struct VS_info_poller * info_poller;                     /* for the server */ 
  struct sockaddr_in vlb_params;           /* IP and port for the vlanbridge */
  int fdescr;    /* the socket descriptor to communicate with the VLANbridge */
  enum
  {
      VS_INIT,                             /* the vlanserver is initializing */
      VS_RUN,                                   /* the vlanserver is running */
      VS_HOLD,              /* the vlanserver does no longer accept requests */
      VS_STOP,                     /* the vlanserver engine is shutting down */
      VS_CANCEL,                               /* the vlanserver is canceled */
  } runlevel ;
};

extern struct VLANserver *vs;

unsigned int VS_init(int argc, char* argv[]);

#endif
