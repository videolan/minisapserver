#ifndef __DB_TYPES_H__
#define __DB_TYPES_H__

#define DB_MACHINES_HASH 64
#define DB_machines_hash(x) (x&63)

#define DB_SWITCHS_HASH 64
#define DB_switchs_hash(x) ((x).s_addr&63)

/* DB_port : the type of a port */
struct DB_port
{
  /* requests fifo */
    pthread_mutex_t requests_lock;      /* lock */
    struct DB_S_request *first;         /* first request in the fifo */
    struct DB_S_request *last;          /* last request in the fifo */
};

/* DB_M_union : type required by the DB_M_req function and the requests on the
 * machines table */
  /* DB_M_reuh */
  struct DB_M_reuh
  {
    VS_MachineId meuuh;
    struct DB_machines_elt **coin;
  };
union DB_M_union
{
  struct DB_machines_elt *glop;
  struct DB_M_reuh *reuh;
};

/* DB_S_union : type required by the DB_S_req function and the requests on the
 * switches table */
  /* DB_S_reuh */
  struct DB_S_reuh
  {
    VS_SwitchId meuuh;
    struct DB_switchs_elt **coin;
  };
  struct DB_S_rah
  {
    struct SNMP_switch **zwitch;
    VS_SwitchId meuuh;
    struct DB_port **coin;
    VS_PORT zarma;
  };
  struct DB_S_switch_fetcher
  {
    VS_SwitchId switch_id;
    struct DB_port ** ports;
    struct SNMP_switch ** zwitch;
  };
union DB_S_union
{
  VS_SwitchId switch_id;
  struct DB_port *glip;
  struct DB_switchs_elt *glop;
  struct DB_S_reuh *reuh;
  struct DB_S_rah *rah;
  struct DB_S_switch_fetcher *switch_fetcher;
};

/* DB_M_request : type of waiting requests on the machines table */
struct DB_M_request
{
  sem_t semaphore;                              /* to wait until other requests
                                                   end*/
  union DB_M_union argument;                    /* the request's argument(s)*/
  ERR_CODE ret_value;                           /* the request's return value*/
  void (*func)(void *,struct DB_M_request *);   /* the function to execute for
                                                   the request*/
  struct DB_M_request *next;                    /* next request in the fifo*/
};

/* DB_S_request : type of waiting requests on the switches table */
struct DB_S_request
{
  sem_t semaphore;                              /* to wait until other requests
                                                   end */
  union DB_S_union argument;                    /* the request's argument(s)*/
  ERR_CODE ret_value;                           /* the request's return value*/
  void (*func)(void *,struct DB_S_request *);   /* the function to execute for
                                                   the request*/
  struct DB_S_request *next;                    /* next request in the fifo*/
  VS_PORT wait_count;                           /* the number of posts() to
                                                   wait for - 1 */
};

/* DB_machines_elt : element of the machines table */
struct DB_machines_elt
{
  VS_MachineId mac;                     /* machine identifyer */
  VS_SwitchId switch_ip;                /* the switch's IP */
  /* requests fifo */
    pthread_mutex_t requests_lock;      /* lock */
    struct DB_M_request *first;         /* first request in the fifo */
    struct DB_M_request *last;          /* last request in the fifo */
  struct DB_machines_elt *next;         /* next machine in the fifo */
  VS_CHANNEL channel;                   /* the channel this machine wants to
                                           receive */
  VS_PORT port;                         /* the port of the switch on which the
                                           machine is */
};

/* DB_switchs_elt : element of the switches table */
struct DB_switchs_elt
{
  struct SNMP_switch *zwitch;   /* snmp variables */
  VS_SwitchId ip;               /* switch identifyer */
  struct DB_switchs_elt *next;  /* next switch in the fifo */
  struct DB_port *ports;        /* points on the switch's ports */
  VS_PORT nports;               /* number of ports in the switch */
};

/* DB_db : the whole database */
struct DB_db
{
  /* the mutex used to synchronize the engines of the two tables */
  pthread_mutex_t db_lock;
  /* the machines table */
  struct DB_machines_elt *machines[DB_MACHINES_HASH];
  /* the switches table */
  struct DB_switchs_elt *switchs[DB_SWITCHS_HASH];
  /* the database's current state */
  enum
  {
    DB_INIT,    /* the database is initializing */
    DB_RUN,     /* the database is running */
    DB_HOLD,    /* the database does no longer accept requests */
    DB_STOP,    /* the database engine is shutting down */
    DB_CANCEL   /* the database is canceled */
  } runlevel;
  /* the machines table engine's thread */
  pthread_t thread_machines;
  /* the switchs table engine's thread */
  pthread_t thread_switchs;
  /* the condition variable allowing the machines table's engine to process */
  pthread_cond_t machines_requests_cond;
  /* the condition variable that allows the switchs table's engine to process*/
  pthread_cond_t switchs_requests_cond;
  /* machines requests fifo */
    pthread_mutex_t machines_requests_lock;
    struct DB_M_request *machines_requests;
    struct DB_M_request *machines_last;
  /* switchs requests fifo */
    pthread_mutex_t switchs_requests_lock;
    struct DB_S_request *switchs_requests;
    struct DB_S_request *switchs_last;
};

/* starts the database engine */
ERR_CODE DB_start(struct DB_db * db);

/* joins with the database engine */
static inline void DB_join(struct DB_db * db)
{
  pthread_join(db->thread_machines, NULL);
  pthread_join(db->thread_switchs, NULL);
}

/* inserts a new machine in the database */
ERR_CODE DB_machines_insert(struct DB_db * db, VS_MachineId mac,
                            VS_SwitchId switch_ip, VS_PORT port,
                            VS_CHANNEL channel);

/* inserts a new switch in the database */
/* the fifo elements of ports do not need to be initialized by the caller */
ERR_CODE DB_switchs_insert(struct DB_db * db, VS_SwitchId ip,
                           struct DB_port * ports, VS_PORT nports,
                           struct SNMP_switch * zwitch);

/* deletes a machine from the database */
ERR_CODE DB_machines_delete(struct DB_db * db, VS_MachineId mac);

//ERR_CODE DB_switchs_delete(struct DB_db *db,VS_SwitchId ip);

/* releases the lock on an entry of the machines table */
ERR_CODE DB_machines_unlock(struct DB_db * db,
                            struct DB_machines_elt * machine);

/* releases the lock on a port of an entry of the switchs table */
ERR_CODE DB_switchs_port_unlock(struct DB_db * db, struct DB_port * port);

/* takes the lock on an entry of the machines table */
ERR_CODE DB_machines_lock(struct DB_db * db, VS_MachineId mac,
                          struct DB_machines_elt ** machine);

/* takes the lock on a port of an entry of the switchs table */
ERR_CODE DB_switchs_port_lock(struct DB_db * db, VS_SwitchId ip,
                              VS_PORT port_num, struct DB_port ** port,
                              struct SNMP_switch ** zwitch);

/* removes all entries (machines and switches) from the database */
ERR_CODE DB_flush(struct DB_db * db);

#endif
