/*****************************************************************************
 * logger.c
 * Print a log to a file or to screen
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: logger.c,v 1.23 2001/11/12 17:30:42 marcari Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <pthread.h>                                   /* pthread_mutex_lock */
#include <stdarg.h>                                                /* va_arg */
#include <time.h>                                                    /* time */
#include <stdio.h>                                           /* fflush , ... */
#include <stdlib.h>

#include "types.h"
#include "logger.h"


#define FALSE   0
#define TRUE    1

/* Local Functions prototypes */


static struct LOG_stream * ls_none;
static pthread_mutex_t ptm_log_available;
static int is_log_available=FALSE;

/*****************************************************************************
 * VS_log_File : Write a log inside a file
 *****************************************************************************/
static void VS_log_File(LOG_LEVELS level,
                        LOG_MODULES module,
                        struct LOG_stream * output_stream,
                        char* format,
                        va_list ap)
{
 
  char MsgBuff[LOGBUFF_MAXSIZE];   /* buffer which will contain 
                                      the message wanted to be displayed */
  char LineBuff[LOGBUFF_LINESIZE]; /* line currently displayed */
  char HeaderBuff[34+20];          /* header of the message + color */
  char FooterBuff[20];             /* footer of the message + end color */

  int MessagePosition;
  int MessageLength;
  
  time_t MsgTime;
  struct tm * ptmTime;

#ifndef DEBUG
  if(level!=LOGDEBUG)
  {
#endif
  /* Record the time */
  time(&MsgTime);
  ptmTime=localtime(&MsgTime);
  sprintf(HeaderBuff, "%02i/%02i %02i:%02i:%02i ", ptmTime->tm_mday,
          ptmTime->tm_mon+1, ptmTime->tm_hour, 
          ptmTime->tm_min, ptmTime->tm_sec);



  /* Determinate in which module it happened */
  switch (module)
  {
    case SNMP:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[SNMP  : ");
      break;
    
    case SERVER:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[SERVER: ");
      break;
      
    case DB:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[DB    : ");
      break;

    case CFG:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[CFG   : ");
      break;
      
    case IF:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[IF    : ");
      break;
    
    case TELNET:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[TELNET: ");
      break;
    case LOGGER:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[LOGGER: ");
      break;
  }

  /* Determinate the error level */
  switch (level)
  {
    case LOGDEBUG:
      sprintf(HeaderBuff+strlen(HeaderBuff), "debug   ] ");
      break;
    case LOGINFO:
      sprintf(HeaderBuff+strlen(HeaderBuff), "info    ] ");
      break;
    case LOGWARNING:
      sprintf(HeaderBuff+strlen(HeaderBuff), "Warning ] ");
      break;
    case LOGFATAL:
      {
        sprintf(HeaderBuff+strlen(HeaderBuff), "FATAL   ] ");
        break;
      }
    case LOGERROR:
      {
        sprintf(HeaderBuff+strlen(HeaderBuff), "ERROR   ] ");
        break;
      }
  }
 
  /* XXX No error checking ! What length is the message ? */
  vsnprintf(MsgBuff, LOGBUFF_MAXSIZE, format, ap);

  sprintf(FooterBuff,"\n");
        
  pthread_mutex_lock(&output_stream->ptm_log);
  fputs(HeaderBuff, output_stream->fd_output);

  MessageLength = strlen(MsgBuff);
  MessagePosition = 0;
  while ( (MessageLength-MessagePosition) > LOGBUFF_LINESIZE )
  {
    snprintf( LineBuff, LOGBUFF_LINESIZE+1, MsgBuff + MessagePosition );
    fputs( LineBuff, output_stream->fd_output );
    fputs( "\n                                  ", output_stream->fd_output );
    MessagePosition += LOGBUFF_LINESIZE;
  }
  fputs( MsgBuff + MessagePosition, output_stream->fd_output);
  
  fputs(FooterBuff, output_stream->fd_output);
  fflush(output_stream->fd_output);
  pthread_mutex_unlock(&output_stream->ptm_log);

#ifndef DEBUG
  }
#endif
}

                  
/*****************************************************************************
 * VS_log_Telnet : formats the ouput to be printed on a telnet intf
 *****************************************************************************/
static void VS_log_Telnet(LOG_LEVELS level,
                        LOG_MODULES module,
                        struct LOG_stream * output_stream,
                        char* format,
                        va_list ap)
{
 
  char MsgBuff[LOGBUFF_MAXSIZE];   /* buffer which will contain 
                                      the message wanted to be displayed */
  char LineBuff[LOGBUFF_LINESIZE]; /* line currently displayed */
  char HeaderBuff[34+20];          /* header of the message + color */
  char FooterBuff[20];             /* footer of the message + end color */

  int MessagePosition;
  int MessageLength;
  
  time_t MsgTime;
  struct tm * ptmTime;

#ifndef DEBUG
  if(level!=LOGDEBUG)
  {
#endif
  /* Record the time */
  time(&MsgTime);
  ptmTime=localtime(&MsgTime);
  sprintf(HeaderBuff, "%02i/%02i %02i:%02i:%02i ", ptmTime->tm_mday,
          ptmTime->tm_mon+1, ptmTime->tm_hour, 
          ptmTime->tm_min, ptmTime->tm_sec);



  /* Determinate in which module it happened */
  switch (module)
  {
    case SNMP:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[SNMP  : ");
      break;
    
    case SERVER:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[SERVER: ");
      break;
      
    case DB:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[DB    : ");
      break;

    case CFG:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[CFG   : ");
      break;
      
    case IF:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[IF    : ");
      break;
    
    case TELNET:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[TELNET: ");
      break;
    case LOGGER:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[LOGGER: ");
      break;
  }

  /* Determinate the error level */
  switch (level)
  {
    case LOGDEBUG:
      sprintf(HeaderBuff+strlen(HeaderBuff), "debug   ] ");
      break;
    case LOGINFO:
      sprintf(HeaderBuff+strlen(HeaderBuff), "info    ] ");
      break;
    case LOGWARNING:
      sprintf(HeaderBuff+strlen(HeaderBuff), "Warning ] ");
      break;
    case LOGFATAL:
      {
        sprintf(HeaderBuff+strlen(HeaderBuff), "FATAL   ] ");
        break;
      }
    case LOGERROR:
      {
        sprintf(HeaderBuff+strlen(HeaderBuff), "ERROR   ] ");
        break;
      }
  }
 
  /* XXX No error checking ! What length is the message ? */
  vsnprintf(MsgBuff, LOGBUFF_MAXSIZE, format, ap);

  sprintf(FooterBuff,"\r\n");
        
  pthread_mutex_lock(&output_stream->ptm_log);
  fputs(HeaderBuff, output_stream->fd_output);

  MessageLength = strlen(MsgBuff);
  MessagePosition = 0;
  while ( (MessageLength-MessagePosition) > LOGBUFF_LINESIZE )
  {
    snprintf( LineBuff, LOGBUFF_LINESIZE+1, MsgBuff + MessagePosition );
    fputs( LineBuff, output_stream->fd_output );
    fputs( "\r\n                                  ", output_stream->fd_output );
    MessagePosition += LOGBUFF_LINESIZE;
  }
  fputs( MsgBuff + MessagePosition, output_stream->fd_output);
  
  fputs(FooterBuff, output_stream->fd_output);
  fflush(output_stream->fd_output);
  pthread_mutex_unlock(&output_stream->ptm_log);

#ifndef DEBUG
  }
#endif
}



/*****************************************************************************
 * Write a log to a screen(-like) output
 *****************************************************************************/
static void VS_log_Screen(LOG_LEVELS level,
                          LOG_MODULES module,
                          struct LOG_stream * output_stream,
                          char* format,
                          va_list ap)
{
  char MsgBuff[LOGBUFF_MAXSIZE];   /* buffer which will contain 
                                      the message wanted to be displayed */
  char LineBuff[LOGBUFF_LINESIZE]; /* line currently displayed */
  char HeaderBuff[34+20];          /* header of the message + color */
  char FooterBuff[20];             /* footer of the message + end color */

  int MessagePosition;
  int MessageLength;
  
  time_t MsgTime;
  struct tm * ptmTime;

#ifndef DEBUG
  if(level!=LOGDEBUG)
  {
#endif
  /* Record the time */
  time(&MsgTime);
  ptmTime=localtime(&MsgTime);
  sprintf(HeaderBuff, "%02i/%02i %02i:%02i:%02i ", ptmTime->tm_mday,
          ptmTime->tm_mon+1, ptmTime->tm_hour, 
          ptmTime->tm_min, ptmTime->tm_sec);



  /* Determinate in which module it happened */
  switch (module)
  {
    case SNMP:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[SNMP  : ");
      break;
    
    case SERVER:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[SERVER: ");
      break;
      
    case DB:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[DB    : ");
      break;

    case CFG:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[CFG   : ");
      break;
      
    case IF:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[IF    : ");
      break;
    
    case TELNET:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[TELNET: ");
      break;
    case LOGGER:
      sprintf(HeaderBuff+strlen(HeaderBuff), "[LOGGER: ");
      break;
  }

  /* Determinate the error level */
  switch (level)
  {
    case LOGDEBUG:
      sprintf(HeaderBuff+strlen(HeaderBuff), "debug   ] ");
      break;
    case LOGINFO:
      sprintf(HeaderBuff+strlen(HeaderBuff), "info    ] ");
      break;
    case LOGWARNING:
#ifdef LINUX_COLOR
      sprintf(HeaderBuff+strlen(HeaderBuff), "\033[40;32;1m");
#endif
      sprintf(HeaderBuff+strlen(HeaderBuff), "Warning ] ");
      break;
    case LOGFATAL:
      {
#ifdef LINUX_COLOR
        sprintf(HeaderBuff+strlen(HeaderBuff), "\033[40;31;1m");
#endif        
        sprintf(HeaderBuff+strlen(HeaderBuff), "FATAL   ] ");
        break;
      }
    case LOGERROR:
      {
#ifdef LINUX_COLOR
        sprintf(HeaderBuff+strlen(HeaderBuff), "\033[40;31;1m");
#endif 
        sprintf(HeaderBuff+strlen(HeaderBuff), "ERROR   ] ");
        break;
      }
  }
 
  /* XXX No error checking ! What length is the message ? */
  vsnprintf(MsgBuff, LOGBUFF_MAXSIZE, format, ap);

#ifdef LINUX_COLOR
  sprintf(FooterBuff, "\033[0m\n");
#else
  sprintf(FooterBuff,"\n");
#endif

  pthread_mutex_lock(&output_stream->ptm_log);
  fputs(HeaderBuff, output_stream->fd_output);

  MessageLength = strlen(MsgBuff);
  MessagePosition = 0;
  while ( (MessageLength-MessagePosition) > LOGBUFF_LINESIZE )
  {
    snprintf( LineBuff, LOGBUFF_LINESIZE+1, MsgBuff + MessagePosition );
    fputs( LineBuff, output_stream->fd_output );
    fputs( "\n                                  ", output_stream->fd_output );
    MessagePosition += LOGBUFF_LINESIZE;
  }
  fputs( MsgBuff + MessagePosition, output_stream->fd_output);
  
  fputs(FooterBuff, output_stream->fd_output);
  fflush(output_stream->fd_output);
  pthread_mutex_unlock(&output_stream->ptm_log);
       
#ifndef DEBUG
  }
#endif
}




/*****************************************************************************
 * Main log function 
 *****************************************************************************/
void VS_log(LOG_LEVELS level, LOG_MODULES module, char* format, ...)
{
   struct LOG_stream * ls_current=ls_none->next_stream;
 va_list ap;
  va_start(ap,format);

  /* check whether the logging system is available or not */
  pthread_mutex_lock(&ptm_log_available);
  if (is_log_available==FALSE)
  {
    return;
  }
  pthread_mutex_unlock(&ptm_log_available);
  
  while(ls_current != NULL)
  {
    switch(ls_current->type)
    {
      case LOGTYPE_FILE:
        VS_log_File(level, module, ls_current, format, ap);
        break;
      case LOGTYPE_SCREEN:
        VS_log_Screen(level, module, ls_current, format, ap);
        break;
      case LOGTYPE_TELNET:
        VS_log_Telnet(level, module, ls_current, format, ap);
        break;
      default:
    }
    ls_current=ls_current->next_stream;
  }
  
  va_end(ap);
}



/*****************************************************************************
 * Open a log file 
 ******************
 * opens a new file for logging and returns if no error the file descriptor
 * in order to be able to close it later
 *****************************************************************************/
FILE * LOG_file (char * sLogName)
{
  int iRc;
  FILE * fdLogFile;
 
  iRc=0;
  if( !(fdLogFile = fopen (sLogName, "w+")))
  {
    iRc=1;
    VS_log(LOGFATAL, LOGGER, "Unable to open %s log file ", sLogName);
  }
  else
  {
    LOG_create(fdLogFile, LOGTYPE_FILE);
  }
  
  if (iRc==1)
  {
    return NULL;
  }
  else
  {
    return fdLogFile;
  }
}



#if 0
/*****************************************************************************
 * Close the LogFile
 *****************************************************************************/
int CloseLogFile ()
{
  int iRc;
  iRc=0;

  if(fdLogFile)
  {
    iRc=fclose(fdLogFile);
    if(iRc)
    {
      VS_log(LOGERROR, LOGGER, "Unable to close file");
    }
    iRc=pthread_mutex_destroy(&ptmLogFile);
  }
  else
  {
    iRc=1;
    VS_log(LOGERROR, LOGGER, "Unable to close log file: file not open");
  }
  return iRc;
        
}

#endif


/*****************************************************************************
 * LOG_init() : Initialize the log process
 *****************************************************************************
 * TODO Gestion d'erreurs
 *****************************************************************************/
int LOG_init()
{
  /* initialization of the logs chain */
  ls_none = malloc(sizeof(struct LOG_stream));
  if(ls_none==NULL)
  {
    return(-1);
  }
  
  ls_none->fd_output=NULL;
  ls_none->type=LOGTYPE_NONE;
  ls_none->next_stream=NULL;

  pthread_mutex_init(&ptm_log_available, NULL);
  is_log_available=TRUE;
  return 0;
}


/*****************************************************************************
 * LOG_create : add a new log output to the list
 *****************************************************************************
 * 
 *****************************************************************************/

int LOG_create(FILE * output_stream, int type)
{
  struct LOG_stream * ls_new;
  struct LOG_stream * ls_current;

  ls_new = malloc(sizeof(struct LOG_stream));
  if (ls_new==NULL)
  {
    return(-1);
  }
  ls_new->fd_output=output_stream;
  ls_new->type=type;
  pthread_mutex_init( &(ls_new->ptm_log), NULL );
  ls_new->next_stream=NULL;
  
  /* inserts the new log stream et the end of the queue */
  ls_current=ls_none;
  while(ls_current->next_stream!=NULL)
  {
    ls_current=ls_current->next_stream;
  }
  ls_current->next_stream = ls_new;
  
  return 0;
}


/**********
 * LOG_delete : removes a log output from the list
 ************
 * 
 ************/

int LOG_delete(FILE * output_stream)
{
  struct LOG_stream * ls_current;
  struct LOG_stream * ls_previous;

  /* searches for the log stream */
  ls_previous=NULL;
  ls_current=ls_none;
  while (ls_current->fd_output!=output_stream)
  {

    ls_previous=ls_current;
    ls_current=ls_current->next_stream;
    if (ls_current==NULL)
    { /* error : log stream not found */
      return -1;
    }
  }
  ls_previous->next_stream=ls_current->next_stream;
  
  pthread_mutex_destroy(&ls_current->ptm_log);
  free(ls_current);
  
  return 0;
}

/****************************************************************************
 * LOG_close() : terminates le the log system 
 ****************************************************************************
 * DONE 
 ****************************************************************************/

int LOG_close()
{
  struct LOG_stream * ls_current;
  struct LOG_stream * ls_next;
 
  VS_log(LOGINFO, SERVER, "Terminating log system");
 
  pthread_mutex_lock(&ptm_log_available);
  is_log_available=FALSE;
  pthread_mutex_unlock(&ptm_log_available);
  
  ls_current=ls_none;
  while (ls_current != NULL)
  {
    ls_next=ls_current->next_stream;
    pthread_mutex_destroy(&ls_current->ptm_log);
    free(ls_current);
    ls_current=ls_next;
  }
  
  return 0;
}

/****************************************************************************
 * VS_log_errstr() : Returns a string corresponding to an ERR_CODE
 ****************************************************************************/
char * VS_log_errstr(ERR_CODE err)
{
  static char * str[]=
  {
    "no error",
    "object not found",
    "trying to do a walk on a switch is currently walked by an other thread",
    "the authentification failed",
    "bad configuration file",
    "unable to bind a socket",
    "unable to connect to remote host",
    "unable to stop the database",
    "unable to resolve the dns",
    "initiatization of the switch failed",
    "unable to open, read or write a file",
    "unable to allocate memory",
    "pb with the number",
    "parse error",
    "pipe creation error",
    "unable to write to a pipe",
    "unable to spawn a thread",
    "unable to receive data from remote host",
    "unable to send data to remote host",
    "unable to execute a select() operation",
    "snmp error",
    "unable to stop the snmp",
    "unable to open a socket",
    "time error",
    "timeout"
  };
  return str[err];
}
