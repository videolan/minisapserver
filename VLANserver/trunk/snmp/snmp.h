/*****************************************************************************
 * snmp.h
 * Header file for snmp.c
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: snmp.h,v 1.9 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __SNMP_H__
#define __SNMP_H__


ERR_CODE SNMP_init_switch(struct SNMP_snmp *snmp,VS_SwitchType type,\
                          VS_SwitchId ip,VS_PORT nports,char *community_string,\
                          VS_UNIT unit, struct SNMP_switch *zwitch);

extern unsigned int current_requests;
#endif
