/*****************************************************************************
 * snmp_switch.h
 * Definition of most SNMP structs
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: snmp_switch.h,v 1.15 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __SNMP_SWITCH_H__
#define __SNMP_SWITCH_H__

#define MAX_VLAN_NB 16
#define MAX_PORT_NB 26
#define MAX_UNIT_NB 8 

/* TODO Write a walk in order to have this table for each switch */
#define Snmp_PORT(x,y) ( (x) + 100*(y) )
#define PORT_Snmp(x,y) ( (x) - 100*(y) )
 

struct SNMP_machines_elt
{
    VS_MachineId machine;
    struct SNMP_machines_elt *next;
};

struct SNMP_walk
{
    char stop[SPRINT_MAX_LEN];
    unsigned int session_num;
    size_t stop_size;
    struct SNMP_machines_elt **machine;
    VS_PORT last_port;
};

struct SNMP_table
{
    char stop[SPRINT_MAX_LEN];
    unsigned int session_num;
    size_t stop_size;
};

struct SNMP_vlans
{
    char stop[SPRINT_MAX_LEN];
    unsigned int session_num;
    size_t stop_size;
};


struct SNMP_ports
{
    char stop[SPRINT_MAX_LEN];
    unsigned int session_num;
    size_t stop_size;
};

struct SNMP_switch
{
  VS_SwitchType type;
  VS_PORT nb_ports;
  VS_UNIT unit;
  unsigned char badinit;
  struct SNMP_vlans walk_vlans;
  struct SNMP_walk walk_macs;
  struct SNMP_table walk_table;
  struct SNMP_ports walk_ports;
  struct snmp_session s2;
  unsigned long int vlanid[MAX_VLAN_NB+1];
  unsigned long int portid[MAX_PORT_NB+1][MAX_UNIT_NB+1];
  unsigned long int ulVLTid[MAX_VLAN_NB+1];
  unsigned long int ul8021Qid[MAX_VLAN_NB+1];
  unsigned long int ulATMid;
  struct VS_port_data ports[MAX_PORT_NB+1];
  struct SNMP_machines_elt **machines;
  ERR_CODE (*get_macs)();
  ERR_CODE (*get_vlans)();
  ERR_CODE (*change_vlan)();
  ERR_CODE (*fill_table)();
  ERR_CODE (*get_next)();
  ERR_CODE (*fill_ports)();
  ERR_CODE (*end_session)();
  sem_t semaMac;
  sem_t semaVlan;
};

struct SNMP_change_vlan_struct
{
    struct SNMP_switch *zwitch;
    VS_VLAN vlan;
    VS_PORT port;
    VS_VLAN_TYPE vlanType;
    unsigned int uRowStatus;
};

struct SNMP_get_macs_struct
{
    struct snmp_session *session;
    struct snmp_pdu *pdu;
    struct SNMP_switch *zwitch;
};

struct SNMP_get_vlans_struct
{
    struct snmp_session *session;
    struct snmp_pdu *pdu;
    struct SNMP_switch *zwitch;
};

struct SNMP_fill_table_struct
{
    struct SNMP_switch *zwitch;
};

struct SNMP_fill_ports_struct
{
    struct SNMP_switch *zwitch;
};

struct SNMP_get_next_struct
{
  struct SNMP_switch *zwitch;
  struct snmp_pdu *pdu;
  struct snmp_session *session;
};

struct SNMP_end_session_struct
{
  struct SNMP_switch *zwitch;
  struct snmp_session *session;
};

union SNMP_arg_union
{
    struct SNMP_change_vlan_struct change_vlan;
    struct SNMP_get_macs_struct get_macs;
    struct SNMP_get_vlans_struct get_vlans;
    struct SNMP_fill_table_struct fill_table;
    struct SNMP_fill_ports_struct fill_ports;
    struct SNMP_get_next_struct get_next;
    struct SNMP_end_session_struct end_session;
};

#endif
