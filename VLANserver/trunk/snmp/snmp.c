/*****************************************************************************
 * snmp.c
 * Provide all snmp dependant functions like set get getnext ...
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: snmp.c,v 1.32 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>                                            /* types.h */
#include <stdlib.h>                                                /* malloc */
#include <string.h>                                                /* strdup */
#include <semaphore.h>                                          /* if_snmp.h */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */

#include "../types.h"
#include "../logger.h"

#include "snmp_switch.h"                               /* struct SNMP_switch */
#include "if_snmp.h"                                     /* struct SNMP_snmp */
#include "snmp.h"                                        /* SNMP_init_switch */
#include "callback.h"                                    /* SNMP_SW_callback */

/*****************************************************************************
 * SNMP_SW_change_vlan: Change or remove a port to/from a VLAN.
 *****************************************************************************
 * This function send a snmp set packet to the appropriate switch
 * in order to change a port to another VLAN or to remove a port from a VLAN
 *****************************************************************************/
static ERR_CODE SNMP_SW_change_vlan(union SNMP_arg_union *arg,
                                 struct SNMP_snmp *snmp)
{  
  char snmp_string[MAX_OID_LEN];
  struct snmp_session *session;
  struct snmp_pdu *pdu;
  size_t name_length;
  oid name[MAX_OID_LEN];
  struct SNMP_switch *zwitch;
  signed int rc;
  unsigned long int ulVlanId;
  char sBuffer[5];
    
  zwitch=arg->change_vlan.zwitch;
  /* Definition of the snmp_string */
  switch(arg->change_vlan.vlanType)
  {
    case NONE:
      ulVlanId=zwitch->vlanid[arg->change_vlan.vlan];
      break;
    case T8021Q:
      ulVlanId=zwitch->ul8021Qid[arg->change_vlan.vlan];
      break;
    case VLT:
      ulVlanId=zwitch->ulVLTid[arg->change_vlan.vlan];
      break;
    case ATM:
      ulVlanId=zwitch->ulATMid;
      break;
    default:
      VS_log(LOGERROR,SNMP,"Vlan type unknown");
      return VS_R_SNMP;
  }
  rc=sprintf(snmp_string,"31.1.2.1.3.%lu.%lu", ulVlanId,\
                             zwitch->portid[arg->change_vlan.port][zwitch->unit]);
  if(rc==-1)
  {
    VS_log(LOGERROR,SNMP,"Error in printing the variable");
    return VS_R_SNMP;
  }
    
  rc=sprintf(sBuffer,"%u", arg->change_vlan.uRowStatus);
  if(rc==-1)
  {
    VS_log(LOGERROR,SNMP,"Error in printing the variable");
    return VS_R_SNMP;
  }
  
  /* Init a new snmp_session from the existing one */
  session=snmp_open(&(zwitch->s2));
  if(session==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to open a new snmp session");
    return VS_R_SNMP; 
  }
    
  /* Create the new pdu */
  pdu=(struct snmp_pdu *)snmp_pdu_create(SNMP_MSG_SET);
  if(pdu==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to create the pdu");
    return VS_R_SNMP;
  }

  /* Sets the maximum length of the OID */
  name_length=MAX_OID_LEN;
  
  /* Read the OID and put it into snmp_string*/
  rc=read_objid(snmp_string,name,&name_length);
  if (!rc)
  {
    VS_log(LOGERROR,SNMP,"Unable to read objid");
    return VS_R_SNMP;
  }
  
  /* Complete the pdu */
  rc=snmp_add_var(pdu,name,name_length,'i',sBuffer);
  if(rc)
  {
    VS_log(LOGERROR,SNMP,"Unable to execute SNMPADDVAR");
    return VS_R_SNMP;
  }
    
  snmp->session_num++;
  snmp->session_num |= 1;
  session->sessid=snmp->session_num;

  /* Send the request */
  rc=snmp_send(session,pdu);
  if(!rc) 
  {
    VS_log(LOGERROR,SNMP,"Unable to send the pdu");
    snmp_free_pdu(pdu);
    return VS_R_SNMP;
  }
  return  0;
}


/*****************************************************************************
 * SNMP_SW_get_macs: Get the SDB of a switch.
 *****************************************************************************
 * This function starts a walk with the switch
 * in order to get the full SDB of it.
 *****************************************************************************/
static ERR_CODE SNMP_SW_get_macs(union SNMP_arg_union *arg,\
                                 struct SNMP_snmp *snmp)
{
  char snmp_string[MAX_OID_LEN];
  size_t name_length;
  struct snmp_session *session;
  struct snmp_pdu *pdu;
  oid name[MAX_OID_LEN];
  signed int rc;
    
  if(!snmp->session_num)
  {
    return VS_R_ALREADY_WALKING;
  }

  /* Definition of the snmp_string */
  rc=sprintf(snmp_string,".1.3.6.1.4.1.43.10.22.2.1.3.%u",
                                                   arg->get_macs.zwitch->unit); 
  if(rc==-1)
  {
    VS_log(LOGERROR,SNMP,"Unable to print the variable");
    return VS_R_SNMP;
  }
    
  /* Opening and Initializing the session s1 */
  session=snmp_open(&arg->get_macs.zwitch->s2);
  if (session==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to open a new snmp session");
    return VS_R_SNMP; 
  }
    
  name_length=MAX_OID_LEN;
  
  rc=read_objid(snmp_string,name,&name_length);
  if (!rc)
  {
    VS_log(LOGERROR,SNMP,"Unable to read objid");
    return VS_R_SNMP;
  }

  pdu=(struct snmp_pdu *)snmp_pdu_create(SNMP_MSG_GETNEXT);
  if(pdu==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to create the pdu");
    return VS_R_SNMP;
  }
  
  if (!snmp_add_null_var(pdu,name,name_length))
  {
    VS_log(LOGERROR,SNMP,"Unable to execute SNMPADDNULLVAR");
    return VS_R_SNMP;
  }
    
  /* Session id numbers */
  snmp->session_num++;
  snmp->session_num|=1;
  session->sessid=snmp->session_num;
  arg->get_macs.zwitch->walk_macs.session_num=snmp->session_num;
  arg->get_macs.zwitch->machines=malloc\
        (sizeof(struct SNMP_machines_elt*)*arg->get_macs.zwitch->nb_ports+1);
    
  if(arg->get_macs.zwitch->machines==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory");
    snmp_free_pdu(pdu);
    return VS_R_MEMORY;
  }
    
  arg->get_macs.zwitch->walk_macs.machine=arg->get_macs.zwitch->machines;
  arg->get_macs.zwitch->machines--;
  arg->get_macs.zwitch->walk_macs.last_port=1;

  /* Sending the request */
  if (!snmp_send(session,pdu))
  {
    VS_log(LOGERROR,SNMP,"Unable to send the pdu");
    snmp_free_pdu(pdu);
    free(arg->get_macs.zwitch->machines);
    return VS_R_SNMP;
  }
  
  return 0; 
}


/*****************************************************************************
 * SNMP_SW_fill_table: Fill the SNMP(VLAN) table.
 *****************************************************************************
 * This function starts a walk with the appropriate switch
 * in order to catch the SNMP id of each VLAN existing on the switch
 *****************************************************************************/
ERR_CODE SNMP_SW_fill_table(union SNMP_arg_union *arg,struct SNMP_snmp *snmp)
{
  char snmp_string[MAX_OID_LEN];
  size_t name_length;
  struct snmp_session *session;
  struct snmp_pdu *pdu;
  oid name[MAX_OID_LEN];
  signed int rc;
    
  if (!snmp->session_num)
  {
    return VS_R_ALREADY_WALKING;
  }

  /* Definition of the snmp_string */
  rc=sprintf(snmp_string,".1.3.6.1.2.1.2.2.1.2"); 
  if(rc==-1)
  {
    VS_log(LOGERROR,SNMP,"Unable to print the variable");
    return VS_R_SNMP;
  }
    
  /* Opening and Initializing the session s1 */
  if ((session=snmp_open(&arg->fill_table.zwitch->s2))==NULL)
  {
      VS_log(LOGERROR,SNMP,"Unable to open a new snmp session");
      return VS_R_SNMP; 
  }
    
  name_length=MAX_OID_LEN;
  
  if (!read_objid(snmp_string,name,&name_length))
  {
    VS_log(LOGERROR,SNMP,"Unable to read objid");
    return VS_R_SNMP;
  }

  pdu=(struct snmp_pdu *)snmp_pdu_create(SNMP_MSG_GETNEXT);
  if(pdu==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to create the pdu");
    return VS_R_SNMP;
  }
    
  if (!snmp_add_null_var(pdu,name,name_length))
  {
    VS_log(LOGERROR,SNMP,"Unable to execute SNMPADDNULLVAR");
    return VS_R_SNMP;
  }
    
  /* Session id numbers */
  snmp->session_num++;
  snmp->session_num|=1;
  session->sessid=snmp->session_num;
  arg->fill_table.zwitch->walk_table.session_num=snmp->session_num;
   
  /* Sending the request */
  if (!snmp_send(session,pdu))
  {
    VS_log(LOGERROR,SNMP,"Unable to send the pdu");
    snmp_free_pdu(pdu);
    return VS_R_SNMP;
  }
  
  return 0; 
}



/*****************************************************************************
 * SNMP_SW_fill_ports: Fill the SNMP(PORT) table.
 *****************************************************************************
 * This function starts a walk with the appropriate switch
 * in order to catch the SNMP id of each PORT existing on the switch
 *****************************************************************************/
ERR_CODE SNMP_SW_fill_ports(union SNMP_arg_union *arg,struct SNMP_snmp *snmp)
{
  char snmp_string[MAX_OID_LEN];
  size_t name_length;
  struct snmp_session *session;
  struct snmp_pdu *pdu;
  oid name[MAX_OID_LEN];
  signed int rc;
     
  if (!snmp->session_num)
  {
    return VS_R_ALREADY_WALKING;
  }

  /* Definition of the snmp_string */
  rc=sprintf(snmp_string,".1.3.6.1.2.1.2.2.1.2"); 
  if(rc==-1)
  {
    VS_log(LOGERROR,SNMP,"Unable to print the variable");
    return VS_R_SNMP;
  }
    
  /* Opening and Initializing the session s1 */
  if ((session=snmp_open(&arg->fill_ports.zwitch->s2))==NULL)
  {
      VS_log(LOGERROR,SNMP,"Unable to open a new snmp session");
      return VS_R_SNMP; 
  }
    
  name_length=MAX_OID_LEN;
  
  if (!read_objid(snmp_string,name,&name_length))
  {
    VS_log(LOGERROR,SNMP,"Unable to read objid");
    return VS_R_SNMP;
  }

  pdu=(struct snmp_pdu *)snmp_pdu_create(SNMP_MSG_GETNEXT);
  if(pdu==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to create the pdu");
    return VS_R_SNMP;
  }
    
  if (!snmp_add_null_var(pdu,name,name_length))
  {
    VS_log(LOGERROR,SNMP,"Unable to execute SNMPADDNULLVAR");
    return VS_R_SNMP;
  }
    
  /* Session id numbers */
  snmp->session_num++;
  snmp->session_num|=1;
  session->sessid=snmp->session_num;
  arg->fill_ports.zwitch->walk_ports.session_num=snmp->session_num;
   
  /* Sending the request */
  if (!snmp_send(session,pdu))
  {
    VS_log(LOGERROR,SNMP,"Unable to send the pdu");
    snmp_free_pdu(pdu);
    return VS_R_SNMP;
  }
  
  return 0; 
}


/*****************************************************************************
 * SNMP_SW_get_vlans: Fill the Port(VLAN) table.
 *****************************************************************************
 * This function starts a walk with the appropriate switch
 * in order to get the VLAN where each port is located
 *****************************************************************************/
ERR_CODE SNMP_SW_get_vlans(union SNMP_arg_union *arg,struct SNMP_snmp *snmp)
{
  char snmp_string[MAX_OID_LEN];
  size_t name_length;
  struct snmp_session *session;
  struct snmp_pdu *pdu;
  oid name[MAX_OID_LEN];
  unsigned int rc;
     
  if (!snmp->session_num)
  {
    VS_log(LOGERROR,SNMP,"Unable to open the session, already walking");
    return VS_R_ALREADY_WALKING;
  }
    
  /* Definition of the snmp_string */
  rc=sprintf(snmp_string,"31.1.2.1.3");
  if(rc==-1)
  {
    VS_log(LOGERROR,SNMP,"Unable to print the variable");
    return VS_R_SNMP;
  }
    
  /* Opening and Initializing the session s1 */
  if ((session=snmp_open(&arg->get_vlans.zwitch->s2))==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to open a new snmp session");
    return VS_R_SNMP; 
  }
    
  name_length=MAX_OID_LEN;
    
  if (!read_objid(snmp_string,name,&name_length))
  {
    VS_log(LOGERROR,SNMP,"Unable to read objid");
    return VS_R_SNMP;
  }

  pdu=(struct snmp_pdu *)snmp_pdu_create(SNMP_MSG_GETNEXT);
  if(pdu==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to create the pdu");
    return VS_R_SNMP;
  }
    
  if (!snmp_add_null_var(pdu,name,name_length))
  {
    VS_log(LOGERROR,SNMP,"Unable to execute SNMPADDNULLVAR");
    return VS_R_SNMP;
  }
    
  /* Session id numbers */
  snmp->session_num++;
  snmp->session_num|=1;
  session->sessid=snmp->session_num;
  arg->get_vlans.zwitch->walk_vlans.session_num=snmp->session_num;
   
  /* Sending the request */
  if (!snmp_send(session,pdu))
  {
    VS_log(LOGERROR,SNMP,"Unable to send the pdu");
    snmp_free_pdu(pdu);
    return VS_R_SNMP;
  }
  
  return 0; 
}



/*****************************************************************************
 * SNMP_SW_get_next: Sends a get_next request.
 *****************************************************************************
 * This function follows a walk with the appropriate switch
 *****************************************************************************/
ERR_CODE SNMP_SW_get_next(union SNMP_arg_union *arg,struct SNMP_snmp *snmp)
{
    
  /* Sending the request */
  if (!snmp_send(arg->get_next.session,arg->get_next.pdu))
  {
    VS_log(LOGERROR,SNMP,"Unable to send the pdu");
    snmp_free_pdu(arg->get_next.pdu);
    return VS_R_SNMP;
  }
  
  return 0; 
}



/*****************************************************************************
 * SNMP_SW_end_session: Ends a session.
 *****************************************************************************
 * This function end a session with a switch (end of a walk/request)
 *****************************************************************************/
ERR_CODE SNMP_SW_end_session(union SNMP_arg_union *arg,struct SNMP_snmp *snmp)
{
  /* Closing the session */
  if (!snmp_close(arg->end_session.session))
  {
    VS_log(LOGERROR,SNMP,"Unable to close the session");
    return VS_R_SNMP;
  }
  
  return 0; 
}



/*****************************************************************************
 * SNMP_init_switch: Initialize a switch.
 *****************************************************************************
 * This function initialize the struct and the SNMP(VLAN) table.
 *****************************************************************************/
ERR_CODE SNMP_init_switch(struct SNMP_snmp *snmp,VS_SwitchType type,\
                          VS_SwitchId ip,VS_PORT nports,char *community_string,\
                          VS_UNIT unit, struct SNMP_switch *zwitch)
{
  unsigned int i,j;
  
  zwitch->type=type;
  zwitch->nb_ports=nports;
  zwitch->unit=unit;
  zwitch->badinit=0;
  
  snmp_sess_init(&zwitch->s2);
    
  zwitch->s2.version=ds_get_int(DS_LIBRARY_ID, DS_LIB_SNMPVERSION);
  zwitch->s2.peername=strdup(VS_SID2A(ip));
  zwitch->s2.community=community_string;
  zwitch->s2.community_len=strlen(community_string);
  zwitch->s2.callback_magic=zwitch;
  zwitch->s2.timeout=5000000L;
  zwitch->change_vlan=SNMP_SW_change_vlan;
  zwitch->get_macs=SNMP_SW_get_macs;
  zwitch->fill_table=SNMP_SW_fill_table;
  zwitch->fill_ports=SNMP_SW_fill_ports;
  zwitch->get_vlans=SNMP_SW_get_vlans;
  zwitch->get_next=SNMP_SW_get_next;
  zwitch->end_session=SNMP_SW_end_session;

  /* XXX these lines are 3Com specific */
  {
    char buffer[29];
    sprintf(buffer,"enterprises.43.10.22.2.1.3.%1u",unit);
    strcpy(zwitch->walk_macs.stop,buffer);
    zwitch->walk_macs.stop_size=28;
  }
  zwitch->walk_table.stop_size=35;
  strcpy(zwitch->walk_table.stop,"interfaces.ifTable.ifEntry.ifDescr.");
  
  zwitch->walk_ports.stop_size=35;
  strcpy(zwitch->walk_ports.stop,"interfaces.ifTable.ifEntry.ifDescr.");
  
  zwitch->walk_vlans.stop_size=11;
  strcpy(zwitch->walk_vlans.stop,\
           "ifMIB.ifMIBObjects.ifStackTable.ifStackEntry.ifStackStatus.");
  zwitch->s2.callback=\
             (int (*)(int,struct snmp_session *,int,struct snmp_pdu *,void *))\
                                                              SNMP_SW_callback;
    
  for(i=1;i<MAX_VLAN_NB+1;i++)
  {
    zwitch->vlanid[i]=0;
    zwitch->ulVLTid[i]=0;
    zwitch->ul8021Qid[i]=0;
  }
  zwitch->ulATMid=0;
  
  for(i=1;i<MAX_PORT_NB+1;i++)
    for(j=1;j<MAX_UNIT_NB+1;j++)  
    {
      zwitch->portid[i][j]=0;
    }

  for(i=1;i<zwitch->nb_ports+1;i++)
  {
    zwitch->ports[i].sLoopNumber=0;
    zwitch->ports[i].vlan_struct=NULL;
  }

  current_requests+=4;
  VS_log(LOGDEBUG,SNMP,"New requests (CR: %d)",current_requests); 

  sem_init(&zwitch->semaMac,0,0);
  sem_init(&zwitch->semaVlan,0,0);
  
  SNMP_get_vlanids(snmp,zwitch);
  SNMP_get_portids(snmp,zwitch);
  
  return VS_R_OK;
}
