/*****************************************************************************
 * if_snmp.h
 * Header file for if_snmp.c
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: if_snmp.h,v 1.13 2001/10/08 16:37:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __IF_SNMP_H__
#define __IF_SNMP_H__

struct SNMP_req
{
  union SNMP_arg_union argument;
  ERR_CODE ret_value;
  ERR_CODE (*cmd)();
  struct SNMP_req *next;
};


struct SNMP_snmp
{
  pthread_t loop;
  pthread_mutex_t fifo_mutex;
  sem_t* sem;
  sem_t* sem_end_init;
  enum {
    SNMP_INIT,
    SNMP_RUN,
    SNMP_STOP
  } runlevel;
  int pipe[2];
  unsigned int session_num;
  int numfds;
  struct SNMP_req *first;
  struct SNMP_req *last;
  unsigned short int se_port;
};



ERR_CODE SNMP_start(struct SNMP_snmp * snmp);
ERR_CODE SNMP_stop(struct SNMP_snmp * snmp);



ERR_CODE SNMP_set_vlan(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch,\
                            VS_PORT port,VS_VLAN vlan,VS_VLAN_TYPE vlanType);

ERR_CODE SNMP_unset_vlan(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch,\
                            VS_PORT port,VS_VLAN vlan,VS_VLAN_TYPE vlanType);

ERR_CODE SNMP_get_vlanids(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch);

ERR_CODE SNMP_get_portids(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch);

ERR_CODE SNMP_get_macs(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch);

ERR_CODE SNMP_get_vlans(struct SNMP_snmp *snmp, struct SNMP_switch *zwitch);


ERR_CODE SNMP_get_next(struct SNMP_snmp *snmp,  struct SNMP_switch *zwitch,\
                           struct snmp_pdu *pdu, struct snmp_session *session);

ERR_CODE SNMP_end_session(struct SNMP_snmp *snmp,  struct SNMP_switch *zwitch,\
                                                 struct snmp_session *session);

#endif
