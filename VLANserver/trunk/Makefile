###############################################################################
# VLANserver main Makefile - (c)2001 VideoLAN
###############################################################################


# DO NOT CHANE ANYTHING IN THAT FILE
# Options are in Makefile.options


######################### C Compiler Config ###################################
#Operating system
#Default is linux
#SYS = linux-gnu
#SYS = solaris

# Use the GNU C compiler
CC = gcc

#Debug messages in color
CFLAGS += -DLINUX_COLOR

# Compiler parameters style
CFLAGS += -Wall
CFLAGS += -fomit-frame-pointer

# To generate the dependencies
DFLAGS += -MM
SHELL = /bin/sh

# Use threads
CFLAGS += -D_REENTRANT
LDLIBS += -lpthread

# Use SNMP lib
LDLIBS += -lsnmp

# Use crypt (MD5 passwords)
LDLIBS += -lcrypt

#Those are default values
# ***** DO NOT CHANGE ******
OPTIMS=6
MEM_DBG=0
DBG=0

-include Makefile.options

############################# LEXER Config ####################################

#Use flex
LEX=flex

#Aborts with an error if an input does not match any rules.
LEXFLAGS += -s

#Directs flex to construct equivalence classes. DO NOT CHANGE
LEXFLAGS += -Ce

#Changes the default yy prefix to CFG_yy. DO NOT CHANGE
LEXFLAGS += -PCFG_yy


ifeq ($(SYS),solaris)
  LDLIBS += -lrt      # semaphores
  LDLIBS += -lsocket  # socket,bind
  LDLIBS += -lnsl     # gethostbyname
  LDLIBS += -lresolv  # inet_aton
  LDLIBS += -lcrypto  # for the libsnmp ...
  LDLIBS += -lkstat   # for the libsnmp ...
endif

ifeq ($(VLB),1)
  CFLAGS += -DVLB
endif
  
ifeq ($(MEM_DBG),1)
  CFLAGS += -DMEM_DBG
endif

ifeq ($(TRAPS),1)
  CFLAGS += -DTRAPS
endif

ifeq ($(DBG),1)
  CFLAGS += -DDEBUG
  CFLAGS += -g
else
  CFLAGS += -O$(OPTIMS)
endif

########################### FILES DESCRIPTION #################################

SOURCES = \
  server/server.c \
  server/connect.c \
  server/request_handler.c \
  server/poll_switch.c\
  server/reinit_switchs.c \
  db/db.c \
  snmp/snmp.c \
  snmp/if_snmp.c \
  snmp/callback.c \
  main.c \
  types.c \
  logger.c \
  interface/interface.c\
  interface/telnet.c
ifeq ($(MEM_DBG),1)
SOURCES += debug/mem.c
endif
ifeq ($(DBG),1)
SOURCES += debug/sleep.c
endif


OBJECTS = $(SOURCES:%.c=%.o)

DEPS = $(SOURCES:%.c=deps/%.d)


LEXSOURCES = \
  config/config.fl 

LEXCFILES = $(LEXSOURCES:%.fl= %.c)

LEXDEPS = $(LEXSOURCES:%.fl= deps/%.d)
  
LEXOBJECTS = $(LEXSOURCES:%.fl= %.o)
  
TARGET=VLANserver
########################### RULES DESCRIPTION #################################

all:		dir dep $(TARGET)

dep:		$(DEPS) $(LEXDEPS)

$(TARGET):	$(OBJECTS) $(LEXOBJECTS)
		@echo "Final linking"
		$(CC) -o $@ $(OBJECTS) $(LEXOBJECTS) $(CFLAGS) $(LDLIBS) $(LPATH) $(IPATH)

$(DEPS):	deps/%.d: %.c
		@echo "Generating dependancies for $*.c"
		@$(CC) $(CFLAGS) $(IPATH) $(DFLAGS) $< | sed 's#\($*\)\.o[ :]*#\1.o $@ : #g' > $@; [ -s$@ ] || rm -f $@

$(OBJECTS):	%.o: deps/%.d
		@echo "Compiling $*.c"
		@$(CC) $(CFLAGS) $(IPATH) -c -o $@ $*.c


$(LEXDEPS):	deps/%.d: %.c
		@echo "Generating dependancies for $*.c"
		@$(CC) $(CFLAGS) $(DFLAGS) $(IPATH) $< | sed 's#\($*\)\.o[ :]*#\1.o $@ : #g' > $@; [ -s$@ ] || rm -f $@
		
$(LEXOBJECTS):	%.o: deps/%.d
		@echo "Compiling $*.c"
		@$(CC) $(CFLAGS) $(IPATH) -c -o $@ $*.c


$(LEXCFILES):	%.c: %.fl
		@echo "Running flex for generation of $*.fl"
		@$(LEX) $(LEXFLAGS) -o$@ $<

dir:
		@echo "Creating deps directory"
		@mkdir -p deps            
		@mkdir -p deps/config    
		@mkdir -p deps/snmp
		@mkdir -p deps/db  
		@mkdir -p deps/interface
		@mkdir -p deps/server
		@mkdir -p deps/debug
		
clean:
		@echo "Removing objects files, and VLANserver"
		@rm -f $(OBJECTS)
		@rm -f $(LEXOBJECTS)
		@rm -f $(TARGET)

distclean:      clean
		@echo "Removing deps directory and C lex-generated files"
		@rm -rf deps
		@rm -f $(LEXCFILES)

