/*****************************************************************************
 * server.c
 * Main part of the server
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: server.c,v 1.51 2001/11/12 15:46:10 gunther Exp $
 *
 * Authors: Laurent Rossier <gunther@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdlib.h>                                                /* malloc */
#include <strings.h>                                               /* memset */
#include <sys/socket.h>                              /* socket, bind, sendto */
#include <pthread.h>                              /* pthread_mutex_init, ... */
#include <sys/time.h>                                      /* struct timeval */
#include <unistd.h>                                                 /* close */
#include <semaphore.h>                                           /* sem_wait */
#include <arpa/inet.h>                                            /* types.h */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */
#include <netdb.h>                                          /* gethostbyname */
#include <errno.h>                                                 /* herror */

#include "../types.h"
#include "../logger.h"

#include "../config.h"                                            /* VERSION */
#include "../snmp/snmp_switch.h"                                /* if_snmp.h */
#include "../snmp/if_snmp.h"                                     /* vs->snmp */
#include "../db/db.h"                                    /* DB_switch_insert */
#include "../config/config.h"                                    /* vs-> cfg */
#include "../vlanserver.h"                                             /* vs */
#ifdef VLB
#include "connect.h"                                        /* CNX_vlb_login */
#endif
#include "server.h"                                               /* VS_stop */
#include "poll_switch.h"                                   /* VS_switch_init */
#include "request_handler.h"                    /* VS_request_handler_thread */


/*****************************************************************************
 * VS_start : Starts and inits the server thread
 *****************************************************************************
 * Return value: ERR_CODE
 *****************************************************************************/
ERR_CODE VS_start()
{
  struct sockaddr_in server_client;
  unsigned int* session_id;
  unsigned int rc;
  int iSocket;
  struct VS_info_poller * info_poller;

  /* WARNING : do not put anything that may fail or return before we initialize
   * vs->cnx_lost, or add a flag to say if it has been initialized */

  vs->runlevel=VS_INIT;

#ifdef VLB
  pthread_mutex_init(&vs->cnx_lock, NULL);
#endif
  
  session_id=malloc(sizeof(unsigned int));
  if(session_id==NULL)
  {
    VS_log(LOGERROR,SERVER,"Unable to allocate memory");
    VS_stop(vs->info_poller);
    return VS_R_MEMORY;
  }
  *session_id=0;

  vs->info_poller=malloc(sizeof(struct VS_info_poller));
  if(vs->info_poller==NULL)
  {
    VS_log(LOGERROR,SERVER,"Unable to allocate memory");
    VS_stop(vs->info_poller);
    return VS_R_MEMORY;
  }
  info_poller=vs->info_poller;
  
  rc=VS_db_thread(info_poller);
  if(rc)
  {
    VS_log(LOGERROR,SERVER,"Unable to create the thread of the db's reinit");
    VS_stop(info_poller);
    return VS_R_PTHREAD;
  }

  /* We should not run db_init before snmp_loop started */
  sem_wait(vs->snmp->sem);
  
  rc=VS_db_init(info_poller);
  if(rc)
  {
    VS_log(LOGERROR,SERVER,"Unable to initialize the database");
    VS_stop(info_poller);
    return VS_R_INIT_FAILED;
  }
  
  iSocket=VS_connect(server_client);
  if(iSocket == -1)
  {
    VS_log(LOGERROR,SERVER,"Unable to connect to the vlb or to the bind the "\
                                                                     "socket");
    VS_stop(info_poller);
    return VS_R_CONNECT;
  }

  info_poller->iSocket=iSocket;
  
  /* We are not sure wether this code should be inside the loop or not */

  vs->runlevel=VS_RUN;

  while (vs->runlevel<VS_STOP)
  {
    VS_select(iSocket, session_id); 
  }
    
  return 0;
}

/*****************************************************************************
 * VS_db_thread
 *****************************************************************************
 * Create the thread that will reinit the database
 *****************************************************************************/
ERR_CODE VS_db_thread(struct VS_info_poller * info_poller)
{
  unsigned int rc;
  pthread_t thread;
  pthread_attr_t thread_attr;

  info_poller->runlevel=VS_POLL_RUN;
  info_poller->uTime=VS_REINIT_TIME;
  pthread_attr_init (&thread_attr);
  rc=pthread_create(&thread,&thread_attr,\
                            (void *(*)())VS_reinit_switchs_thread,info_poller);
  if(rc)
  {
    VS_log(LOGERROR,SERVER,"The creation of the thread that will "\
                                "reinitialize the database doesn't succeed !");
    VS_stop(info_poller);
    return VS_R_PTHREAD;
  }
  return 0;
}

/*****************************************************************************
 * ERR_CODE VS_db_init
 *****************************************************************************
 * Initialized the database when the server start
 * It reads the config file
 *****************************************************************************/
ERR_CODE VS_db_init(struct VS_info_poller * info_poller)
{
  VS_PORT port;
  unsigned int rc;
  unsigned int i;
  struct CFG_SWITCH * z;
  struct SNMP_switch * zwitch;
  struct DB_port * ports;
  struct VS_switches * list_switch;
  
  list_switch=malloc(sizeof(struct VS_switches));

  /* Malloc of ALL the switches read in the config file */
  for(z=vs->cfg->zwitches, i=1 ; z!=NULL ; z=z->next, i++)
  {
    zwitch=malloc(sizeof(struct SNMP_switch));
    if(zwitch==NULL)
    {
      VS_log(LOGERROR,SERVER,"Unable to allocate memory");
      VS_stop(info_poller);
      return VS_R_MEMORY;
    }
    list_switch->zwitch[i]=zwitch;
    list_switch->init[i]=0;
    for (port=1 ; port<=z->nports ; port++)
    {
      list_switch->zwitch[i]->ports[port].protection=\
          z->ports[port].protection;
      list_switch->zwitch[i]->ports[port].flags=z->ports[port].flags;
    }
  } 
  
  list_switch->zwitch[i]=NULL;
  
  
  VS_log(LOGINFO,SERVER,"Sending SNMP requests");

 
  VS_switch_init(list_switch);
  
 
  /* Enter the inited swicthes inside DB */ 
  
  for(z=vs->cfg->zwitches, i=1;z!=NULL;z=z->next, i++)
  {
    if(list_switch->init[i]==1)
    {
      ports=malloc((((int)z->nports)+1)*sizeof(struct DB_port));
      if(ports==NULL)
      {
        VS_stop(info_poller);
        return VS_R_MEMORY;
      }
      rc=DB_switchs_insert(vs->db, z->ip, ports, z->nports,\
                                                       list_switch->zwitch[i]);
      if(rc)
      {
        VS_log(LOGERROR,SERVER,"Unable to insert switch in the database");
      }
    }
    else
    {
      VS_log(LOGERROR,SERVER,"Initialization of switch n� %u failed. "\
                                                                "ABORTING.",i);
    }
    
  }
  free(z);
  free(list_switch);
  return 0;
}

/*****************************************************************************
 * VS_connect
 *****************************************************************************
 * Collect information about the server for the client
 * Log the server to the vlanbridge
 * Initialize and bind the socket for the client
 *****************************************************************************/
ERR_CODE VS_connect(struct sockaddr_in server_client)
{
  unsigned int rc;
  int iSocket;
#ifdef VLB
  struct hostent * h;
#endif
  
  memset(&server_client, 0, sizeof(struct sockaddr_in));
  server_client.sin_family=AF_INET;
  server_client.sin_port=htons(DEFAULT_PORT);
  server_client.sin_addr.s_addr=INADDR_ANY;

#ifdef VLB
  if((h=gethostbyname(vs->cfg->vlanbridge))==NULL)
  {
    herror("VLANbridge address ");
    return VS_R_DNS;
  }
  VS_log(LOGINFO, SERVER, "VLANbrdige IP : %s",
         inet_ntoa(*((struct in_addr *)h->h_addr)));
  vs->vlb_params.sin_addr=*((struct in_addr *)h->h_addr);
  vs->vlb_params.sin_family=AF_INET;
  vs->vlb_params.sin_port=htons(DEFAULT_ROUTER_PORT);
  memset(&(vs->vlb_params.sin_zero), '\0', 8);
  rc=CNX_vlb_login(VERSION);
  VS_log(LOGINFO,SERVER,"vlb login %x", rc);
#endif
  
  iSocket=socket(AF_INET, SOCK_DGRAM, 0);
  if(iSocket==-1)
  {
    VS_log(LOGERROR,SERVER,"Unable to open a socketfor the client");
    return iSocket;
  }
  rc=bind(iSocket, (struct sockaddr *)(&server_client),
          sizeof(struct sockaddr));
  if(rc)
  {
    VS_log(LOGERROR,SERVER,"Unable to bind the socket for the client");
    return rc;
  }
  VS_log(LOGDEBUG,SERVER,"Bind socket for the client %d", rc);
  
  return iSocket;
}

/*****************************************************************************
 * VS_select
 *****************************************************************************
 * Select the packets the server receive
 * Receive packets from the client
 * Collect information of the client
 * Call the right function to do the job
 *****************************************************************************/
ERR_CODE VS_select(int iSocket, unsigned int * session_id)
{
  int numfds;
  fd_set readfds;
  int rc;
  unsigned int fromlen;
  struct timeval tv;
  struct sockaddr_in client;
  struct VS_info_client * info_client;

  numfds=iSocket+1;
  FD_ZERO(&readfds);
  FD_SET(iSocket, &readfds);
  
  rc=select(numfds, &readfds, 0, 0, NULL);
  if(rc!=-1)
  {
    if(FD_ISSET(iSocket, &readfds))
    {
      info_client=malloc(sizeof(struct VS_info_client));
      if(info_client==NULL)
      {
        VS_log(LOGERROR,SERVER,"Unable to allocate memory");
        return VS_R_MEMORY;
      }
      
      info_client->session_id = *session_id;

      fromlen=sizeof(struct sockaddr);
      rc=recvfrom(iSocket, info_client->mess, 80, 0,\
                                           (struct sockaddr *)(&client), &fromlen);
      if(rc==-1)
      {
        VS_log(LOGERROR,SERVER,"Unable to receive packets");
        return VS_R_RECV;
      }
      
      VS_log(LOGDEBUG,SERVER,"Message %s", info_client->mess);
      strcpy(info_client->ipaddr, inet_ntoa(client.sin_addr));
      VS_log(LOGDEBUG,SERVER,"IP %s", info_client->ipaddr);
  
      /* Getting date of the server for the database */
      rc=gettimeofday(&tv,NULL);
      if(rc)
      {
        VS_log(LOGERROR,SERVER,"Unable to get the date of the server");
        return VS_R_TIME;
      }
      info_client->date_se=tv.tv_sec;
      VS_log(LOGDEBUG,SERVER,"date %lu", info_client->date_se);

      info_client->vlanType=NONE;

      info_client->iSocket=-1;

      VS_request_handler_thread(info_client);
    }
    else
    {
      VS_log(LOGINFO,SERVER,"Unknow descriptor of the socket");
    }
  }
  else
  {
    VS_log(LOGERROR,SERVER,"Error in select statement");
    return VS_R_SELECT;
  }

  (*session_id)++;
  
  return 0;
  
}

/*****************************************************************************
 * VS_request_handler_thread
 *****************************************************************************
 * Create thread that will manage the client's request
 *****************************************************************************/
ERR_CODE VS_request_handler_thread(struct VS_info_client * info_client)
{
  unsigned int rc;
  pthread_attr_t thread_attr;
  pthread_t thread;

  pthread_attr_init (&thread_attr);
  pthread_attr_setdetachstate (&thread_attr, PTHREAD_CREATE_DETACHED);
  

  VS_log(LOGDEBUG,SERVER,"Thread creation");
  rc=pthread_create(&thread, &thread_attr,\
                                 (void *(*)())VS_request_handler, info_client);
  if (rc)
  {
    VS_log(LOGERROR,SERVER,"The creation of the thread doesn't succed !");
    return VS_R_PTHREAD;
  }

  return 0;
}

/*****************************************************************************
 * VS_stop
 *****************************************************************************
 * Stop the server
 *****************************************************************************/
ERR_CODE VS_stop(struct VS_info_poller * info_poller)
{
  vs->runlevel=VS_STOP;
  info_poller->runlevel=VS_POLL_STOP;
  /* WRNING we do not verify cnx_lost has been initialized because currently,
   * we are sure it has been */
#ifdef VLB
  pthread_mutex_destroy(&(vs->cnx_lock));
#endif
  close(info_poller->iSocket);
  if(info_poller!=NULL)
  {
    free(info_poller);
  }
  VS_log(LOGINFO,SERVER,"The vlanserver is down.");
  return 0;
}
