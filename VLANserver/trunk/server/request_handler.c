/*****************************************************************************
 * request_handler.c
 * Functions to handle a request from client
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: request_handler.c,v 1.38 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Laurent Rossier <gunther@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>                                            /* types.h */
#include <sys/socket.h>                                    /* socket, sendto */
#include <stdlib.h>                                                  /* free */
#include <string.h>                                                /* strcat */
#include <strings.h>                                               /* memset */
#include <unistd.h>                                                 /* close */
#include <pthread.h>                                                 /* db.h */
#include <semaphore.h>                                               /* db.h */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */

#include "../types.h"
#include "../logger.h"

#include "../snmp/snmp_switch.h"                       /* struct SNMP_switch */
#include "../snmp/if_snmp.h"                                /* SNMP_set_vlan */
#include "../db/db.h"                                    /* DB_machines_lock */
#include "../config/config.h"                                     /* vs->cfg */
#include "../vlanserver.h"                                             /* vs */
#ifdef VLB
#include "connect.h"                                     /* CNX_vlb_set_vlan */
#endif
#include "server.h"                                 /* struct VS_info_client */
#include "request_handler.h"                                      /* VS_send */

/*****************************************************************************
 * VS_request_handler
 *****************************************************************************
 * Scan the message from the client
 * Send a message to the client
 * Lock the machine and the port in the db
 * Call the SNMP_set_vlan and the CNX_vlb_set_vlan functions
 * Unlock the machine and the port in the db
 *****************************************************************************/
ERR_CODE VS_request_handler(struct VS_info_client * info_client)
{
  VS_MachineId macaddr_db;
  VS_CHANNEL channel_dest;
  VS_VLAN vlan_dest;
  unsigned int rc;
  unsigned int i1, i2, i3, i4, i5;
  unsigned int version_cl;
  unsigned int date_cl;
  unsigned int uType;
  int iSocket;
  struct sockaddr_in sa_client;
  char macaddr_vlb[18];
  char* sMessage;
  struct DB_machines_elt * machines_elt;
  struct SNMP_switch * zwitch;
  struct DB_port * port;
  struct VS_vlan* vlanLoop;
  struct VS_vlan* vlanLoopToChange;
  struct VS_vlan* vlanLoopToChange2;
  struct VS_vlan* vlanLoopEnd;

  macaddr_db=0;
  macaddr_vlb[17] = 0;

  sMessage=malloc(256*sizeof(char));
  if(sMessage==NULL)
  {
    VS_log(LOGWARNING,SERVER,"Unable to allocate memory");
    return VS_R_MEMORY;
  }

  /*
   * Analysis of the paquet
   */
  VS_log(LOGDEBUG,SERVER,"%s", info_client->mess);
  if(sscanf(info_client->mess, "%u %u %u %s", (unsigned *)&channel_dest,\
                                        &version_cl, &date_cl, macaddr_vlb)!=4)
  {
    VS_log(LOGERROR,SERVER,"Wrong packet received");
    return VS_R_PARSE;
  }
  VS_log(LOGINFO,SERVER,"IP %s mac %s chandest %hhu version %u sessid %u date"\
                                    "cl %u", info_client->ipaddr, macaddr_vlb,\
                   channel_dest, version_cl, info_client->session_id, date_cl);
  
  if(sscanf(macaddr_vlb, "%2x:%2x:%2x:%2x:%2x:%2x", (unsigned *)&macaddr_db,\
                                                   &i1, &i2, &i3, &i4, &i5)!=6)
  {
    VS_log(LOGERROR,SERVER,"Wrong packet received");
    return VS_R_PARSE;
  }
  macaddr_db<<=8;
  macaddr_db|=i1;
  macaddr_db<<=8;
  macaddr_db|=i2;
  macaddr_db<<=8;
  macaddr_db|=i3;
  macaddr_db<<=8;
  macaddr_db|=i4;
  macaddr_db<<=8;
  macaddr_db|=i5;

  
  /* Usefull for the VS_send function */
  if(info_client->iSocket == -1)
  {
    uType=0;
    /* Opening the socket with the client */
    iSocket=socket(AF_INET, SOCK_DGRAM, 0);
    if(iSocket==-1)
    {
      VS_log(LOGERROR,SERVER,"Unable to open a socket");
      return VS_R_SOCKET;
    }
    memset(&sa_client, 0, sizeof(struct sockaddr_in));
    sa_client.sin_family=AF_INET;
    sa_client.sin_port=htons(4312);
    inet_aton(info_client->ipaddr, &(sa_client.sin_addr));
  }
  else
  {
    uType=1;
    iSocket=info_client->iSocket;
  }
  
  
  /*
   * Effective part of the request
   */
  
  /* looking for the DB entry of the mac */
  rc=DB_machines_lock(vs->db, macaddr_db, &machines_elt);
  if(!rc)
  {
    VS_log(LOGINFO, SERVER, "switch: %s port: %u",\
                                            VS_SID2A(machines_elt->switch_ip),\
                                                 (unsigned)machines_elt->port);
    
    /* looking of the DB entry of the switch */
    rc=DB_switchs_port_lock(vs->db, machines_elt->switch_ip,\
                                           machines_elt->port, &port, &zwitch);
    if(!rc)
    {
      /*
       * MAC is found, we are going to perform the request,
       * But before, we send to the client the new information
       * (server, method, TCP port, ...)
       */
      if(channel_dest<vs->cfg->nchannels)
      {
        vlan_dest=ChannelToVlan(*(vs->cfg), channel_dest);

        if(zwitch->ports[machines_elt->port].protection<VS_PL_VIDEOLAN)
        {
          for(vlanLoop=zwitch->ports[machines_elt->port].vlan_struct;
              vlanLoop!=NULL;
              vlanLoop=vlanLoop->next)
          {
            switch(vlanLoop->vlanType)
            {
              case ATM:
              case VLT:
                /* Failed bec not supported by the server */
                /* TODO we have to implement this */
                VS_log(LOGWARNING,SERVER,"Pb to change a port which is a VLT "\
                                         "or an ATM port");
                sprintf(sMessage, "E: You are not allowed to change the VLAN of "\
                                 "this port : it is protected.");
                VS_send(uType, iSocket, sa_client, sMessage);
                DB_switchs_port_unlock(vs->db, port);
                DB_machines_unlock(vs->db, machines_elt);
                if(uType == 0)
                {
                  close(iSocket);
                }
                free(info_client);
                return 0;
              default:
                break;
            }
          }
          
          vlanLoopEnd=NULL;
          vlanLoopToChange=NULL;
          vlanLoopToChange2=NULL;
          for(vlanLoop=zwitch->ports[machines_elt->port].vlan_struct;
              vlanLoop!=NULL;
              vlanLoopEnd=vlanLoop,vlanLoop=vlanLoop->next)
          {
            if(vlanLoop->vlan == vlan_dest)
            {
              if(vlanLoop->vlanType == info_client->vlanType)
              {
                /* Nothing to do */
                if(uType == 0)
                {
                  sprintf(sMessage, vs->cfg->chan_map[channel_dest].raw_data);
                }
                else
                {
                  sprintf(sMessage,"You are already in the right channel.");
                }
                VS_log(LOGINFO,SERVER,"The client %s is already in the right "\
                                      "channel",info_client->ipaddr);
                VS_send(uType, iSocket, sa_client, sMessage);
                DB_switchs_port_unlock(vs->db, port);
                DB_machines_unlock(vs->db, machines_elt);
                if(uType == 0)
                {
                  close(iSocket);
                }
                free(info_client);
                return 0;
              }
              else
              {
                vlanLoopToChange=vlanLoop;
              }
            }
            else if((vlanLoop->vlanType == NONE) && (info_client->vlanType ==
                                                                          NONE))
            {
                vlanLoopToChange2=vlanLoop;
            }
          }

          /* Send to the client the config of the VideoLAN server */
          VS_send(uType, iSocket, sa_client,
                                      vs->cfg->chan_map[channel_dest].raw_data);
          
          if(vlanLoopEnd == NULL)
          {
            vlanLoop=malloc(sizeof(struct VS_vlan));
            if(vlanLoop == NULL)
            {
              VS_log(LOGERROR,SERVER,"Unable to allocate memory");
              
              DB_switchs_port_unlock(vs->db, port);
              DB_machines_unlock(vs->db, machines_elt);
                  
              /* No need to send a message to the client because it doesn't
               * have to know that there is no more memory. By the way this
               * possibility won't come a lot ! :) */
              
              if(uType == 0)
              {
                close(iSocket);
              }
              free(info_client);
              return VS_R_MEMORY;
            }
            vlanLoop->vlan=2; /* By default the vlan is 2 
                               * We will set it to the right vlan after */
            vlanLoop->vlanType=NONE; /* Same thing */
            vlanLoop->next=NULL;
            vlanLoop->prev=NULL;
          }
          else if((vlanLoopToChange == NULL) && (vlanLoopToChange2 == NULL))
          {
            vlanLoopEnd->next=malloc(sizeof(struct VS_vlan));
            if(vlanLoopEnd->next == NULL)
            {
              VS_log(LOGERROR,SERVER,"Unable to allocate memory");
              
              DB_switchs_port_unlock(vs->db, port);
              DB_machines_unlock(vs->db, machines_elt);
                  
              /* No need to send a message to the client because it doesn't
               * have to know that there is no more memory. By the way this
               * possibility won't come a lot ! :) */
              
              if(uType == 0)
              {
                close(iSocket);
              }
              free(info_client);
              return VS_R_MEMORY;
            }
            vlanLoopEnd->next->vlan=2; /* By default the vlan is 2 
                                    * We will set it to the right vlan after */
            vlanLoopEnd->next->vlanType=NONE; /* Same thing */
            vlanLoopEnd->next->next=NULL;
            vlanLoopEnd->next->prev=vlanLoopEnd;
            vlanLoop=vlanLoopEnd->next;
          }
          else if(vlanLoopToChange == NULL)
          {
            vlanLoop=vlanLoopToChange2;
          }
          else if(vlanLoopToChange2 == NULL)
          {
            vlanLoop=vlanLoopToChange;
            rc=SNMP_unset_vlan(vs->snmp, zwitch, machines_elt->port,
                               vlanLoopToChange->vlan,
                               vlanLoopToChange->vlanType);
            if(rc)
            {
              VS_log(LOGERROR,SERVER,"Unable to call the SNMP_unset_vlan"\
                                     "function");
              VS_log(LOGERROR,SERVER,"Request interrupted");
              
              DB_switchs_port_unlock(vs->db, port);
              DB_machines_unlock(vs->db, machines_elt);
              
              if(uType == 0)
              {
                close(iSocket);
              }
              free(info_client);
              return 0;
            }
          }
          else
          {
            rc=SNMP_unset_vlan(vs->snmp, zwitch, machines_elt->port,
                               vlanLoopToChange->vlan,
                               vlanLoopToChange->vlanType);
            if(rc)
            {
              VS_log(LOGERROR,SERVER,"Unable to call the SNMP_unset_vlan"\
                                     "function");
              VS_log(LOGERROR,SERVER,"Request interrupted");
              
              DB_switchs_port_unlock(vs->db, port);
              DB_machines_unlock(vs->db, machines_elt);
              
              if(uType == 0)
              {
                close(iSocket);
              }
              free(info_client);
              return 0;
            }
            
            if(vlanLoopToChange->next != NULL)
            {
              vlanLoopToChange->next->prev=vlanLoopToChange->prev;
            }
            if(vlanLoopToChange->prev != NULL)
            {
              vlanLoopToChange->prev->next=vlanLoopToChange->next;
            }
            else
            {
              zwitch->ports[machines_elt->port].vlan_struct=
                                                        vlanLoopToChange->next;
            }
            free(vlanLoopToChange);
            vlanLoop=vlanLoopToChange2;
          }
            
          /* Asking the SNMP to change VLAN */
          rc=SNMP_set_vlan(vs->snmp, zwitch, machines_elt->port, vlan_dest,
                           info_client->vlanType);
          if(!rc)
          {
#ifdef VLB
            /* Asking the VLB to bridge */
            /* TODO We have here to bridge all macs known on the port */
            if(info_client->vlanType == NONE)
            {
              CNX_vlb_set_vlan(3, macaddr_vlb, info_client->ipaddr,\
                               vlan_dest, vlanLoop->vlan);
            }
#endif
	          vlanLoop->vlan=(unsigned short int)vlan_dest;
            vlanLoop->vlanType=info_client->vlanType;

            VS_log(LOGDEBUG,SERVER,"The port %u is in:", machines_elt->port);
            for(vlanLoop=zwitch->ports[machines_elt->port].vlan_struct;
                vlanLoop!=NULL;
                vlanLoop=vlanLoop->next)
            {
              VS_log(LOGDEBUG,SERVER,"  vlan %hi whith the type %i",
                     vlanLoop->vlan,vlanLoop->vlanType);
            }
 
            /* Changing the DB entry */
            machines_elt->channel=channel_dest;
            VS_log(LOGINFO, SERVER, "succeed to change client's channel");
          }
          else
          {
            free(vlanLoop->next);
            vlanLoop->next=NULL;
            /* Failed because of SNMP */
            VS_log(LOGERROR,SERVER,"Unable to call the SNMP_set_vlan function");
            VS_log(LOGERROR,SERVER,"Request interrupted");
            sprintf(sMessage, "E: Request interrupted.");
            VS_send(uType, iSocket, sa_client, sMessage);
          }
        }
        else
        {
          /* Failed because of permission */
          VS_log(LOGWARNING,SERVER,"Permission denied to change the VLAN of a "\
                                                               "protected port");
          sprintf(sMessage, "E: You are not allowed to change the channel of this "\
                            "port : it is protected.");
          VS_send(uType, iSocket, sa_client, sMessage);
        }
      }
      else
      {
        VS_log(LOGERROR,SERVER,"Bad channel dest from client, %hhu",
                                                              channel_dest);
        sprintf(sMessage, "E: The channel %u is not available on this server.",\
                                                    (unsigned int)channel_dest);
        VS_send(uType, iSocket, sa_client, sMessage);
      }
      
      DB_switchs_port_unlock(vs->db, port);
    }
    else
    {
      /* Failed because of db */
      VS_log(LOGINFO,SERVER,"Pb with the DB_switchs_port_lock function");
      sprintf(sMessage, "E: Request interrupted.");
      VS_send(uType, iSocket, sa_client, sMessage);
    }

    DB_machines_unlock(vs->db, machines_elt);
  }
  else
  {
    /* Failed because of mac entry in db not found */
    VS_log(LOGERROR,SERVER,"MAC not found in database");
    sprintf(sMessage, "E: Your computer is not recorded in our database. "\
                      "Please wait %u minutes and try again.",
                                                     vs->info_poller->uTime/60);
    VS_send(uType, iSocket, sa_client, sMessage);
    if(uType == 0)
    {
      close(iSocket);
    }
    free(info_client);
    return rc;
  }
  if(uType == 0)
  {
    close(iSocket);
  }
  free(info_client);
  return 0;
}

/*****************************************************************************
 * VS_send
 *****************************************************************************
 * Test who make the request and send it a message
 *****************************************************************************/
ERR_CODE VS_send(unsigned int uType, int iSocket, struct sockaddr_in client,
                 char* sMess)
{
  if(uType == 0)
  {
    sendto(iSocket, sMess, strlen(sMess)+1, 0, (struct sockaddr *)(&client),
           sizeof(struct sockaddr));
  }
  else
  {
    sMess=strcat(sMess,"\r\n");
    write(iSocket, sMess, strlen(sMess));
  }
  return VS_R_OK;
}
