/*****************************************************************************
 * reinit_switchs.c
 * Provide a way to reload configuration every n minutes
 * Warning: header are in server.c
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: reinit_switchs.c,v 1.13 2001/11/12 16:31:06 gunther Exp $
 *
 * Authors: Laurent Rossier <gunther@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef DEBUG
#  include <unistd.h>                                               /* sleep */
#endif
#include <arpa/inet.h>                                            /* types.h */
#include <stdlib.h>                                                /* malloc */
#include <semaphore.h>                                          /* if_snmp.h */
#include <pthread.h>                                                 /* db.h */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */

#include "../types.h"
#include "../logger.h"

#include "../snmp/snmp_switch.h"                                /* if_snmp.h */
#include "../snmp/if_snmp.h"                                   /* SNMP_start */
#include "../db/db.h"                                            /* DB_flush */
#include "../vlanserver.h"                                             /* vs */
#include "server.h"                                    /* VS_reinit_switches */

#ifdef DEBUG
#  include "../debug/sleep.h"                                    /* VS_sleep */
#endif

/*****************************************************************************
 * VS_reinit_switchs_thread
 *****************************************************************************
 * Call the VS_reinit_switchs function every X sec
 *****************************************************************************/
ERR_CODE VS_reinit_switchs_thread(struct VS_info_poller * info_poller)
{
  unsigned int rc;
  
  while (info_poller->runlevel<VS_POLL_STOP)
  {
    while(info_poller->uTime!=0)
    {
#ifdef DEBUG
      VS_sleep(1);
#else
      sleep(1);
#endif
      if(info_poller->uTime%300 == 0)
      {
        VS_log(LOGINFO,SERVER,"%u minutes left before the next reinit of the "\
                                                   "db", info_poller->uTime/60);
      }
      info_poller->uTime--;
    }
    VS_log(LOGINFO, SERVER, "Reinitialization of the db");
    vs->info_poller->uTime=VS_REINIT_TIME;
    rc=VS_reinit_switchs (info_poller);
    if(rc)
    {
      return VS_R_INIT_FAILED;
    }
  }
  return 0;
}


/*****************************************************************************
 * VS_reinit_switchs
 *****************************************************************************
 * Stop the snmp
 * Flush the database
 * Start the snmp
 * Call the VS_db_init function to reinitialize the databse
 *****************************************************************************/
ERR_CODE VS_reinit_switchs (struct VS_info_poller * info_poller)
{
  int rc;
  struct SNMP_switch ** list_switch;

  list_switch=malloc(MAX_SWITCH*sizeof(struct SNMP_switch));
  if(list_switch==NULL)
  {
    VS_log(LOGERROR,SERVER,"Unable to allocate memory");
    VS_stop(info_poller);
    return VS_R_MEMORY;
  }

  rc=SNMP_stop(vs->snmp);
  if(rc)
  {
    VS_log(LOGERROR, SERVER, "unable to stop the snmp");
    VS_log(LOGFATAL, SERVER, "stop the reinit operation of the db");
    VS_stop(info_poller);
    return VS_R_SNMP_STOP;
  }
  /* sleep for the timeout of the switches */
#ifdef DEBUG
  VS_sleep(12);
#else
  sleep(12);
#endif

  rc=DB_flush(vs->db);
  if(rc)
  {
    VS_log(LOGERROR, SERVER, "unable to stop the db");
    VS_log(LOGFATAL, SERVER, "stop the reinit operation of the db");
    VS_stop(info_poller);
    return VS_R_DB_STOP;
  }


  sem_init(vs->snmp->sem,0,0);
  rc=SNMP_start(vs->snmp);
  if(rc)
  {
    VS_log(LOGERROR, SERVER, "unable to restart the snmp");
    return VS_R_SNMP;
  }

  /* We should not start the db_init before snmp loop started */
  sem_wait(vs->snmp->sem);
  
  rc=VS_db_init(info_poller);
  if(rc)
  {
    VS_log(LOGERROR,SERVER,"Unable to initialize the database");
    VS_stop(info_poller);
    return VS_R_INIT_FAILED;
  }

  VS_log(LOGINFO, SERVER, "The reinitialization of the db is done");
  VS_log(LOGINFO, SERVER, "Next reload in %u minutes and %u secondes",
                                  info_poller->uTime/60, info_poller->uTime%60);

  return 0;
}
