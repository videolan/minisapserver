/*****************************************************************************
 * client.c
 * Function to use to communicate with the VLANserver
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: client.c,v 1.21 2001/11/12 15:46:09 gunther Exp $
 *
 * Authors: Laurent Rossier <gunther@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>                                            /* types.h */
#include <stdio.h>                                        /* printf, sprintf */
#include <strings.h>                                               /* memset */
#include <stdlib.h>                                                  /* free */
#include <unistd.h>                                                 /* close */
#include <net/if.h>                                             /* interface */
#include <sys/ioctl.h>                                              /* ioctl */
#include <sys/socket.h>                                      /* bind, sendto */
#include <sys/time.h>                                                /* time */
#include <netdb.h>                                          /* gethostbyname */
#include <errno.h>                                                 /* herror */

#include "../types.h"
#include "../config.h"

#include "client.h"

#define INTERFACE "eth0"


/* vishnou*/
//#define SERVER "vishnou.via.ecp.fr"
/* titi */
//#define SERVER "titi.via.ecp.fr"
/* obelix */
//#define SERVER "obelix.via.ecp.fr"
/* ekaki */
//#define SERVER "ekaki.via.ecp.fr"
/* blutch */
//#define SERVER "blutch.via.ecp.fr"
/* maceo [Tooney] */
//#define SERVER "maceo.via.ecp.fr"
/* toji */
//#define SERVER "toji.via.ecp.fr"
/* vlcs */
#define SERVER "vlcs.via.ecp.fr"

/*****************************************************************************
 * VS_client
 *****************************************************************************
 * Send to the client the order to change the channel to Channel_dest
 *****************************************************************************/
int VS_client(VS_CHANNEL Channel_dest)
{
    int	socket_cl;
    int fromlen;
    struct ifreq interface;
    struct sockaddr_in sa_server;
    struct sockaddr_in sa_client;
    unsigned int version;
    char mess[mess_length];
    struct timeval *date_cl;
    struct timeval time;
    struct hostent * h;
    long unsigned int date;
    int nbanswer;
    char* sAnswer;
    fd_set rfds;
    unsigned int rc;


    version=12;

    sAnswer=malloc(256*sizeof(char));
    if(sAnswer==NULL)
    {
      printf("Memory Error\n");
      return VS_R_MEMORY;
    }
    
    /*      
     * Looking for information about the eth0 interface
     */
    interface.ifr_addr.sa_family=AF_INET;
    strcpy(interface.ifr_name,INTERFACE);
    
    
    /*
     * Initializing the socket
     */
    socket_cl=socket(AF_INET,SOCK_DGRAM,0);

    
    /* 
     * Getting the server's information 
     */
    if((h=gethostbyname(SERVER))==NULL)
    {
      herror("Server name ");
      return 1;
    }
    memset(&sa_server,0,sizeof(struct sockaddr_in));
    sa_server.sin_family=AF_INET;
    sa_server.sin_port=htons(DEFAULT_PORT);
    sa_server.sin_addr=*((struct in_addr *)h->h_addr);
    
    /*
     * Looking for the interface MAC address
     */
    ioctl(socket_cl,SIOCGIFHWADDR,&interface);
    
    /*
     * Getting date of the client
     */
    date_cl=malloc(sizeof(struct timeval));
    if(date_cl==NULL)
    {
        printf("Unable to allocate memory\n");
        return VS_R_MEMORY;
    }
    
    if (gettimeofday(date_cl,0)==-1)
        return -1;
    date=date_cl->tv_sec;
    free(date_cl);

    /* 
     * Build of the message
     */
    sprintf(mess,"%d %u %lu %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x \n",
        Channel_dest,version, date,
        interface.ifr_hwaddr.sa_data[0] & 0xff, 
        interface.ifr_hwaddr.sa_data[1] & 0xff,
        interface.ifr_hwaddr.sa_data[2] & 0xff,
        interface.ifr_hwaddr.sa_data[3] & 0xff,
        interface.ifr_hwaddr.sa_data[4] & 0xff,
        interface.ifr_hwaddr.sa_data[5] & 0xff);
 
    /*
     * Open the socket 2
     */
    memset(&sa_client,0,sizeof(struct sockaddr_in));
    sa_client.sin_family=AF_INET;
    sa_client.sin_port=htons(4312);
    sa_client.sin_addr.s_addr=INADDR_ANY;
    fromlen=sizeof(struct sockaddr);
    rc=bind(socket_cl,(struct sockaddr *)(&sa_client),sizeof(struct sockaddr));
    if (rc)
    {
        printf("Unable to bind socket:%u\n",rc); 
    // TODO mettre un VS_R_BIND dans types.h
        return VS_R_SOCKET;
    }


    /*
     * Send the message
     */
    sendto(socket_cl,mess,mess_length,0,(struct sockaddr *)(&sa_server),\
           sizeof(struct sockaddr));
    
     /*
     * Waiting 5 sec for one answer from the server
     */
    time.tv_sec=5;
    time.tv_usec=0;
    FD_ZERO(&rfds);
    FD_SET(socket_cl,&rfds);
    nbanswer=select(socket_cl+1,&rfds,NULL,NULL,&time);
    if(nbanswer==0)
    {
      printf("No answer from the server\n");
      close(socket_cl);
      return -2;
    }
    else if(nbanswer==-1)
    {
        printf("Unable to receive the answer\n");
    }
    else
    {
        recvfrom(socket_cl,sAnswer,256,0,\
                 (struct sockaddr *)(&sa_client),&fromlen);
        printf("channelserver's answer : \n%s\n", sAnswer);
    }
    
    /*
     * Close the socket
     */
    close(socket_cl);

    return 0;
}
