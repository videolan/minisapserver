/*****************************************************************************
 * main.c
 * Main function
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: main.c,v 1.11 2001/04/29 03:41:49 nitrox Exp $
 *
 * Authors: Brieuc Jeuhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>                                            /* types.h */
#include <stdlib.h>                                                /* malloc */
#include <netdb.h>                                              /* if_snmp.h */
#include <semaphore.h>                                               /* db.h */
#include <pthread.h>                                                 /* db.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */

#include "types.h"
#include "logger.h"

#include "config/config.h"                               /* struct VS_config */
#include "db/db.h"                                           /* struct DB_db */
#include "snmp/snmp_switch.h"                                   /* if_snmp.h */
#include "snmp/if_snmp.h"                                /* struct SNMP_snmp */
#include "server/server.h"                                        /* VS_init */
#include "vlanserver.h"                                 /* struct VLANserver */
#ifdef MEM_DBG
# include "debug/mem.h"                                          /* mem_init */
#endif

/* Global  Variable */
struct VLANserver *vs;

/*****************************************************************************
 * MAIN
 *****************************************************************************
 * Malloc vs
 * Call VS_init
 *****************************************************************************/
int main(int argc,char *argv[])
{
  ERR_CODE rc;

#ifdef MEM_DBG
  mem_init();
#endif

  vs=malloc(sizeof(struct VLANserver));
  vs->db=malloc(sizeof(struct DB_db));
  vs->cfg=malloc(sizeof(struct VS_config));
  vs->snmp=malloc(sizeof(struct SNMP_snmp));
  
  if(vs==NULL||vs->db==NULL||vs->cfg==NULL||vs->snmp==NULL)
  {
    VS_log(LOGFATAL,SERVER,"Unable to allocate memory");
    return 1;
  }
  rc=VS_init(argc,argv);
  if (rc)
  {
    VS_log(LOGFATAL,SERVER,"%s",VS_log_errstr(rc));
    return rc;
  }
  return 0;
}
