/*****************************************************************************
 * mem.c
 * Add a memory limitation
 * Log every malloc and free in a file
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: mem.c,v 1.1 2001/04/29 03:41:49 nitrox Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <malloc.h>


/* Global variables used to hold underlaying hook values.  */
void *(*old_malloc_hook) (size_t);
void (*old_free_hook) (void*);

/* Global variables to define log file */
static FILE * fd;

/* Prototypes for our hooks.  */
void *my_malloc_hook (size_t);
void my_free_hook (void*);

void * my_malloc_hook (size_t size)
{
    void *result;
    /* Restore all old hooks */
    __malloc_hook = old_malloc_hook;
    __free_hook = old_free_hook;
    /* Call recursively */
    result = malloc (size);
    /* Save underlaying hooks */
    old_malloc_hook = __malloc_hook;
    old_free_hook = __free_hook;
    /* printf might call malloc, so protect it too. */
    fprintf (fd,"malloc for (%u) returns %p\n", (unsigned int) size, result);
    fflush(fd);
    /* Restore our own hooks */
    __malloc_hook = my_malloc_hook;
    __free_hook = my_free_hook;
    return result;
}

void my_free_hook (void *ptr)
{
    /* Restore all old hooks */
    __malloc_hook = old_malloc_hook;
    __free_hook = old_free_hook;

    /* Call recursively */
    free (ptr);
    /* Save underlaying hooks */
    old_malloc_hook = __malloc_hook;
    old_free_hook = __free_hook;
    /* printf might call free, so protect it too. */
    fprintf (fd,"freed pointer %p\n", ptr);
    fflush(fd);
    /* Restore our own hooks */
    __malloc_hook = my_malloc_hook;
    __free_hook = my_free_hook;
}

void mem_init (void)
{
  struct rlimit a;
  int i;
  
  fd=fopen("test.malloc","w+");
  old_malloc_hook = __malloc_hook;
  old_free_hook = __free_hook;
  __malloc_hook = my_malloc_hook;
  __free_hook = my_free_hook;

  a.rlim_cur=33000000;
  a.rlim_max=60000000;
  i=setrlimit(RLIMIT_DATA,&a);
  i|=setrlimit(RLIMIT_STACK,&a);
  i|=setrlimit(RLIMIT_MEMLOCK,&a);
  i|=setrlimit(RLIMIT_AS,&a);
}
