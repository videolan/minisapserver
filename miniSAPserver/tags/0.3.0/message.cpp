/*****************************************************************************
 * message.cpp : SAP Message class
 ****************************************************************************
 * Copyright (C) 1998-2004 VideoLAN
 * $Id$
 *
 * Authors: Damien Lucas <nitrox@videolan.org>
 *          Philippe Van Hecke <philippe.vanhecke@belnet.be>
 *          Derk-Jan Hartman <hartman at videolan dot org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

#include <sys/time.h>

#include "sapserver.h"
#include "program.h"
#include "message.h"

/*****************************************************************************
 * Constructors
 *****************************************************************************/
Message::Message(char t, uint16_t v, const char* ip)
{

    version = v;
    compressed = 0;
    encrypted = 0;
    type = t;
    ip_server=inet_addr(ip); //TODO automaticaly get it ?
    msg=NULL;
    sdp="";
    sap=NULL;
    return;
}

/*****************************************************************************
 * Destructor
 *****************************************************************************/
Message::~Message(void)
{
    if(msg) free(msg);
    if(sap) free(sap);
    return;
}


void Message::SetEncrypted(void)
{
    fprintf(stderr, "Encryption is not available\n");
    return;
}

void Message::SetCompressed(void)
{
    fprintf(stderr, "Compression is not available\n");
    return;
}



/*****************************************************************************
 * BuildHeader - Build the Message Header once initialized
 *               According to RFC 2974 (SAP)
 *****************************************************************************/
void Message::BuildHeader(const char* payload_type)
{
    unsigned int mh_size;

    mh_size = 4
            + 4 /*(IPv4 specific)*/
    /*      + authentification length (N/A)*/
            + strlen(payload_type) + 1;

    sap=(char*)malloc(mh_size);


    /* Byte 1 : Version number V1        = 001      (3 bits)
     *          Address type   IPv4/IPv6 = 0/1      (1 bit)
     *          Reserved                   0        (1 bit)
     *          Message Type   ann/del   = 0/1      (1 bit)
     *          Encryption     on/off    = 0/1      (1 bit)
     *          Compressed     on/off    = 0/1      (1 bit) */
    sap[0]=0x20;
    //if ( ip_version == SAP_IPV6 )     sap[0]|=0x10;
    if ( type == SAP_DELETE )           sap[0]|=0x04;
    if ( encrypted == SAP_ON )          sap[0]|=0x02;
    if ( compressed == SAP_ON )         sap[0]|=0x01;

    /* Byte 2 : Authentification length - Not supported */
    sap[1]=0x00;

    /* Byte 3 & Byte 4 : Message Id Hash */
    sap[2]=version;
    sap[3]=version>>8;

    /* next 4 (or 16) byte: Originating source */
    sap[4]=ip_server;
    sap[5]=ip_server>>8;
    sap[6]=ip_server>>16;
    sap[7]=ip_server>>24;

    strncpy(sap+8, payload_type, 15);
    sap[8+strlen(payload_type)]='\0';

    header_len=9+strlen(payload_type);
    return;
}

void Message::AddProgram(Program *p)
{
    /* FIXME */
    /* Currently only announces a single mediasession
     * and only a MPEG2 TS/RTP stream announce is supported
     * RFC 2327 Compliance ? */

    string v="v=0\n";  // SDP version
    string ver="";
    stringstream ssin(ver);
    ssin << version;
    ver = ssin.str();
    string o="o="+p->GetUser()+" "+ver+" 1 IN IP4 "+p->GetMachine()+"\n";
    string s="s="+p->GetName()+"\n";
    string u="u="+p->GetSite()+"\n";
    string t;
    if(p->IsPermanent())
    {
        t="t=0 0\n";
    }
    else
    {
        /* TODO */
        t="t=0 0\n";
        fprintf(stderr, "Non permanent program not supported ...\n");
        fprintf(stderr, "Announcing permanent session instead\n");
    }
    string m = "m=video " + p->GetPort() + " "
             + (p->IsRTP() ? "RTP/AVP" : "udp") +" 33\n";

    string c;

    if(p->GetIPVersion() == 6)
        c="c=IN IP6 ["+p->GetAddress()+"]/"+p->GetTTL()+"\n";
    else
        c="c=IN IP4 "+p->GetAddress()+"/"+p->GetTTL()+"\n";
    // TODO ttl type should be specified

    string a = p->IsRTP()
        ? "a=rtpmap:33 MP2T/90000\n"
        : "a=type:udp\na=mux:m2t\na=packetformat:RAW\n";

    a+= "a=tool:"+(string)PACKAGE_STRING+"\n";

    if( p->HasPlGroup() )
    {
        a+= "a=x-plgroup:"+p->GetPlGroup()+"\n";
    }

    sdp += v + o + s + u + t + m + c + a;

    printf("%s\n",sdp.c_str());

    return;
}

void Message::CreateFinalMessage(void)
{
    /* Deletes previous message */
    if(msg != NULL)
        free(msg);

    /* Check the length */
    msg_len = sdp.size() + header_len;
    if(msg_len>1024)
    {
        msg_len=0;
        msg=NULL;  //RFC 2974 Chap 6.
        return;
    }


    msg=(char*)malloc(msg_len*sizeof(char));

    unsigned int i;

    for(i=0; i<header_len; i++)
    {
        msg[i]=sap[i];
    }
    for(; i<msg_len; i++)
    {
        msg[i]=sdp[i-header_len];
    }

}



char* Message::GetFinalMessage(){return msg;}

unsigned int Message::GetFinalMessageLen(){return msg_len;}
