/*****************************************************************************
 * message.h : SAP Message Class definition
 ****************************************************************************
 * Copyright (C) 1998-2003 VideoLAN
 * $Id$
 *
 * Authors: Damien Lucas <nitrox@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#define SAP_ON 1
#define SAP_OFF 0
#define SAP_ANNOUNCE 0
#define SAP_DELETE 1
#define SAP_IPV4 4
#define SAP_IPV6 6

class Message {
  public:
    Message(char message_type, uint16_t version, const char* ip);
    ~Message();
    void BuildHeader(const char* payload_type);
    void AddProgram(Program*);
    void SetEncrypted();
    void SetCompressed();
    void CreateFinalMessage();
    char* GetFinalMessage();
    unsigned int GetFinalMessageLen();

  private:
    uint16_t version;           // Version of this annoucement
    bool compressed;            // Is the message compressed with zlib ?
    bool encrypted;             // Is the message encrypted ?
    uint8_t type;               // Announce or Deletion
    uint32_t ip_server;         // IP of the server (IPV4 specific)

    char* sap;                  // header of the message
    string sdp;                 // SDP message
    char* msg;                  // final message
    unsigned int header_len;    //len of the SAP HEADER
    unsigned int msg_len;       //len of the complete packet
};
