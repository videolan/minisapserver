#!/home/diff/bin/php -q
<?
// this script updates all the MACs in the DB and vlans for all ports


include("snmp.inc");
include("database.inc");

base_Init();

base_IncreaseUnseen();

$request=pg_exec($pg_handle, 
    "select switch_id, switch_name, switch_ip, unit_number, ".
    "vlan1_id, vlan2_id, vlan3_id, vlan4_id,".
    "vlan5_id, vlan6_id, vlan7_id, vlan8_id,".
    "vlan9_id, vlan10_id, vlan11_id, vlan12_id,".
    "vlan13_id, vlan14_id, vlan15_id, vlan16_id,".
    "switch_type, community_string from switch;"
    );
    //"switch_type, community_string from switch where switch_name = 'switch-i3';");
$nb_switches=pg_numrows($request);
for($num_switch=0 ; $num_switch<$nb_switches ; $num_switch++)
{
    $row=pg_fetch_array($request, $num_switch);
    $switch_id=$row["switch_id"];
    echo $row["switch_name"]." : ";

    // builds the array to make correspondance between vlan numbers
    // and vlan_ids
    unset($vlans);
    for( $vlan_num=1 ; $vlan_num<=16; $vlan_num++)
    {
        $vlans[$row["vlan"."$vlan_num"."_id"]]=$vlan_num;
    }
    
    // builds the array to make correspodance betwenn port numbers
    // and port_ids
    unset($ports);
    $res=pg_exec($pg_handle,
       "select port_internal_id, port_number from port where switch_id=$switch_id and port_internal_id>=0");
    for( $port=0 ; $port<pg_numrows($res) ; $port++ )
    {
        $row_port=pg_fetch_row($res,$port);
        $port_id=$row_port[0];
        $port_num=$row_port[1];
        $ports["$port_id"]=$port_num;
    }
      
    // Gets the switching database of the switch
    $sdb=snmp_get_sdb( $row["switch_ip"], 
                       $row["unit_number"], 
                       $row["community_string"],
           $row["switch_type"] );
    foreach ( $sdb as $mac => $port_num )
    {
        $res=pg_exec($pg_handle,
                    "select port_id from port where ".
            "switch_id=$switch_id and ".
            "port_number=$port_num and port_protection<=1");
        if (pg_numrows($res)==1)
        {
        //inserts the MAC in the DB if port exists
            $port=pg_fetch_row($res, 0);
            set_Machine( $port[0], $mac );
        }
    }

    // Gets the vlan in which the ports are 
    $vlantab=snmp_get_vlan($row["switch_ip"], 
                       $row["unit_number"], 
                       $row["community_string"]);
    foreach($vlantab as $portid => $vlanid)
    {
        if (isset($ports["$portid"]) && isset($vlans["$vlanid"]))
        {
        // update the entry in the database
            update_Vlan( $switch_id, 
                         $ports["$portid"], 
                         $vlans["$vlanid"] );
        }
    }
}

$unseen=base_UnseenLimit();

if ( !$unseen )
{
    base_Close();
    closelog();
    exit( 1 );
}

foreach($unseen as $mac_ad)
{
//    system("./change_channel.php 0 $mac_ad 0.0.0.0");
// trouver un truc plus propre que ��
}

base_Close();
closelog();
?>
