#!/home/diff/bin/php -q
<?
// This script lauches walk on switch in order to get vlanIds an portIds 
// of all the switches and stores them into the DB
include("snmp.inc");
include("database.inc");

base_Init();

$request=pg_exec($pg_handle, 
        "select switch_id, switch_name, switch_ip, unit_number, switch_type, community_string from switch;");
//        "select switch_id, switch_name, switch_ip, unit_number, switch_type, community_string from switch where switch_name = 'switch-i3';");
$nb_switches=pg_numrows($request);
for($num_switch=0 ; $num_switch<$nb_switches ; $num_switch++)
{
    $row=pg_fetch_array($request, $num_switch);
    echo $row["switch_name"]."\n";
    $switch_id=$row["switch_id"];

    /* gets the portIds and vlanIds of the switch */
    $id_list=snmp_walk_switch_ids( 	$row["switch_ip"], 
                                    $row["unit_number"], 
                                    $row['switch_type'],
                                    $row["community_string"]);
    foreach ( $id_list as $key => $id)
    {
        list($type, $number)=explode(" ",$key);
        if ($type=="Port")
        {
            set_PortInternalId($switch_id, $number, $id);
        }
        else if ($type=="Vlan")
        {
            set_VlanId($switch_id, $number, $id);
        }
        else
        {
            syslog(LOG_WARNING, "warning : unknown type of id");
        }
    }
}

base_Close();

?>
