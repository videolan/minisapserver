#!/home/diff/bin/php -q
<?
/*****************************************************************************
 * change_channel.php: PostgreSQL and SNMP queries for VLCS
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000 VideoLAN
 * $Id: change_channel.php,v 1.1 2002/01/27 22:00:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

/* change_channel.php <channel> <MAC address>
 *
 * Return values :
 *  0 : OK
 *  1 : Machine not found
 *  2 : Protected port
 *  3 : SNMP failed
 */
if( $argv[1] == "-h" || $argv[1] == "--help" )
{
    echo "Usage : change_channel.php <channel> <MAC address> <ip>\n";
    exit( -1 );
}

$channel = $argv[1];
$mac_ad = $argv[2];
$ip = $argv[3];

//exec("host $ip",$result);
//list($dummy, $dns_l)=explode(" ",$result[0]);
//list($dns, $j1, $j2, $j3)=explode(".",$dns_l);

include( "database.inc" );
include( "snmp.inc" );

openlog( "VLCS/PHP", 0, LOG_DAEMON );
syslog(LOG_INFO,"$ip asked for channel $channel");


base_Init();

echo date("d/M H:i:s")." $ip asked for channel $channel\n";
$query = base_Check( $mac_ad, Channel2VLAN( $channel ) );

if( !$query )
{
    base_Close();
    closelog();
    exit( 1 );
}

if( $query["port_protection"] )
{
    base_Close();
    closelog();
    exit( 2 );
}

syslog(LOG_INFO,"Sending SNMP request");
/* Do SNMP request */
$var_name = "vlan".Channel2VLAN( $channel )."_id";
if( !snmp_set_vlan( $query["switch_ip"], $query["port_internal_id"],
                    $query[$var_name], $query["community_string"] ) )
{
    syslog( LOG_CRIT, "SNMP failed (".$query["switch_ip"].":"
            .$query["port_internal_id"].")\n" );
    base_Close();
    closelog();
    exit( 3 );
}

syslog(LOG_INFO,"SNMP request sent");
base_Change( $query["port_id"], Channel2VLAN( $channel ) );

// remove the ARP entry because we know that now, the client
// has changed vlan and so changed visible MAC address (cf vlanbridge)
// system("sudo arp -d $ip");

syslog(LOG_INFO,"Changed in DB");
base_Close();
closelog();
exit( 0 );
