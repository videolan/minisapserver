/*****************************************************************************
 * vlcs.c: VideoLAN Channel Server socket facility
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: vlcs.c,v 1.1 2002/01/27 22:00:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>

#include "vlcs.h"

#define VLCS_OK 0
#define VLCS_NOTFOUND 1
#define VLCS_PROTECTED 2
#define VLCS_FAILED 3

void Close( int );
void socket_Send( struct sockaddr_in *, char * );

int i_handle;
int i_channel_min, i_channel_max;

typedef struct channel_s
{
    char psz_vlc_config[256];
} channel_t;

channel_t p_channels[16];

/* Chained list of sons */

typedef struct vlc_s
{
    struct sockaddr_in sa_client;
    pid_t son;
    channel_t * p_channel;
    int i_version;
    struct vlc_s * p_next;
} vlc_t;

vlc_t * p_head = NULL;

void list_InsertVLC( vlc_t * p_vlc )
{
    p_vlc->p_next = p_head;
    p_head = p_vlc;
}

void list_DeleteVLC( vlc_t * p_vlc )
{
    if( p_vlc == p_head )
    {
        p_head = p_vlc->p_next;
        free( p_vlc );
    }
    else
    {
        vlc_t * p_current = p_head->p_next;
        vlc_t * p_previous = p_head;
        while( p_current != NULL )
        {
            if( p_current == p_vlc )
            {
                p_previous->p_next = p_vlc->p_next;
                free( p_vlc );
                break;
            }
            p_previous = p_current;
            p_current = p_current->p_next;
        }
    }
}

/* Socket management */

void socket_Init( void )
{
    int i_opt;
    struct sockaddr_in sa_server;

    i_handle = socket( AF_INET, SOCK_DGRAM, 0 );

    if( i_handle < 0 )
    {
        syslog( LOG_ALERT, "Cannot open socket (%s), exiting\n",
                strerror(errno) );
        closelog();
        exit( -1 );
    }

    if( setsockopt( i_handle, SOL_SOCKET, SO_REUSEADDR, &i_opt, sizeof(i_opt) )
          < 0 )
    {
        syslog( LOG_ALERT, "Cannot setsockopt (%s), exiting\n",
                strerror(errno) );
        Close( -1 );
    }

    sa_server.sin_family = AF_INET;
    sa_server.sin_addr.s_addr = INADDR_ANY;
    sa_server.sin_port = ntohs( VLCS_PORT );
    if( bind( i_handle, (struct sockaddr *)&sa_server, sizeof(sa_server) ) < 0 )
    {
        syslog( LOG_ALERT, "Cannot bind (%s), exiting\n", strerror(errno) );
        Close( -1 );
    }
}

void socket_Get( void )
{
    fd_set sockets;
    struct timeval timeout;

    FD_ZERO( &sockets );
    FD_SET( i_handle, &sockets );
    FD_SET( 0, &sockets );

    timeout.tv_usec = 0;
    timeout.tv_sec = 1;

    if( select( i_handle + 1, &sockets, NULL, NULL, &timeout ) < 0 )
    {
        if( errno == EINTR )
        {
            return;
        }
        syslog( LOG_ALERT, "Cannot select (%s), exiting\n", strerror(errno) );
        Close( -1 );
    }
    
    if( FD_ISSET( 0, &sockets ) )
    {
        /* Administration socket */
        char p_buffer[128];
        if( read( 0, p_buffer, sizeof( p_buffer ) ) < 0 )
        {
            if( errno == EINTR )
            {
                return;
            }
            syslog( LOG_CRIT, "Duh my controlling terminal has gone fishing\n" );
        }
        else if( p_buffer[0] == 'q' )
        {
            /* Quit */
            syslog( LOG_ALERT, "Exiting now on admin's request\n" );
            Close( 0 );
        }
    }

    if( FD_ISSET( i_handle, &sockets ) )
    {
        /* Client request */
        char p_buffer[2048];
        struct sockaddr_in sa_client;
        int i_len, i_version, i_time, i_channel;
        socklen_t i_dummy = sizeof( struct sockaddr_in );
        char psz_macad[18];
        char psz_channel[3];
        char * psz_tmp;
        vlc_t * p_vlc;

        if( (i_len = recvfrom( i_handle, p_buffer, sizeof( p_buffer ), 0,
                               (struct sockaddr *)&sa_client, &i_dummy )) < 0 )
        {
            if( errno == EINTR )
            {
                return;
            }
            syslog( LOG_CRIT, "Cannot recvfrom (%s)\n", strerror(errno) );
            return;
        }
        p_buffer[i_len] = '\0';
        
        if( sscanf( p_buffer, "%2s %d %d %17s", psz_channel, &i_version,
                    &i_time, psz_macad ) != 4 )
        {
            socket_Send( &sa_client, "E: Bad request" );
            syslog( LOG_CRIT, "sscanf failed (%s)\n", p_buffer );
            return;
        }

        if ((i_version < VLCS_VERSION_MIN ) || ( i_version > VLCS_VERSION_MAX ))
        {
            socket_Send( &sa_client, "E: Version mismatch" );
            syslog( LOG_CRIT, "Break-in attempt ! (%s)\n",
                    inet_ntoa( sa_client.sin_addr ) );
            return;
        }

        i_channel = strtol( psz_channel, &psz_tmp, 0 );
        if( psz_tmp[0] != '\0' || i_channel < i_channel_min ||
            i_channel > i_channel_max )
        {
            socket_Send( &sa_client, "E: Channel doesn't exist" );
            syslog( LOG_CRIT, "Bad channel (%s)\n", psz_channel );
            return;
        }

        if( (i_dummy = fork()) == 0 )
        {
            /* Son */
            close( i_handle );
            close( 0 );
            //close( 1 );
            close( 2 );

	        syslog(LOG_INFO,"Launching change_channel.php");
            closelog();
            execl( CHANGE_CHANNEL_PATH, CHANGE_CHANNEL, psz_channel,
                   psz_macad, inet_ntoa(sa_client.sin_addr), NULL );

            openlog( "VLCS", LOG_PERROR, LOG_DAEMON );
            syslog( LOG_ALERT, "Cannot execl (%s)\n", strerror(errno) );
            closelog();
            exit( -1 );
        }

        /* Father */
        if( i_dummy < 0 )
        {
            syslog( LOG_ALERT, "Cannot fork (%s)\n", strerror(errno) );
            return;
        }

#ifdef DEBUG
        syslog( LOG_NOTICE, "Spawned change_channel (%d)\n", i_dummy );
#endif
        p_vlc = (vlc_t *)malloc( sizeof( vlc_t ) );
        memcpy( &p_vlc->sa_client, &sa_client, sizeof( struct sockaddr_in ) );
        p_vlc->son = i_dummy;
        p_vlc->p_channel = &p_channels[i_channel];
        p_vlc->i_version=i_version;
        list_InsertVLC( p_vlc );
    }
}

void socket_Send( struct sockaddr_in * p_sa, char * psz_message )
{
  static char command[80];
  sprintf( command, "sudo arp -d %s", inet_ntoa( p_sa->sin_addr ) );
  printf( "Executing : %s\n", command );
  system(command);
    for( ; ; )
    {
        printf("Sending : %s\n", psz_message);
        if( sendto( i_handle, psz_message, strlen( psz_message ) + 1, 0,
                    (struct sockaddr *)p_sa,
                    sizeof( struct sockaddr_in ) ) < 0 )
        {
            if( errno == EINTR )
            {
                continue;
            }
            syslog( LOG_CRIT, "Cannot sendto (%s)\n", strerror(errno) );
        }
        break;
    }
}

void child_Wait( void )
{
    pid_t son;
    int i_status, i_done = 0;

    while( (son = waitpid( -1, &i_status, WNOHANG )) > 0 )
    {
        vlc_t * p_current = p_head;
        while( p_current != NULL )
        {
            if( p_current->son == son )
            {
                char psz_message[256];
                if( WIFSIGNALED( i_status ) )
                {
                    snprintf( psz_message, sizeof( psz_message ),
                              "E: vlcs died on signal %d", WTERMSIG(i_status) );
                    syslog( LOG_CRIT, "Child was killed by signal %d\n",
                            WTERMSIG( i_status ) );
                }
                else
                {
                    switch( WEXITSTATUS( i_status ) )
                    {
                    case VLCS_OK:
                        /* if the config line begins with I, it is raw data */
                        if (*p_current->p_channel->psz_vlc_config=='I')
                        {
                            snprintf( psz_message, sizeof( psz_message ),
                                  "%s", p_current->p_channel->psz_vlc_config );
                        }
                        else
                        {
                            /* depending on the protocol version, the
                             * string returned to the vlc is formatted
                             * in a different way                        */
                            static char p_server[30];
                            static char p_dest[30];
                            static char p_port[8];
                            sscanf(p_current->p_channel->psz_vlc_config,
                                    "%s %s %s", p_server, p_dest, p_port );

                            if ( p_current->i_version == 12 )
                            {
                                sprintf(psz_message,"ts://%s:%s/%s", 
                                            p_server, p_port, p_dest );
                            }
                            else if ( p_current->i_version == 13 )
                            {
                                sprintf(psz_message,"udpstream://%s@%s:%s", 
                                            p_server, p_dest, p_port );
                            }
                            else 
                            {
                                sprintf(psz_message,"Unknown protocol version");
                            }
                        }
                        break;

                    case VLCS_NOTFOUND:
                    {
                        time_t current_timestamp = time( NULL );
                        struct tm * current_time = localtime( &current_timestamp );

                        snprintf( psz_message, sizeof( psz_message ),
                                  "E: Your machine isn't in the database (next reload in %d minutes)",
                                  (RELOAD_TIME + 60 - current_time->tm_min) % 60 );
                        syslog( LOG_CRIT, "Machine %s not found\n",
                                inet_ntoa( p_current->sa_client.sin_addr ) );
                        break;
                    }

                    case VLCS_PROTECTED:
                        snprintf( psz_message, sizeof( psz_message ),
                                  "E: Your machine is on a protected port" );
                        syslog( LOG_CRIT, "Machine %s is on a protected port\n",
                                inet_ntoa( p_current->sa_client.sin_addr ) );
                        break;

                    case VLCS_FAILED:
                    default:
                        snprintf( psz_message, sizeof( psz_message ),
                                  "E: Query failed" );
                        syslog( LOG_CRIT, "SNMP failure for %s\n",
                                inet_ntoa( p_current->sa_client.sin_addr ) );
                        break;
                    }
                }
#ifdef DEBUG
                syslog( LOG_NOTICE, "Sent %s\n", psz_message );
#endif
                socket_Send( &p_current->sa_client, psz_message );

                list_DeleteVLC( p_current );
                i_done = 1;
                break;
            }
            p_current = p_current->p_next;
        }

        if( !i_done )
        {
            syslog( LOG_CRIT, "Cannot find client for PID %d !\n",
                    son );
        }
    }
}

void config_ReadFile( void )
{
    char psz_buffer[2048];
    FILE * p_config;

    p_config = fopen( CHANNEL_CONF, "r" );
    if( p_config == NULL )
    {
        syslog( LOG_ALERT, "Cannot fopen " CHANNEL_CONF " (%s)\n",
                strerror(errno) );
        exit( -1 );
    }

    i_channel_min = 0;
    i_channel_max = -1;

    while( fgets( psz_buffer, sizeof( psz_buffer ), p_config ) != NULL )
    {
        if( psz_buffer[0] != '#' && psz_buffer[0] != '\0' )
        {
            strncpy( p_channels[++i_channel_max].psz_vlc_config, psz_buffer,
                     255 );
        }
    }

    fclose( p_config );
}

void Close( int i_ret )
{
    close( i_handle );
    closelog();

    exit( i_ret );
}

void signal_DummyHandler( int i_signal )
{
#ifdef DEBUG
    syslog( LOG_NOTICE, "Ignoring signal %d\n", i_signal );
#endif
}

int main( int argc, char ** argv )
{
    config_ReadFile();
    socket_Init();
    openlog( SYSLOG_NAME, LOG_PERROR, SYSLOG_FACILITY );

    signal( SIGCHLD, signal_DummyHandler );

    printf("VLCS succesfully inited, entering main loop\n");
    for( ; ; )
    {
        socket_Get();

        child_Wait();
    }

    return( 0 );
}
