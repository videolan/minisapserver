/*****************************************************************************
 * vlcs.h: VideoLAN Channel Server configuration
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: vlcs.h,v 1.1 2002/01/27 22:00:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#define VLCS_VERSION_MIN 12
#define VLCS_VERSION_MAX 13
#define VLCS_PORT 6010
#define CHANGE_CHANNEL_PATH "/home/diff/nvlcs/bin/change_channel.php"
#define CHANGE_CHANNEL "change_channel.php"
#define CHANNEL_CONF "/home/diff/nvlcs/conf/vlcs.conf"
#define SYSLOG_NAME "VLCS"
#define SYSLOG_FACILITY LOG_DAEMON
/* Database is reloaded at xx:12:00 */
#define RELOAD_TIME 12
#undef DEBUG
