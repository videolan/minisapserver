create table "switch" (
	"switch_id" integer NOT NULL,
	"switch_name" character(50),
	"switch_ip" inet,
	"unit_number" integer,
	"switch_type" integer,
	"community_string" varchar(20),
	"vlan1_id" integer,
	"vlan2_id" integer,
	"vlan3_id" integer,
	"vlan4_id" integer,
	"vlan5_id" integer,
	"vlan6_id" integer,
	"vlan7_id" integer,
	"vlan8_id" integer,
	"vlan9_id" integer,
	"vlan10_id" integer,
	"vlan11_id" integer,
	"vlan12_id" integer,
	"vlan13_id" integer,
	"vlan14_id" integer,
	"vlan15_id" integer,
	"vlan16_id" integer,
	PRIMARY KEY (switch_id)
);

create table "port" (
	"port_id" integer NOT NULL,
	"switch_id" integer NOT NULL references "switch" on delete cascade,
	"port_number" integer,
        "port_internal_id" integer,
	"port_protection" integer,
	"vlan_num" integer,
	"unseen_count"integer,
	PRIMARY KEY (port_id)
);

create table "machine" (
	"machine_id" integer NOT NULL,
	"port_id" integer NOT NULL references "port" on delete cascade,
	"mac_ad" macaddr NOT NULL,
	"expires" timestamp NOT NULL,
	PRIMARY KEY (machine_id)
);

--create table "channel" (
--	"channel_number" integer,
--	"vlan_num" integer,
--	"vlc_config" text,
--	PRIMARY KEY (channel_number)
--);

create unique index "switch_idx" on "switch" ("switch_id");
create unique index "port_idx" on "port" ("port_id");
create unique index "machine_idx" on "machine" ("machine_id");
--create unique index "channel_idx" on "channel" ("channel_number");

create sequence "switch_seq";
create sequence "port_seq";
create sequence "machine_seq";

grant all on switch to "www-data";
grant all on switch_seq to "www-data";
grant all on port to "www-data";
grant all on port_seq to "www-data";
grant all on machine to "www-data";
grant all on machine_seq to "www-data";
