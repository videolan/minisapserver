#!/bin/sh
#
# script pour construire le vlb.diff
#

DIR=tmp-vlb-diff
KERNEL=linux-2.4.12

copy()
{
	if [ -e $KERNEL/$1 ]
	then
		cp -Rf "$KERNEL/$1" "$DIR/$KERNEL/$1"
	fi
	if [ -e $KERNEL-vlb/$1 ]
	then
		cp -Rf "$KERNEL-vlb/$1" "$DIR/$KERNEL-vlb/$1"
	fi
}

cd /usr/src/$KERNEL
make clean > /dev/null

cd /usr/src/$KERNEL-vlb
make clean > /dev/null

cd /usr/src
rm -Rf $DIR
mkdir $DIR
mkdir $DIR/$KERNEL
mkdir $DIR/$KERNEL/net
mkdir $DIR/$KERNEL/include
mkdir $DIR/$KERNEL-vlb
mkdir $DIR/$KERNEL-vlb/net
mkdir $DIR/$KERNEL-vlb/include

copy "include/if_vlb.h"
copy "net/bridge"
copy "net/Config.in"
copy "net/Makefile"

cd $DIR
find . | grep '/\.' | xargs rm -f
diff -urN $KERNEL $KERNEL-vlb > ../vlb-against-linux-2.4.x.diff
cd ..

rm -Rf $DIR
exit 0
