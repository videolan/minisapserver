***********************************************
************* WARNING *************************
***********************************************

This software comes with absolutely no warranty.

This is software has been developed for network administrators. If you do not
understand what is told in this file, please do NOT use VLANBridge, you can
completely crash your network if this is not configured properly.

You should inform yourself about the 802.1Q standard
before reading this document.

***********************************************


The VideoLAN project :

At the begining, the aim of the project was to broadcast high quality
video over a network using the MPEG2/TS standard.

The major problem is the following : How to send several 5Mbps stream over
a network that has 10Mbps terminal connections and a 155Mbps backbone ?
Two solutions appeared : 
    1 - multicast
    2 - broadcast isolated in a VLAN

The first solution was not possible because the network devices in the Ecole
Centrale Paris did not support multicast at all, but did support very well the
VLAN technology. So that one was chosen to be developed.

What is a VLAN ?
~~~~~~~~~~~~~~~~

VLAN means Virtual LAN. These allow to split a physical LAN (i.e. switches
connected together) into virtual LANs.

Here is an example of a 12-ports switch : 

+--------------------+
| 111122 222222      |
+--------------------+

The first four ports are in the VLAN 1 and the following ones are in
the VLAN 2.

Packets received on a port in the VLAN 1 will NEVER be sent through a
port in a different VLAN : VLANs are completely isolated.


How VLANs are used by VideoLAN ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Each stream is broadcasted in a different VLAN and people who want to see
that program just ask the VLANserver to change their port into a different
VLAN so that the switch will transmit the packets to them.


What happens with several switches ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let's look at the following example with two switches connected
together using their port number 12.

+--------------------+
| 422222 22222T      | Switch-A
+-------------�------+
              �
+-------------�------+ 
| 222222 22244T      | Switch-B
+--------------------+

One VideoLAN server is connected on the first port of Switch-A and is
broadcasting a stream (VLAN 4).

One client on 6 port of Switch-B asks for VLAN 4, the channel server
does a SNMP request on the switch and the result is the following :


+--------------------+
| 422222 22222T      | Switch-A
+-------------�------+
              �
+-------------�------+ 
| 222224 22244T      | Switch-B
+------�-------------+
       �
    +--�-----+
    | client |
    +--------+

Port 12 of Switch-A and Switch-B are configured to be in all the VLANs that
are needed. When a packet is sent through this port, the Switch adds to the
packet a tag indicating the VLAN which the packet belongs to. The 802.1Q tagging
is supported by the most part of the existing switches. 3com switches have a
special tagging called vlt (vlantrunking), those ports belongs to all the vlans.

So when a switch receives a packet on such a port, it can easily know where it
could send the packet or not.

Switch-A receives the stream on port 1 and sees it is sent in the VLAN 4, it
sends it through the port 12 with the tag 'VLAN 4'. Then Switch-B receives it
and sends it to the ports 6, 10 and 11.

NB: the switch sends the packet to every port because it is a broadcast packet
which is aimed to be received to anyone in the VLAN.


Why a VLANBridge ?
~~~~~~~~~~~~~~~~~~

+--------------------+
| 422222 22222T      | Switch-A
+-------------�------+
              �
+-------------�------+ 
| 222224 22244T      | Switch-B
+-�-------�----------+
  �       �
  �    +------------+
  �    | ftp server |
  �    +------------+
  �
  �
+-�------+  
| Router ���>> Internet
+--------+

The client is the VLAN 4 may want to keep its connection to the internet or
to a ftp server while watching the stream or surfing on the web during the
advertisement :p. But the VLANs prevents him from this :-(.

The idea is to plug a 'VLANBridge' in every VLAN needed so that it forwards the
packets from one VLAN to another excepts the stream :

      +------------+
      | VLANBridge |
      +---|||------+
+---------|||----------+
| 422222 22342T      | Switch-A
+-------------�------+
              �
+-------------�------+ 
| 222224 22244T      | Switch-B
+-�-------�----------+
  �       �
  �    +------------+
  �    | ftp server |
  �    +------------+
  �
  �
+-�------+  
| Router ���>> Internet
+--------+

When a packet is sent from a client in the VLAN 4 to an IP outside the current
subnet, it is caught by the VLANBridge using its interface in the VLAN 4 and
sent back to the router using its interface in the VLAN 2.


Two different views of the same problem :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1 - the VLANbridge passes packets from one interface to another
only for a limited number of client that are specified (the ones
that are not in the default VLAN)
2 - passes every packets only if they do not come from a list of
specified hosts (typically : VideoLAN servers)

The first view was the one of the former VLANBridge. The second
one has been chosen for this VLANBridge. The main problem with the
older solution is :
- when someone wants to change a channel, the Channel Server
changes the VLAN on the switch AND it needs to tell the vlanbridge
who to bridge.
- if one the two operations fails, the client could have a lot of
trouble (completely isolated from the network, ip conflict under windows...)

With the newer solution, only one atomic operation is made and the
VLANBridge learns by itself which hosts are behind its interfaces.


***********************************************************************

How it works ?
~~~~~~~~~~~~~~

+--------+
| VLAN 2 |
+----�---+
     �
     �
+----�-------+      +--------+
|   Bridge   �������� VLAN 3 |
+----�-------+      +--------+
     �
     �
+----�---+
| VLAN 4 |
+--------+

The VLANBridge is based on the Linux Kernel Bridge module. A bridge
acts as a simple switch on a network. It memorises for each interface,
the list of the MAC addresses that are behind this interface.

When a packet arrives on an interface, it looks in the packet to know
where to forward it. A broadcast packet (FF:FF:FF:FF:FF:FF MAC address)
is sent to all the interfaces.

Problem :
~~~~~~~~~
Such a bridge with a filtering feature could be enough but some switches
does not support to see the same MAC adress in different VLAN.

Solution :
~~~~~~~~~~

The vlanbridge changes MAC adresses so that :
In the point of view of VLAN 3, all hosts in the VLAN 2 and 4 seems
to have the same MAC adress.

* A packet arrives :
- it memorizes the source MAC address and IP address.
- it changes the destination MAC address according to the IP destination
- it passes the packet to the bridging module

* The bridging module only sees the real MAC address and switches as usual.

* Before the packet is sent back :
- it changes the source MAC address with the interface's MAC address


       +----------------------------------------------------------------------+
       |VLANbridge                                                            |
       |                                                                      |
eth1   | +--------+ +--------------------------------+ +------------------+   |
=========> Filter ==> Adds an entry in the arp table ==> Changes MAC dest ==+ |
       | +--------+ | based on the IP/MAC source     | +------------------+ | |
       |            +--------------------------------+                      | |
       |                                               +--------------------|-+
       |                                               |                eth1|
eth2   | +-----------------------------+               |eth2+---------------V-+ 
<========= Replaces source MAC address <===================== Bridging module |
       | +-----------------------------+               |    +-----------------+
       +-----------------------------------------------+ 

       
Features :
~~~~~~~~~~
- only ARP, IP and DHCP packet can go through the VLANBridge, the others
are simply discarded by the kernel
- the filter is placed when a packet arrives. If the MAC adress matches
one in the specified list, the packet is discarded.
    one option can be specified : 'allow_unicast', when it is
    specified, only broadcast packets that are not ARP packets are
    discarded by the filter
- when an IP's MAC address changes, it is loggued to the syslog, and
every day, the syslog is parsed and a mail is sent in order to advise
these changes to the network administrator (like arpwatch)
- packets whose IP is outside the subnet is sent to the specified gateway.
Only one gateway is supported for the moment.
- the configuration is completely done using IOCTLs. A patch to the
brctl has been written in order to make it easy to configure the
vlanbridge.
- the VLANbridge has not been tested yet with an IP attached to
the bridge.

WARNING : This patch has not been tested with STP enabled

**************************************************************

How to install it ?
~~~~~~~~~~~~~~~~~~~

cd /usr/src/linux
patch -p1 < vlb.diff
make menuconfig
* Code Maturity level option :
    - select Prompt for development and/or incomplete code/drivers
* in networking options :
    - select 802.1d Ethernet bridging as a module
    - select VLANBridge
make modules
make modules_install
edit your configuration script
launch it !

Here is a sample configuration script :

ifconfig eth0 up
ifconfig eth1 up
ifconfig eth2 up
ifconfig eth3 up
brctl addbr br0 vlanbridge
brctl addif br0 eth0
brctl addif br0 eth1
brctl addif br0 eth2
brctl addif br0 eth3
brctl setfd br0 0
brctl stp br0 off
brctl setsubnet br0 192.168.0.0/16 08:00:12:AF:E3:10
brctl settimeoutstate br0 disable
brctlsetloglevel br0 5
brctl adddeny 00:01:02:AB:CD:EF
ifconfig br0 up

And to shutdown it cleanly :

ifconfig br0 down
ifconfig eth0 down
ifconfig eth1 down
ifconfig eth2 down
ifconfig eth3 down
brctl delif eth0
brctl delif eth1
brctl delif eth2
brctl delif eth3
brctl delbr br0
modprobe -r bridge

You can simply add those commands in the file /etc/init.d/networking
for example (debian).
